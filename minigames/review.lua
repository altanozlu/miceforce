s = {
	a = {Kinta = true}, -- admins
	m = {}, -- maps
	p = {}, -- players
	c = {"start", "queue", "help", "add", "reset", "review"},
	round = 0,
	lastLoad = os.time(),
}

function main()
	tfm.exec.disableAutoNewGame(true)
	tfm.exec.disableAutoTimeLeft(true)
	for k, v in pairs(s.c) do system.disableChatCommandDisplay(v) end
	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end
end

function eventNewGame()
	eventPopupAnswer(0, nil, "", true)
end

function eventNewPlayer(n)
	local b = system.bilgi(n)
	s.p[n] = {}
	if b.isMapcrew or b.priv > 9 then
		s.a[n] = true
		tfm.exec.bindKeyboard(n, 74, true, true)
		tfm.exec.bindKeyboard(n, 75, true, true)
		tfm.exec.bindKeyboard(n, 76, true, true)
	end
end

function eventChatCommand(n, c)
	local a = split(c, "%s")
	if s.a[n] then
		if h(a[1], "help") then
			log(n, "Please use <CH>!add [map codes] <BL>to add maps to your playlist and then type <CH>!start <BL>to start the review.")
		elseif h(a[1], "add") then
			for k in c:gmatch("%d+") do
				local v = tonumber(k)
				if not s.m[v] then
					s.m[v] = {}
				end
			end
			log(n, "Your maps have been added. You can type <CH>!start <BL>to start reviewing and <CH>!queue <BL>to see your playlist.")
		elseif h(a[1], "queue") then
			local m, i = {}, 0
			for k, v in pairs(s.m) do
				i = i + 1
				table.insert(m, "<J>"..(v.a or 'Unknown').." <BL>- @"..k..(v.r and ': <N>'..v.r or ''))
			end
			ui.addLog("<BV><p align='center'><b><font face='Courier New' size='22'>Q U E U E</font></b></p><N>"..table.concat(m, "\n"), n)
		elseif h(a[1], "reset") then
			s.m = {}
			log(n, "You have succesfully cleaned your playlist.")
		elseif h(a[1], "start") then
			if s.round < 1 then
				s.round = 1
				s.lastLoad = os.time()
				play()
				log(n, "Your review has started.")
			end
		elseif h(a[1], "review") then
			ui.addPopup(0, 2, "", n, 10, 30, 780, true)
		end
	end
end

function eventPopupAnswer(i, n, a, f)
	if #a > 0 or f then
		local i, code = 0
		for k, v in pairs(s.m) do
			i = i + 1
			if i == s.round then
				code = k
			end
		end
		if code then
			s.m[code].r = #a > 0 and a or nil
			s.m[code].a = tfm.get.room.xmlMapInfo.author
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	if s.a[n] then
		if h(k, 74, 75, 76) then
			if s.round < 1 then
				log(n, "Please type <CH>!start <BL>to start reviewing maps.")
			elseif os.time() - s.lastLoad <= 3000 then
				log(n, "Please wait <CH>"..3 - math.floor((os.time() - s.lastLoad) / 1000).." seconds <BL>before changing maps.")
			else
				if k == 74 and s.round > 1 then
					s.round = s.round - 1
				elseif k == 75 then

				elseif k == 76 then
					if s.round < count(s.m) then
						s.round = s.round + 1
					else
						s.round = 0
						tfm.exec.newGame(0)
						log(n, "You have finished reviewing the maps. Please type <CH>!start <BL>to restart or <CH>!reset <BL>to clean your playlist.")
					end
				else
					return
				end
				play()
			end
		end
	end
end

function count(t)
	local i = 0
	for k, v in pairs(t) do
		i = i + 1
	end
	return i
end

function play()
	local i = 0
	for k, v in pairs(s.m) do
		i = i + 1
		if i == s.round then
			tfm.exec.newGame("@"..k)
			s.lastLoad = os.time()
		end
	end
end

function find(t, e)
	if e ~= nil then
		for k, v in pairs(t) do
			if v == e then
				return true
			end
		end
	end
	return false
end

function log(n, m)
	tfm.exec.chatMessage("<V>[<a href='event:x_mj;#'>#</a>] <BL>"..m, n)
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function h(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

main()