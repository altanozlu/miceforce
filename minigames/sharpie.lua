tfm.exec.disableAutoShaman(true)
devs = {"Soulzone", "Sevenops"}

function eventNewPlayer(name)
  tfm.exec.chatMessage("<VP>Welcome to the #sharpie game!",name)
  tfm.exec.bindKeyboard(name,32,true,true)
end

for name,player in pairs(tfm.get.room.playerList) do
	eventNewPlayer(name)
end

function eventKeyboard(name,key,down,x,y)
  if key==32 then 
  	tfm.exec.movePlayer(name,0,0,false,0,-50,true)
  end
end

function eventChatCommand(name, cmd)
  if cmd:sub(0,4) == "chat" then
  	for k,chatter in pairs(devs) do
  		tfm.exec.chatMessage("<ROSE>• [LUA] ["..name.."] "..string.sub(cmd, 6).." </font>",chatter)
  	end 
  end
end

tfm.exec.newGame()
system.disableChatCommandDisplay('chat',true)