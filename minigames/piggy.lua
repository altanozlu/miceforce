roommod={}
players = {}
timer = 0
countdown = 6
player = nil
ending = false
difficulty = 30
highscore = 0
scoredby="?inconnu"
playerList = {}
function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
RL={"piggy_mg_2.png","piggy_mg_4.png"}
RL2={"piggy_mg_2.png","piggy_mg_3.png","piggy_mg_4.png"}
function main()
  tfm.exec.disableAutoNewGame(true)
  tfm.exec.disableAutoShaman(true)
  tfm.exec.disableAutoTimeLeft(true)
  tfm.exec.disableAutoScore(true)
  tfm.exec.newGame('<C><P /><Z><S><S P="0,0,,,,0,0,0" L="800" H="89" X="400" Y="407" T="9" /><S m="" P="0,0,0,0.2,0,0,0,0" c="3" L="11" H="407" X="807" Y="202" T="1" /><S m="" P="0,0,0,0.2,0,0,0,0" c="3" L="11" H="407" X="-7" Y="201" T="1" /></S><D><DS Y="368" X="382" /></D><O /></Z></C>')

  ui.setMapName("<CH><B>#Piggy</B> <N>| <b>Time</b> : <V>00:00 <N>| <CH><b>Best Bird:</b><n> "..scoredby.." ("..highscore.."sec)")
end

function eventLoop(time)

  timer = timer + 0.5
  if time>3000 and not ending then
    difficulty = math.ceil(timer/15)
    spawnArrow(difficulty)
  end
  if timer%1==0 then
    showTime(timer)
  end
  if ending then
    countdown = countdown - 1
    ui.setMapName("<ROSE>Pigs are coming in "..countdown.." sec.")
    if countdown == 0 then
      tfm.exec.newGame('<C><P /><Z><S><S P="0,0,,,,0,0,0" L="800" H="89" X="400" Y="407" T="9" /><S m="" P="0,0,0,0.2,0,0,0,0" c="3" L="11" H="407" X="807" Y="202" T="1" /><S m="" P="0,0,0,0.2,0,0,0,0" c="3" L="11" H="407" X="-7" Y="201" T="1" /></S><D><DS Y="368" X="382" /></D><O /></Z></C>')


    end
  end
end

function showTime(timer)
  local amount = timer / 60
  local mins = math.floor(amount)
  local seconds = math.floor((amount - mins)*60)

  if mins < 10 then
    mins = "0"..tostring(mins)
  end
  if seconds < 10 then
    seconds = "0"..tostring(seconds)
  end
  ui.setMapName("<CH><B>#Piggy</B> <N>| <b>Time</b> : <V>"..mins..":"..seconds.." <N>| <CH><b>Best Bird:</b><n> "..scoredby.." ("..highscore.."sec)")
end

function eventNewGame(name)
  ui.removeTextArea(1)
  ui.removeTextArea(2)
  choosePlayer()
  difficulty = 1
  ui.setMapName("<CH><B>#Piggy</B> <N>| <b>Time</b> : <V>00:00 <N>| <CH><b>Best Bird:</b><n> "..scoredby.." ("..highscore.."sec)")
  timer=0
  countdown=5
  ending = false
  for k,v in pairs(tfm.get.room.playerList) do
    if k~=player then

      tfm.exec.killPlayer(k)

    end

  end
end

function eventPlayerDied(name)
  if name == player then
    ending = true
    if timer > highscore then
      highscore = math.floor(timer)
      scoredby=player
      tfm.exec.chatMessage("<CH>"..player.." DID IT")

    end
  else
    tfm.exec.chatMessage("<R>"..player.." made us ANGRY")
  end
end

function spawnArrow(int)
  if int==1 then
    local id=tfm.exec.addShamanObject(17, math.random()*800, -80)
    tfm.exec.addImage(RL[math.random(#RL)], "#"..id, -25, -27);
  elseif int==2 then
    local id=tfm.exec.addShamanObject(17, math.random()*800, -80, 180)
    tfm.exec.addImage(RL2[math.random(#RL2)], "#"..id, -25, -27);
  else
    for i=1, int-2 do
      local id=tfm.exec.addShamanObject(17, math.random()*800, -80, 180)
      tfm.exec.addImage(RL2[math.random(#RL2)], "#"..id, -25, -27);
    end

    local id=tfm.exec.addShamanObject(17, math.random()*800, -80)
    tfm.exec.addImage(RL[math.random(#RL)], "#"..id, -25, -27);
  end
end

function choosePlayer()
  player = players[1]
  tfm.exec.addImage("piggy_mg_1.png", "%"..player.."", -37, -27);


  table.remove(players, 1)
  table.insert(players, player)
end

function eventNewPlayer(name)
  tfm.exec.chatMessage("<CH>PIGGY PIGGY",name)
  table.insert(players, name)
end

function eventPlayerLeft(name)
  table.delete(players, name)
end

function table.delete(tab, val)
  for k,v in pairs(tab) do
    if val == v then
      table.remove(tab, k)
      break end
  end
end

for k,v in pairs(tfm.get.room.playerList) do
  eventNewPlayer(k)
end


main()