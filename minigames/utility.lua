p = {}
commands = {"soulmate", "sm", "soul", "event", "slap", "gro", "obj", "ag", "ao", "ro", "ban", "hban", "xml", "showxml", "nc", "namecolor", "kick", "privnum", "chatlog", "clear", "priv", "admin", "mute", "color", "chat", "fw", "word", "del", "admins", "tright", "vodka", "title", "proplist", "set", "snow", "prop", "bg", "fly", "a", "look", "p", "pw", "password", "lines", "ns", "next", "fromage", "f", "ch", "shaman", "v", "vampire", "vamp", "fix", "n", "name", "s", "size", "r", "res", "respawn", "m", "map", "play", "np", "log", "k", "kill", "mort", "c", "cheese", "w", "win", "victory", "ai", "ri", "t", "time", "timeleft", "bind", "tp", "e", "emote"}

help = {
	"<textformat tabstops='[10, 170, 290]'>Welcome to Utility. The module has been recoded and it has new functions that you can use. If you would like to be a room admin and use utility, go to <J>#utility0%s<N>. If you're using a command function that applies to a specific player, <G>leaving the username blank <N>will apply the function to <N>you, putting <J>* <N>will apply it for <J>everyone<N>.\n\n<CH><b>\tCommand\tArguments\tDescription</b>\n\t<PT>!m<G> / <PT>!map<G> / <PT>!np <T>\t[mapcode]<BL>\tPlay a map.\n\t<PT>!r <G>/ <PT>!res <G>/ <PT>!respawn <T>\t[name]\t<BL>Respawn a player.\n\t<PT>!k <G>/ <PT>!kill <G>/ <PT>!mort <T>\t[name]\t<BL>Kill a player.\n\t<PT>!f <G>/ <PT>!cheese <G>/ <PT>!fromage <T>\t[name]\t<BL>Give cheese to a player.\n\t<PT>!w <G>/ <PT>!win <G>/ <PT>!victory <T>\t[name]\t<BL>Teleport a player into the hole.\n\t<PT>!ch <G>/ <PT>!shaman <T>\t[name]\t<BL>Turn a player into a shaman.\n\t<PT>!nc <G>/ <PT>!namecolor\t<T>[name] [color]\t<BL>Change someone's name color.\n\t<PT>!v <G>/ <PT>!vamp <G>/ <PT>!vampire <T>\t[name]\t<BL>Turn a player into a vampire.\n\t<PT>!t <G>/ <PT>!time <G>/ <PT>!timeleft <T>\t[seconds]\t<BL>Change the time left.\n\t<PT>!e <G>/ <PT>!emote <T>\t[name] [num]\t<BL>Make someone do a specific emote.",
	"<textformat tabstops='[10, 170, 290]'>\t<CH><b>Command\tArguments\tDescription</b>\n\t<PT>!log\t<T>-\t<BL>Disables/enables the command log.\n\t<PT>!proplist\t<T>-\t<BL>Shows the prop list.\n\t<PT>!prop\t<T>[propcode]\t<BL>Turns you into a prop.\n\t<PT>!tp\t<T>[name]\t<BL>Changes your teleport target.\n\t<PT>!fly\t<T>[name]\t<BL>Give someone the ability to fly.\n\t<PT>!bg\t<T>[1-9]\t<BL>Change the map's background.\n\t<PT>!snow\t<T>[seconds]\t<BL>Make it snow.\n\t<J>!set\t<T>dm\t<BL>Disables/enables auto new game.\n\t<PT>\t<T>dc\t<BL>Disables/enables shamans.\n\t<PT>\t<T>ar\t<BL>Disables/enables auto respawn.\n\t<PT>\t<T>afk\t<BL>Disables/enables afk death.\n\t<PT>\t<T>con\t<BL>Disables/enables consumables.\n\t<PT>\t<T>vodka\t<BL>Sevenops fun.\n\t<PT>\t<T>disco\t<BL>Enables/disables disco names.\n\t<PT>\t<T>fw\t<BL>Enables/disables fireworks.",
}

events = {
	val1 = {
		xml = '<C><P Ca="" APS="x_transformice/x_sevgili/x_shopfg.png,1,0,0,0,0,0,0" D="x_transformice/x_sevgili/x_shopbg.png" L="1600" /><Z><S><S P="0,0,0.3,0.2,0,0,0,0" L="750" o="" H="20" X="375" Y="385" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="890" o="" H="20" X="635" Y="387" T="12" /><S P="0,0,0.3,0.2,-13,0,0,0" L="120" o="" H="20" X="1135" Y="367" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="420" o="" H="20" X="1400" Y="350" T="12" /><S P="0,0,0.3,0.2,-2,0,0,0" L="420" o="" H="20" X="1470" Y="345" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="10" o="" H="101" X="1077" Y="255" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="200" o="" H="20" X="992" Y="208" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="200" o="" H="20" X="956" Y="159" T="12" /><S P="0,0,0.3,0.2,-36,0,0,0" L="10" o="" H="61" X="1070" Y="176" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="220" o="" H="10" X="745" Y="160" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="55" o="" H="10" X="796" Y="146" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="55" o="" H="10" X="693" Y="146" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="283" o="" H="10" X="719" Y="57" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="221" o="" H="10" X="516" Y="141" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="300" o="" H="33" X="256" Y="128" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="10" o="" H="182" X="111" Y="234" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="" H="400" X="-5" Y="200" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="" X="1605" H="400" Y="200" T="12" /></S><D><DS Y="360" X="455" /></D><O /></Z></C>',
		name = "Valentine's Shopping"
	},
	val2 = {
		xml = '<C><P APS="x_transformice/x_sevgili/x_foreground.png,1,0,0,0,0,0,0;x_transformice/x_sevgili/x_sevgililer2018_cafe.png,0,545,150,670,390,0,0;x_transformice/x_sevgili/x_treehouse.png,0,0,0,250,150,0,0" Ca="" L="2400" D="x_transformice/x_sevgili/x_sevgililer2018_bg.png" /><Z><S><S m="" P="0,0,0.3,0.2,0,0,0,0" L="900" o="36D087" H="25" X="450" Y="400" T="12" /><S m="" P="0,0,0.3,0.2,-3,0,0,0" L="343" o="36D087" H="25" X="1042" Y="391" T="12" /><S m="" P="0,0,0.3,0.2,-6,0,0,0" L="280" o="36D087" H="25" X="1350" Y="368" T="12" /><S m="" P="0,0,0.3,0.2,-21,0,0,0" L="155" o="36D087" H="25" X="1525" Y="330" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="500" o="36D087" H="25" X="1842" Y="302" T="12" /><S m="" P="0,0,0.3,0.2,20,0,0,0" L="157" o="36D087" H="25" X="2161" Y="328" T="12" /><S m="" P="0,0,0.3,0.2,6,0,0,0" L="207" o="36D087" H="25" X="2314" Y="363" T="12" /><S m="" P="0,0,,,,0,0,0" L="55" H="140" X="860" Y="320" T="9" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="476" o="36D087" H="10" X="1130" Y="232" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="290" o="36D087" H="10" X="682" Y="235" T="12" /><S m="" P="0,0,0.3,0.2,-1,0,0,0" L="301" o="36D087" H="10" X="1047" Y="137" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="397" o="36D087" H="10" X="734" Y="140" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="210" X="540" Y="240" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="60" X="1200" Y="160" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="100" X="1200" Y="280" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="240" X="30" Y="270" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="190" X="95" Y="245" T="12" /><S m="" P="0,0,,,,0,0,0" L="60" H="220" X="60" Y="280" T="9" /><S m="" P="0,0,0.3,0.2,-28,0,0,0" L="10" o="36D087" H="83" X="119" Y="294" T="12" /><S m="" P="0,0,0.3,0.2,-96,0,0,0" L="10" o="36D087" H="45" X="120" Y="333" T="12" /><S m="" P="0,0,0.3,0.2,27,0,0,0" L="10" o="36D087" H="119" X="130" Y="213" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="190" o="36D087" H="10" X="185" Y="155" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="40" o="36D087" H="10" X="15" Y="155" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="400" X="-5" Y="200" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="400" X="2405" Y="200" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="2400" o="36D087" H="20" X="1200" Y="10" T="12" /><S m="" P="0,0,0.3,0.2,10,0,0,0" L="50" o="36D087" H="10" X="303" Y="159" T="12" /><S m="" P="0,0,0.3,0.2,5,0,0,0" L="75" o="36D087" H="10" X="362" Y="166" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="50" o="36D087" H="10" X="420" Y="169" T="12" /><S m="" P="0,0,0.3,0.2,-10,0,0,0" L="50" o="36D087" H="10" X="468" Y="165" T="12" /><S m="" P="0,0,0.3,0.2,-20,0,0,0" L="77" o="36D087" H="10" X="501" Y="153" T="12" /><S m="" P="0,0,0.3,0.2,-2,0,0,0" L="188" o="36D087" H="10" X="1505" Y="214" T="12" /><S m="" P="0,0,0.3,0.2,-2,0,0,0" L="159" o="36D087" H="10" X="1691" Y="161" T="12" /><S m="" P="0,0,0.3,0.2,1,0,0,0" L="284" o="36D087" H="10" X="1961" Y="128" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="100" o="36D087" H="10" X="2280" Y="208" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="90" o="36D087" H="15" X="145" Y="145" T="12" /><S m="" P="0,0,0.3,0.2,-1,0,0,0" L="200" o="36D087" H="10" X="1268" Y="230" T="12" /></S><D><P X="692" P="0,0" Y="190" T="70" /><DS Y="375" X="350" /><P X="640" P="0,0" Y="230" T="67" /><P X="690" P="0,0" Y="230" T="68" /><P X="740" P="0,1" Y="230" T="67" /><P X="1000" P="0,0" Y="227" T="67" /><P X="1050" P="0,0" Y="227" T="68" /><P X="1100" P="0,1" Y="227" T="67" /><P X="470" P="0,0" Y="385" T="69" /><P X="280" P="0,0" Y="160" T="77" /><P X="990" P="0,0" Y="380" T="67" /><P X="1040" P="0,0" Y="377" T="68" /><P X="1090" P="0,1" Y="375" T="67" /><P X="640" P="0,0" Y="385" T="67" /><P X="690" P="0,0" Y="385" T="68" /><P X="740" P="0,1" Y="385" T="67" /><P X="1050" P="0,1" Y="188" T="73" /><P X="10" P="0,0" Y="149" T="72" /><P X="690" P="0,0" Y="346" T="73" /><P X="1360" P="0,1" Y="232" T="77" /><P X="1040" P="0,0" Y="338" T="71" /></D><O /></Z></C>',
		name = "Valentine's Date"
	},
	['2018'] = {
		xml = '<C><P bg="fd4a9d62cf3b11e7b5f688d7f643024a.png" Ca="" fg="0e9334b2cf3c11e7b5f688d7f643024a.png" /><Z><S><S P="0,0,0.3,0.2,-4,0,0,0" L="800" o="" H="50" X="356" Y="360" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="156" o="" H="10" X="650" Y="334" T="13" /><S P="0,0,0.3,0.2,-2,0,0,0" L="400" o="" H="50" X="845" Y="196" T="12" /><S P="0,0,0.3,0.2,-45,0,0,0" L="400" o="" H="40" X="418" Y="373" T="12" /><S P="0,0,0.3,0.2,-20,0,0,0" L="400" o="" H="40" X="425" Y="340" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="20" o="" H="400" X="810" Y="200" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="20" o="" H="400" X="-10" Y="200" T="12" /></S><D><DS Y="342" X="58" /></D><O /></Z></C>',
		name = "Happy New Year!"
	},
	boat = {
		xml = '<C><P Ca="" mc="" F="0" /><Z><S><S P="0,0,,,,0,0,0" L="800" X="400" H="800" Y="600" T="9" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="6a7495" X="-5" H="400" Y="200" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="6a7495" H="400" X="805" Y="200" T="12" /><S P="1,9999,0.3,0.2,0,1,0,0" i="-15,-30,http://cdn.miceforce.com/image/boat_lol.5032/" L="250" H="30" X="400" N="" Y="210" T="0" /><S m="" P="1,0,0.3,0.2,-25,1,0,0" L="10" H="60" X="275" Y="195" T="0" /><S m="" P="1,0,0.3,0.2,25,1,0,0" L="10" H="40" X="520" Y="185" T="0" /><S m="" P="1,0,0.3,0.2,0,1,0,0" L="160" H="40" X="400" Y="230" T="0" /></S><D><DS Y="170" X="400" /></D><O><O X="285" C="22" Y="215" P="0" /><O X="512" C="22" Y="200" P="0" /><O X="400" C="22" Y="215" P="0" /></O></Z></C>',
		name = "Fishing Boat"
	},
	candy = {
		xml = '<C><P Ca="" bg="candlyland_bg.5033" fg="candlyland_fg.5034" /><Z><S><S m="" H="10" P="0,0,0.3,0.2,0,0,0,0" L="237" o="324650" X="321" Y="196" T="12" /><S m="" H="11" P="0,0,0.3,0.2,3,0,0,0" L="130" o="324650" X="504" Y="200" T="12" /><S m="" H="10" P="0,0,0.3,0.2,5,0,0,0" L="45" o="324650" X="591" Y="205" T="12" /><S m="" H="10" P="0,0,0.3,0.2,7,0,0,0" L="30" o="324650" X="628" Y="209" T="12" /><S m="" H="10" P="0,0,0.3,0.2,9,0,0,0" L="30" o="324650" X="656" Y="213" T="12" /><S m="" H="10" P="0,0,0.3,0.2,11,0,0,0" L="30" o="324650" X="685" Y="218" T="12" /><S m="" H="10" P="0,0,0.3,0.2,13,0,0,0" L="20" o="324650" X="709" Y="223" T="12" /><S m="" H="10" P="0,0,0.3,0.2,15,0,0,0" L="30" o="324650" X="733" Y="229" T="12" /><S m="" H="10" P="0,0,0.3,0.2,17,0,0,0" L="30" o="324650" X="761" Y="237" T="12" /><S m="" H="10" P="0,0,0.3,0.2,19,0,0,0" L="30" o="324650" X="789" Y="246" T="12" /><S m="" H="10" P="0,0,0.3,0.2,30,0,0,0" L="13" o="324650" X="199" Y="192" T="12" /><S m="" H="10" P="0,0,0.3,0.2,20,0,0,0" L="21" o="324650" X="185" Y="186" T="12" /><S m="" H="10" P="0,0,0.3,0.2,10,0,0,0" L="19" o="324650" X="167" Y="181" T="12" /><S m="" H="10" P="0,0,0.3,0.2,-2,0,0,0" L="24" o="324650" X="147" Y="180" T="12" /><S m="" H="10" P="0,0,0.3,0.2,-10,0,0,0" L="144" o="324650" X="65" Y="193" T="12" /><S m="" H="400" P="0,0,0,0.2,0,0,0,0" L="10" o="324650" X="-5" Y="200" T="12" /><S m="" H="400" P="0,0,0,0.2,0,0,0,0" L="10" o="324650" X="805" Y="200" T="12" /><S m="" H="10" P="0,0,0.3,0.2,0,0,0,0" L="23" o="324650" X="65" Y="209" T="13" /></S><D><DS Y="177" X="275" /></D><O /></Z></C>',
		name = "Candyland"
	},
	beach = {
		xml = '<C><P Ca="" fg="beach_fg2.5146" bg="beach_bg.5103" /><Z><S><S m="" H="106" P="0,0,,,,0,0,0" L="800" X="400" Y="348" T="9" /><S m="" H="10" P="0,0,0.3,0.2,14,0,0,0" L="35" o="324650" X="14" Y="245" T="12" /><S m="" H="11" P="0,0,0.3,0.2,5,0,0,0" L="35" o="324650" X="47" Y="251" T="12" /><S m="" H="11" P="0,0,0.3,0.2,8,0,0,0" L="20" o="324650" X="74" Y="254" T="12" /><S m="" H="11" P="0,0,0.3,0.2,13,0,0,0" L="15" o="324650" X="90" Y="257" T="12" /><S m="" H="11" P="0,0,0.3,0.2,28,0,0,0" L="16" o="324650" X="103" Y="262" T="12" /><S m="" H="12" P="0,0,0.3,0.2,39,0,0,0" L="21" o="324650" X="117" Y="272" T="12" /><S m="" H="12" P="0,0,0.3,0.2,36,0,0,0" L="21" o="324650" X="133" Y="284" T="12" /><S m="" H="12" P="0,0,0.3,0.2,24,0,0,0" L="21" o="324650" X="149" Y="293" T="12" /><S m="" H="10" P="0,0,0.3,0.2,34,0,0,0" L="46" o="324650" X="177" Y="309" T="12" /><S m="" H="10" P="0,0,0.3,0.2,42,0,0,0" L="20" o="324650" X="203" Y="328" T="12" /><S m="" H="10" P="0,0,0.3,0.2,72,0,0,0" L="20" o="324650" X="212" Y="342" T="12" /><S m="" H="10" P="0,0,0.3,0.2,37,0,0,0" L="20" o="324650" X="220" Y="356" T="12" /><S m="" H="10" P="0,0,0.3,0.2,14,0,0,0" L="20" o="324650" X="229" Y="361" T="12" /><S m="" H="10" P="0,0,0.3,0.2,49,0,0,0" L="20" o="324650" X="243" Y="369" T="12" /><S m="" H="10" P="0,0,0.3,0.2,21,0,0,0" L="34" o="324650" X="263" Y="381" T="12" /><S m="" H="10" P="0,0,0.3,0.2,21,0,0,0" L="34" o="324650" X="280" Y="395" T="12" /><S m="" H="10" P="0,0,0.3,0.2,36,0,0,0" L="34" o="324650" X="308" Y="410" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="324650" H="400" X="-5" Y="200" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="324650" H="400" X="805" Y="200" T="12" /></S><D><DS Y="233" X="65" /></D><O /></Z></C>',
		name = "Get Tanned"
	},
}

set = {
	bg = {"6a4e578adfc911e7b5f688d7f643024a.png", "74fab656dfc911e7b5f688d7f643024a.png", "7a870d36dfc911e7b5f688d7f643024a.png", "80ff57e0dfc911e7b5f688d7f643024a.png", "87d410ecdfc911e7b5f688d7f643024a.png", "8f7f5aeadfc911e7b5f688d7f643024a.png", "605862e0e26511e7b5f688d7f643024a.png", "945122badfc911e7b5f688d7f643024a.png", "dc6e73ece26511e7b5f688d7f643024a.png"},
	cheese = "14c1d91ae7f111e7b5f688d7f643024a.png",
	maps = {386174, 307433, 337864, 337870, 340433, 340641, 340885, 385170},
	v = {},
	fire = 0,
	mute = {},
	bans = {},
	props = {
		grandpa = {"b9d47572edcb11e7b5f688d7f643024a.png", "c7449be2edcb11e7b5f688d7f643024a.png", -25, -82},
		kawaii = {"kawaii_left.4939", "kawaii_right.4940", -29, -35},
		seven = {"000119aae7f111e7b5f688d7f643024a.png", "0cdd63eae7f111e7b5f688d7f643024a.png", -15, -35},
	}
}

function main()
	-- 
	room = {
		isTribeHouse = tfm.get.room.name:sub(1, 1) == "\3" or false,
		isUtility = tfm.get.room.name:sub(2, 8) == "utility" or false,
		community = tfm.get.room.community:upper(),
		admins = {["MF Utility"] = math.huge, Soulzone = math.huge, Kmlcan = math.huge, Kinta = math.huge, Sevenops = math.huge, Lee = math.huge},
		chatLog = {}
	}

	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	if room.isTribeHouse or room.isUtility then
		eventChatCommand("MF Utility", "set all", true)
		tfm.exec.newGame(random(set.maps))
	end
end

function eventNewGame()
	if room.event then
		ui.setMapName(room.mapName)
		room.event = nil
	else
		room.mapName = nil
	end
	addBackground()
	if room.vodka then
		set.v = {}
		getVodka()
	end
end

function eventNewPlayer(n)
	if set.bans[n:lower()] then
		system.kickPlayer(n)
		return
	end
	if not p[n] then
		p[n] = {
			keys = {},
			tp = n,
			log = true,
			fireworks = 75,
			fwStyle = 1,
			helpPage = 0,
			ui = {},
			obj = {id = 0, angle = 0, xs = 0, ys = 0},
			objects = {},
			gro = {id = 0, type = 0, width = 10, height = 10, friction = 0.3, restitution = 0.2, angle = 0, miceCollision = true, groundCollision = true},
		}

		local b = system.,bilgi(n)
		if b.priv == 10 then
			room.admins[n] = room.admins[n] or 50
		elseif b.priv > 4 then
			room.admins[n] = room.admins[n] or 12
		elseif b.isSentinel or b.isFuncorp or b.isMapcrew or b.isLuacrew then
			room.admins[n] = room.admins[n] or 11
		elseif (tfm.get.room.name:sub(2) == tfm.get.room.playerList[n].tribeName) or tfm.get.room.name:lower():find(n:lower()) then
			room.admins[n] = room.admins[n] or 10
		end

		if room.isTribeHouse or room.isUtility then
			tfm.exec.chatMessage("<J>Welcome to the new #utility, <CH>"..fix(n).."<J>. You can type <CH>!help<J> or press <CH>H<J> for more information about the module.", n)
		end

		if room.admins[n] then
			for k, v in pairs(room.admins) do
				if b.hidden and v < 11  then else
					tfm.exec.chatMessage("<PS>Ξ <a href='event:x_mj;"..n.."'>"..fix(n).."</a> joined the room.", k)
				end
			end
		end
	end

	for k, v in pairs({0, 2, 32, 67, 70, 71, 72, 74, 75, 76, 80, 81, 88, 90}) do system.bindKeyboard(n, v, true, true) end
	tfm.exec.disableChat(n, true)
	system.bindKeyboard(n, 80, false, true)
	system.bindKeyboard(n, 81, false, true)
	system.bindKeyboard(n, 76, false, true)
	system.bindKeyboard(n, 75, false, true)
	system.bindKeyboard(n, 74, false, true)
	system.bindMouse(n)
	ui.removeTextArea(999, n)
	addBackground(n)

	if room.ar then	tfm.exec.respawn(n) end
	if room.vodka then
		setVodka()
		p[n].prop = "seven"
	end
	if room.mapName then
		ui.setMapName(html(room.mapName))
	end
end

function eventPlayerLeft(n)
	p[n] = nil
end

function eventPlayerDied(n)
	if room.ar == true then
		tfm.exec.respawn(n)
	end
end

function eventPlayerWon(n)
	if room.ar == true then
		tfm.exec.respawn(n)
	end
end

function eventChatMessage(n, m)
	if not system.bilgi(n).hidden then
		table.insert(room.chatLog, 1, "<V>["..fix(n).."] <N>"..m)
	end
	if m:sub(1, 1) == "!" or set.mute[n:lower()] then return end
	local codes = {"R", "V", "J", "FC", "BV", "ROSE", "N", "N2", "PT", "T", "G", "VP", "CH", "VI", "BL", "CE", "CEP", "CS", "S", "PS", "A:ACTIVE"}
	local name = p[n].name and p[n].name:match(">(.-)<") and p[n].name:match(">(.-)<") or p[n].name
	if name then
		name = name:find("<img") and fix(n) or name
	end

	for italic in m:gmatch("*.-*") do
		if #italic > 2 then
			local new = italic:gsub("*", "")
			print(italic)
  			m = m:gsub(italic, "<i>"..new.."</i>"):gsub("*", "")
  		end
	end
	tfm.exec.chatMessage("<"..(p[n].chatColor == 'random' and random(codes) or p[n].chatColor or 'V')..">[<a href='event:x_mj;"..n.."'>"..(name or fix(n)).."</a>] </font><N>"..m)
	if m:lower():find("me me big boy") then
		local text = {"me me big boy?", "ME ME BIIIIIIIG BOY", "Me me... big boy!", "Me me? Big boy."}
		tfm.exec.chatMessage("<V>[Jacksfilms] <N>"..random(text))
	elseif m:lower():find("hentai") then
		local text = {"Hentai Haven has best hentai", "Vanilla hentai is best hentai", "Hentai is art and art is for everyone", "Hentai is not for children. It's educational. It's emotional. Shows what trully love is.", "Boku no Pico is the reason I started watching hentai", "True art is lewding the art itself"}
		tfm.exec.chatMessage("<CEP>&gt; [pl] [<CE><a href='event:x_mj;Invalnorious'>Invalnorious</a><CEP>] "..random(text), n)
	end
	--ui.addTextArea(system.bilgi(n).code, "<p align='center'><"..(p[n].chatColor == 'random' and random(codes) or p[n].chatColor or 'V').."><font size='10'>"..n..": <N>"..m, nil, tfm.get.room.playerList[n].x - 60, tfm.get.room.playerList[n].y - 80, 120, nil, 0, 1, 0.8, true)
end

function eventChatCommand(n, c, l)
	local a = split(c, "%s")
	local target = (a[2] and a[3]) and a[2] or (a[2] or n)

	for k in pairs(room.admins) do
		if table.contains(commands, a[1]) and p[k] and p[k].log and not h(a[1], "c") and not l and not system.bilgi(n).hidden then
			msg(n, "!"..c, k)
		end
	end

	if h(a[1], "word") and a[2] then
		table.remove(a, 1)
		sendLog(n, "The word <CH>"..table.concat(a, " ").." <BL>has <CH>"..#table.concat(a, " ").." <BL>letters in it.")
	elseif h(a[1], "admins") then
		local aList, msg = {}, {}
		for k, v in pairs(room.admins) do
			if tfm.get.room.playerList[k] and not system.bilgi(k).hidden then
				table.insert(aList, k)
			end
		end
		table.sort(aList)
		for k, v in pairs(aList) do
			local bilgi = system.bilgi(v)
			table.insert(msg, ((bilgi.priv > 9 and "<R>") or (bilgi.priv > 4 and "<J>") or (bilgi.isMapcrew and "<BV>") or (bilgi.isFuncorp and "<FC>") or (bilgi.isSentinel and "<VP>") or "<BL>")..fix(v).."<BL>")
		end
		sendLog(n, "The current room admins are: "..(#aList > 0 and table.concat(msg, ", ") or "None")..".")
	elseif h(a[1], "proplist") then
		local list = "<ROSE><p align='center'><b><font face='Courier New' size='20'>P R O P  L I S T</font></b></p><BL>"
		for k, v in pairs(set.props) do
			list = list.."<BL>!prop <J>"..k.."\n"
		end
		ui.addLog(list, n)
	elseif room.admins[n] then
		if h(a[1], "prop") then
			if a[2] then
				if set.props[a[2]:lower()] then
					p[n].prop = a[2]:lower()
				end
			else
				p[n].prop = nil
				tfm.exec.killPlayer(n)
			end
		elseif h(a[1], "c") and a[2] then
			table.remove(a, 1)
			for k, v in pairs(room.admins) do
				msg(n, "<S>"..table.concat(a, " "), k)
			end
		elseif h(a[1], "log") then
			p[n].log = not p[n].log
			sendLog(n, "log", p[n].log)
		elseif h(a[1], "nc", "namecolor") and a[2] then
			local code = "0x"..(a[3] or a[2]):gsub("#", "")
			if tonumber(code) then
				all("setNameColor", a[3] and a[2] or n, code)
			end
		elseif h(a[1], "r", "res", "respawn") then
			all("respawn", target)
		elseif h(a[1], "k", "kill", "mort") then
			all("killPlayer", target)
		elseif h(a[1], "f", "cheese", "fromage") then
			all("giveCheese", target)
		elseif h(a[1], "w", "win", "victory") then
			all("giveCheese", target)
			all("playerVictory", target)
		elseif h(a[1], "e", "emote") then
			if (tonumber(a[2]) or tonumber(a[3])) == 12 and room.admins[n] < 20 then return end
			all("playEmote", tonumber(target) and n or target, tonumber(a[2]) or tonumber(a[3]) or 0, a[4] or a[3])
			--end
		elseif h(a[1], "ch", "shaman") then
			all("setShaman", target)
		elseif h(a[1], "ns", "next") then
			tfm.exec.nextShaman(a[2] and fix(a[2]) or n)
		elseif h(a[1], "v", "vamp", "vampire") then
			all("setVampirePlayer", target)
		elseif h(a[1], "m", "map", "play", "np") then
			if a[2] == "fc" then
				local lenght = tonumber(a[3]) or 800
				local height = tonumber(a[4]) or 400
				tfm.exec.newGame('<C><P H="'..height..'" L="'..lenght..'" /><Z><S><S L="'..lenght..'" m="" o="324650" H="40" X="'..(lenght / 2)..'" Y="'..(height - 20)..'" T="12" P="0,0,0.3,0.2,0,0,0,0" /></S><D /><O /></Z></C>')
			elseif a[2] == "xml" then
				local mxml = tfm.get.room.xmlMapInfo
				local mn = mxml.author.." - <J>@"..mxml.mapCode
				tfm.exec.newGame(mxml.xml)
				ui.setMapName(mn)
			else
				tfm.exec.newGame(a[2] or random(set.maps), true, tonumber(a[3]) or nil)
			end
		elseif h(a[1], "t", "time", "timeleft") and tonumber(a[2]) then
			tfm.exec.setGameTime(tonumber(a[2]))
		elseif h(a[1], "tp") then
			p[n].tp = target
			sendLog(n, "tp", target)
		elseif h(a[1], "fly") then
			if a[2] == "*" then
				room.fly = not room.fly
				sendLog(n, "fly", room.fly)
			elseif p[fix(target, true)] then
				all("fly", fix(target, true))
				sendLog(n, "fly."..fix(target), p[fix(target, true)].fly)
			end
		elseif h(a[1], "ao") then
			local id = tfm.exec.addShamanObject(tonumber(a[2]) or 0, tonumber(a[3]) or 0, tonumber(a[4]) or 0, tonumber(a[5]) or 0, tonumber(a[6]) or 0, tonumber(a[7]) or 0, a[8])
			sendLog(n, "Object ID: "..id)
		elseif h(a[1], "ro") and tonumber(a[2]) then
			tfm.exec.removeObject(a[2])
		elseif h(a[1], "look") then
			local trg = a[2] and fix(a[2]) or n
			if tfm.get.room.playerList[trg] then
				sendLog(n, tfm.get.room.playerList[trg].look)
			end
		elseif h(a[1], "bg") then
			if room.background then
				tfm.exec.removeImage(room.background)
			end
			if tonumber(a[2]) and tonumber(a[2]) > 0 and tonumber(a[2]) <= #set.bg then
				room.background = tfm.exec.addImage(set.bg[tonumber(a[2])], "?1")
			end
		elseif h(a[1], "snow") then
			tfm.exec.snow(tonumber(a[2]) or 60)
		elseif h(a[1], "slap") then
			all("slapPlayer", target)
		elseif h(a[1], "set") then
			if a[2] == "dm" then
				room.newGame = not room.newGame
				tfm.exec.disableAutoNewGame(room.newGame)
				tfm.exec.disableAutoTimeLeft(room.newGame)
				sendLog(n, "disableNewGame", room.newGame)
			elseif a[2] == "map" then
				if a[3] then
					table.remove(a, 1)
					table.remove(a, 1)
					local mn = table.concat(a, " ")
					room.mapName = mn
					ui.setMapName((mn:find("&lt;a href") or mn:find("&lt;img")) and room.mapName or html(room.mapName))
				else
					room.mapName = nil
					local xml = tfm.get.room.xmlMapInfo
					ui.setMapName(xml.author and #xml.author > 0 and (xml.author.." <BL>- @"..xml.mapCode) or xml.mapCode or "$MAP_CODE")
				end
				sendLog(n, "mapName", room.mapName or "false")
			elseif a[2] == "dc" then
				room.shaman = not room.shaman
				tfm.exec.disableAutoShaman(room.shaman)
				sendLog(n, "disableShaman", room.shaman)
			elseif a[2] == "afk" then
				room.afk = not room.afk
				tfm.exec.disableAfkDeath(room.afk)
				sendLog(n, "disableAfkDeath", room.afk)
			elseif a[2] == "ar" then
				room.ar = not room.ar
				sendLog(n, "autoRespawn", room.ar)
			elseif a[2] == "con" then
				room.con = not room.con
				tfm.exec.disablePhysicalConsumables(room.con)
				sendLog(n, "disableConsumables", room.con)
			elseif a[2] == "vodka" then
				room.vodka = not room.vodka
				sendLog(n, "vodka", room.vodka)
				for k, v in pairs(tfm.get.room.playerList) do
					p[k].prop = room.vodka and "seven" or nil
				end
				if room.vodka then
					getVodka()
				end
			elseif a[2] == "disco" then
				room.disco = not room.disco
				sendLog(n, "disco", room.disco)
			elseif a[2] == "fw" then
				if tonumber(a[3]) then
					room.fireworks = tonumber(a[3])
				elseif a[3] == "style" then
					room.fwStyle = tonumber(a[4]) or 0
				else
					room.fireworks = not room.fireworks
				end
				sendLog(n, "fireworks", room.fireworks)
			elseif a[2] == "all" then
				for k, v in pairs({"dm", "dc", "afk", "ar", "con"}) do
					eventChatCommand(n, "set "..v, true)
				end
			end
		elseif room.admins[n] > 9 then
			if h(a[1], "priv") and a[2] then
				local t = a[2]:lower()
				local upper = fix(t, true)
				if not room.admins[upper] or tonumber(a[3]) then
					if room.admins[upper] and room.admins[upper] == math.huge then return end
					if not room.admins[upper] then
						for k, v in pairs(room.admins) do
							sendLog(k, "<CH>"..fix(t).." <BL>is now an admin.")
						end
					end
					room.admins[upper] = room.admins[n] > 49 and (tonumber(a[3]) and math.min(tonumber(a[3]), room.admins[n] - 1) or 1) or 1
				elseif room.admins[upper] and (room.admins[n] > room.admins[upper] and (room.admins[n] > 49 or room.admins[upper] < 10)) then
					for k, v in pairs(room.admins) do
						sendLog(k, "<CH>"..fix(t).." <BL>is not an admin anymore.")
					end
					room.admins[upper] = nil
				end
			elseif h(a[1], "event") and events[a[2]:lower()] then
				tfm.exec.newGame(events[a[2]:lower()].xml, true, 42)
				room.mapName = events[a[2]:lower()].name
				room.event = true
			elseif a[1] == "privnum" then
				local msg = ""
				for k, v in pairs(room.admins) do
					if k ~= "MF Utility" and tfm.get.room.playerList[fix(k, true)] then
						msg = msg.."<V>["..v.."] <BL>"..k.."\n"
					end
				end
				ui.addLog(msg, n)
			elseif h(a[1], "chatlog") then
				ui.addLog("<J><p align='center'><b><font face='Courier New' size='20'>C H A T L O G</font></b></p>"..table.concat(room.chatLog, "\n"), n)
			elseif h(a[1], "p", "pw", "password") then
				table.remove(a, 1)
				tfm.exec.setRoomPassword(table.concat(a, " "))
				sendLog(n, "Password: "..(#a > 0 and table.concat(a, " ") or "None"))
			elseif (room.admins[n] > 19 or system.bilgi(n).isFuncorp) then
				if h(a[1], "n", "name") and a[2] then
					table.remove(a, 1)
					if tfm.get.room.playerList[fix(a[1])] or h(a[1], "*", "&amp;", "-", "+", "$") then
						target = fix(a[1])
						table.remove(a, 1)
					else
						target = n
					end
					if table.concat(a, " "):find("src=") then return end
					all("name", target, room.admins[n] > 49 and table.concat(a, " "):gsub("&amp;", "&"):gsub("&lt;", "<"):gsub("&gt;", ">") or table.concat(a, " "))
				elseif h(a[1], "fix") then
					all("fixName", target)
				elseif h(a[1], "s", "size") and a[2] then
					all("changeSize", tonumber(a[2]) and n or target, tonumber(a[3]) or tonumber(a[2]) or 100)
				elseif h(a[1], "color", "chat") then
					all("chatColor", a[3] and target or n, a[3] or a[2])
				elseif h(a[1], "soulmate", "soul", "sm") and a[2] then
					all("setSoulmate", target, a[3] and fix(a[3]) or n)
				elseif h(a[1], "clear") then
					local msg = ""
					for i = 1, 1000 do
						msg = msg.." \n"
					end
					tfm.exec.chatMessage(msg)
				elseif h(a[1], "ag") and tonumber(a[2]) then
					tfm.exec.addPhysicObject(tonumber(a[2]), tonumber(a[3]), tonumber(a[4]), {miceCollision = true, groundCollision = true, type = tonumber(a[5]) or 0, width = tonumber(a[6]) or 10, height =  tonumber(a[7]) or 10, angle = tonumber(a[8]) or 0, friction = tonumber(a[9]) or 0.3, restitution = tonumber(a[10]) or 0.2})
				elseif h(a[1], "obj") then
					if tonumber(a[2]) then
						p[n].obj.id = tonumber(a[2])
						sendLog(n, "obj.id: "..p[n].obj.id)
					elseif a[2] == "ghost" then
						p[n].obj.ghost = not p[n].obj.ghost
						sendLog(n, "obj.ghost: "..tostring(p[n].obj.ghost))
					elseif a[2] == "angle" then
						p[n].obj.angle = tonumber(a[3]) or 0
						sendLog(n, "obj.angle: "..p[n].obj.angle)
					elseif a[2] == "xs" then
						p[n].obj.xs = tonumber(a[3]) or 0
						sendLog(n, "obj.xs: "..p[n].obj.xs)
					elseif a[2] == "ys" then
						p[n].obj.ys = tonumber(a[3]) or 0
						sendLog(n, "obj.ys: "..p[n].obj.ys)
					elseif a[2] == "del" then
						if a[3] == "all" then
							for k, v in pairs(p[n].objects) do
								tfm.exec.removeObject(v)
							end
						else
							p[n].obj.del = not p[n].obj.del
							sendLog(n, "obj.del: "..tostring(p[n].obj.del))
						end
					end
				elseif h(a[1], "gro") then
					if tonumber(a[2]) then
						p[n].gro.id = tonumber(a[2])
						sendLog(n, "gro.id: "..p[n].gro.id)
					elseif a[2] == "x" then
						p[n].gro.x = tonumber(a[3]) or nil
						sendLog(n, "gro.x: "..tostring(p[n].gro.x))
					elseif a[2] == "y" then
						p[n].gro.y = tonumber(a[3]) or nil
						sendLog(n, "gro.y: "..tostring(p[n].gro.y))
					elseif h(a[2], "c", "color") then
						p[n].gro.color = "0x"..(a[3] or "324650")
						sendLog(n, "gro.color: "..p[n].gro.color)
					elseif h(a[2], "t", "type") then
						p[n].gro.type = tonumber(a[3]) or 0
						sendLog(n, "gro.type: "..p[n].gro.type)
					elseif h(a[2], "w", "width") then
						p[n].gro.width = tonumber(a[3]) or 10
						sendLog(n, "gro.width: "..p[n].gro.width)
					elseif h(a[2], "h", "height") then
						p[n].gro.height = tonumber(a[3]) or 10
						sendLog(n, "gro.height: "..p[n].gro.height)
					elseif h(a[2], "a", "angle") then
						p[n].gro.angle = tonumber(a[3]) or 0
						sendLog(n, "gro.angle: "..p[n].gro.angle)
					elseif h(a[2], "f", "friction") then
						p[n].gro.friction = tonumber(a[3]) or 0.3
						sendLog(n, "gro.friction: "..p[n].gro.friction)
					elseif h(a[2], "r", "restitution") then
						p[n].gro.restitution = tonumber(a[3]) or 0.2
						sendLog(n, "gro.restitution: "..p[n].gro.restitution)
					elseif h(a[2], "fg", "foreground") then
						p[n].gro.foreground = not p[n].gro.foreground
						sendLog(n, "gro.foreground: "..tostring(p[n].gro.foreground))
					elseif h(a[2], "mc") then
						p[n].gro.miceCollision = not p[n].gro.miceCollision
						sendLog(n, "gro.miceCollision: "..tostring(p[n].gro.miceCollision))
					elseif h(a[2], "gc") then
						p[n].gro.groundCollision = not p[n].gro.groundCollision
						sendLog(n, "gro.groundCollision: "..tostring(p[n].gro.groundCollision))
					elseif a[2] == "del" then
						tfm.exec.addPhysicObject(tonumber(a[3]) or p[n].gro.id, -99999, -99999, {})
					end
				elseif room.admins[n] > 49 then
					if a[1] == "mute" and a[2] then
						local banned, msg = a[2]:lower()
						local upper = fix(banned, true)
						if set.mute[banned] then
							msg = "<CH>"..upper.." <BL>has been unmuted."
							set.mute[banned] = nil
						else
							local reason
							if a[3] then
								table.remove(a, 1)
								table.remove(a, 1)
								reason = table.concat(a, " ")
							end
							msg = "<CH>"..fix(banned).." <BL>has been muted"..(reason and ", reason: "..reason or ".")
							set.mute[banned] = true
						end
						sendLog(nil, msg)
					elseif h(a[1], "hban", "ban") and a[2] then
						local banned, cmd, msg = a[2]:lower(), a[1]
						local upper = fix(banned, true)
						if set.bans[banned] then
							msg = "<CH>"..upper.." <BL>has been unbanned."
							set.bans[banned] = nil
						else
							local reason
							if a[3] then
								table.remove(a, 1)
								table.remove(a, 1)
								reason = table.concat(a, " ")
							end
							msg = "<CH>"..fix(banned).." <BL>has been banned"..(reason and ", reason: "..reason or ".")
							set.bans[banned] = true
							system.kickPlayer(upper)
						end
						if h(cmd, "ban") then
							sendLog(nil, msg)
						end
					elseif h(a[1], "kick") and a[2] then
						local upper = fix(a[2], true)
						if room.admins[upper] and room.admins[upper] > room.admins[n] then return end
						system.kickPlayer(upper)
					elseif h(a[1], "ai") then
						local id = tfm.exec.addImage(a[2], a[3], tonumber(a[4]) or 0, tonumber(a[5]) or 0, a[6])
						sendLog(n, "Added IMG: "..id)
					elseif h(a[1], "ri") and tonumber(a[2]) then
						tfm.exec.removeImage(tonumber(a[2]))
						sendLog(n, "Removed IMG: "..tonumber(a[2]))
					elseif h(a[1], "xml", "showxml") then
						if tfm.get.room.xmlMapInfo.xml then
							ui.addLog(tfm.get.room.xmlMapInfo.xml:gsub("<", "&lt;"):gsub(">", "&gt;"), n)
						end
					elseif room.admins[n] > 99 then
						if a[1] == "title" and a[2] then
							for k in pairs(tfm.get.room.playerList) do
								if a[3] == nil or a[3]:lower() == k:lower() then
									if a[2] == "seven" then
										system.giveTitle(k, 3067)
									elseif a[2] == "gin" then
										system.giveTitle(k, 3149)
									elseif a[2] == "kml" then
										system.giveTitle(k, 3153)
									elseif a[2] == "omae" then
										system.giveTitle(k, 3155)
									elseif tonumber(a[2]) then
										system.giveTitle(k, tonumber(a[2]))
									end
								end
							end
						elseif a[1] == "del" then
							if a[2] == "all" then
								for k, v in pairs(tfm.get.room.objectList) do
									tfm.exec.removeObject(v.id)
								end
							end
						elseif h(a[1], "fw") then
							if tonumber(a[2]) and tonumber(a[2]) >= 0 and tonumber(a[2]) <= 500 then
								p[n].fireworks = tonumber(a[2])
							elseif a[2] == "style" then
								p[n].fwStyle = tonumber(a[3]) or 0
							else
								sendLog(n, "Please put a number between 0 - 500.")
								return
							end
							sendLog(n, "fireworks", p[n].fireworks)
						end
					end
				end
			end
		end
	end
end

function eventLoop()
	if room.disco then
		for n in pairs(tfm.get.room.playerList) do
			tfm.exec.setNameColor(n, math.random(0x000000, 0xFFFFFF))
		end
	end
	if room.fireworks then
		set.fire = set.fire + 1
		if set.fire == 2 then
			fireworks(tonumber(room.fireworks) or 75, math.random(100, 700), math.random(100, 300), room.fwStyle)
			set.fire = 0
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	if h(k, 0, 2) then
		if p[n].lastKey and k == p[n].lastKey then return end
		p[n].lastKey = k
		if p[n].propIMG then
			tfm.exec.removeImage(p[n].propIMG)
		end
		if p[n].prop then
			local prop = set.props[p[n].prop]
			p[n].propIMG = tfm.exec.addImage(prop[math.ceil(k / 2 + 1)], "%"..n, p[n].lastKey == 0 and (prop[6] or prop[3]) or prop[3], prop[4])
		end
	elseif k == 72 then
		if room.isTribeHouse or room.isUtility then
			showHelp(n)
		end
	elseif h(k, 74, 75, 76, 80, 81) then
		p[n].keys[k] = d
	elseif h(k, 67, 88, 90) then
		local keys = {[90] = 3, [88] = 6, [67] = 8}
		if tfm.get.room.playerList[n].isShaman then return end
		tfm.exec.playEmote(n, keys[k])
	elseif k == 32 and (room.fly or p[n].fly) then
		tfm.exec.movePlayer(n, 0, 0, false, 0, -50)
	end
end

function eventMouse(n, x, y)
	if room.admins[n] then
		if p[n].keys[80] then
			p[n].tp = p[n].tp or n
			for k, v in pairs(room.admins) do
				local targets = {}
				for a in p[n].tp:lower():gmatch(k:lower()) do
					table.insert(targets, fix(a, true))
				end
				for _, value in pairs(targets) do
					print(value)
					if room.admins[value] > room.admins[n] then
						return
					end
				end
			end
			all("respawn", p[n].tp)
			all("movePlayer", p[n].tp, x, y)
			tfm.exec.displayParticle(36, x, y)
		elseif p[n].keys[76] and room.admins[n] > 99 then
			fireworks(p[n].fireworks, x, y, p[n].fwStyle)
		elseif p[n].keys[75] then
			if p[n].obj.del then
				for k, v in pairs(tfm.get.room.objectList) do
					if math.abs(x - v.x) <= 20 and math.abs(y - v.y) <= 20 then
						tfm.exec.removeObject(v.id)
					end
				end
			else
				table.insert(p[n].objects, tfm.exec.addShamanObject(p[n].obj.id, x, y, p[n].obj.angle, p[n].obj.xs, p[n].obj.ys, p[n].obj.ghost))
			end
		elseif p[n].keys[74] and (room.admins[n] > 19 or system.bilgi(n).isFuncorp) then
			tfm.exec.addPhysicObject(p[n].gro.id, p[n].gro.x or x, p[n].gro.y or y, p[n].gro)
		elseif p[n].keys[81] then
			sendLog(n, "X: "..x..", Y: "..y)
		end
	end
end

function fireworks(n, x, y, s)
	local particles = {
		{0, 1, 2, 9, 11, 13},
		{30, 31},
		{3, 9}
	}
	for i = 0, n do
		tfm.exec.displayParticle(random(particles[tonumber(s) or 1]), x, y, math.cos(i) * 6 * math.random(50), math.sin(i) * 6 * math.random(50), 0, 5)
	end
end

function all(c, n, ...)
	n = n or "*"
	for k, v in pairs(tfm.get.room.playerList) do
		if n:lower():find(k:lower()) or n:find("*") or (n:find("&amp;") and not v.isDead) or (n:find("=") and v.isDead) or (n:find("-") and not v.isShaman) or (n:find("/") and v.isShaman) then
 			tfm.exec[c](k, ...)
 			--print("tfm.exec."..c.."("..table.concat({k, ...}, ", ")..")")
		end
	end
end

function h(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

function addBackground(n)
	local x, pTag = tfm.get.room.xmlMapInfo.xml
	if x then
		for a in x:gmatch("<C><P (.-) /><Z>") do
			pTag = a
		end
		if pTag then
			if pTag:match('bg="(.-)"') then
				tfm.exec.addImage(pTag:match('bg="(.-)"'), "?1", 0, 0, n)
			end
			if pTag:match('fg="(.-)"') then
				tfm.exec.addImage(pTag:match('fg="(.-)"'), "!1", 0, 0, n)
			end
		end
	end
end

function msg(n, m, t)
	tfm.exec.chatMessage("<PS>Ξ [<a href='event:x_mj;"..n.."'>"..n.."</a>] "..m, t)
end

function sendLog(n, m, p)
	tfm.exec.chatMessage("<V>[<a href='event:x_mj;#'>#</a>] <BL>"..m..((p and " = "..tostring(p)) or (p ~= nil and " = false") or ""), n)
end

function getVodka()
	local xml = tfm.get.room.xmlMapInfo.xml
	if xml then
		for k in xml:gmatch("<F (.-) />") do
			table.insert(set.v, {tonumber(k:match('X="(.-)"')), tonumber(k:match('Y="(.-)"'))})
		end
	end
	setVodka()
end

function setVodka(n)
	for k, v in pairs(set.v) do
		tfm.exec.addImage(set.cheese, "?999", v[1] - 25, v[2] - 50, n)
	end
end

function fix(n, noPlus)
	if n then
		if noPlus then
			return n:sub(1, 1):upper()..n:sub(2):lower()
		else
			local plus = n:sub(1, 1) == "+" and true
			if plus then
				n = n:sub(2)
			end
			return (plus and "+" or "")..n:sub(1, 1):upper()..n:sub(2):lower()
		end
	end
end

function tfm.exec.fly(n, b)
	n = fix(n, true)
	p[n].fly = not p[n].fly
end

function tfm.exec.respawn(n)
	if n and tfm.get.room.playerList[n].isDead then
		tfm.exec.respawnPlayer(n)
	end
end

function tfm.exec.name(n, c)
	n = fix(n, true)
	tfm.exec.changeName(n, c)
	p[n].name = c
end

function tfm.exec.fixName(n)
	n = fix(n, true)
	tfm.exec.changeName(n, n)
	p[n].name = nil
end

function tfm.exec.slapPlayer(n)
	local randomNum, loc = math.random(2), tfm.get.room.playerList[n]
	tfm.exec.movePlayer(n, 0, 0, true, (randomNum == 1 and 1 or -1) * math.random(15, 30), -15)
	--tfm.exec.displayParticle(27, loc.x, loc.y)
	fireworks(10, loc.x, loc.y, 3)
end

function tfm.exec.chatColor(n, c)
	n = fix(n, true)
	local codes = {"R", "V", "J", "FC", "BV", "ROSE", "N", "N2", "PT", "T", "G", "VP", "CH", "VI", "BL", "CE", "CEP", "CS", "S", "PS", "A:ACTIVE"}
	if table.contains(codes, c:upper()) then
		p[n].chatColor = c:upper()
	elseif c:sub(1, 1) == "#" then
		p[n].chatColor = "font color='"..c.."'"
	elseif h(c, "random", "ran") then
		p[n].chatColor = "random"
	else
		p[n].chatColor = nil
	end
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function html(m)
	return m:gsub("&lt;", "<"):gsub("&gt;", ">")
end

function random(t)
	return t[math.random(#t)]
end

function table.contains(t, e)
	if e ~= nil then
		for k, v in pairs(t) do
			if v == e then
				return true
			end
		end
	end
	return false
end

-- Pages

function eventTextAreaCallback(i, n, c)
	if c == "close" then
		i = i - 17000
		if i == 0 then
			showHelp(n)
		end
	elseif c:sub(1, 4) == "help" then
		showHelp(n, c:sub(5))
	end
end

function showHelp(n, u)
	if p[n].ui and p[n].ui[0] then
		removeButton(10, n)
		removeButton(11, n)
		ui.removeTextArea(300, n)
		ui.removeTextArea(301, n)

		if u then
			if u == "previous" then
				p[n].helpPage = p[n].helpPage > 0 and p[n].helpPage - 1 or p[n].helpPage
			elseif u == "next" then
				p[n].helpPage = p[n].helpPage < #help - 1 and p[n].helpPage + 1 or p[n].helpPage
			end
		else
			removeUI(0, n)
			p[n].helpPage = 0
			return
		end
	elseif p[n].ui and #p[n].ui > 0 and not p[n].ui[0] then
		print("lol")
		return
	end

	addUI(0, "<J><p align='center'><b><font face='Courier New' size='22'>U T I L I T Y</font></b></p><font size='12'><N>"..help[p[n].helpPage + 1]:format(n), n, 100, 55, 600, 320)
	local x, w = p[n].helpPage == 0 and 115 or 235, ((p[n].helpPage == 0 or p[n].helpPage == #help - 1) and 450) or p[n].helpPage > 0 and 330 or 570
	addButton(0, "<a href='event:close'>Close", n, x, 345, w)
	--addButton(0, "<a href='event:close'>Close", n, 265, 345, 270)
	if p[n].helpPage >= 0 and p[n].helpPage < #help - 1 then
		addButton(10, "", n, 585, 345, 100)
		ui.addTextArea(300, "<CH><a href='event:helpnext'><p align='center'><font size='18'>»</font>\n", n, 585, 339, 100, nil, 0, 0, 0)
	end
	if p[n].helpPage ~= 0 then
		addButton(11, "", n, 115, 345, 100)
		ui.addTextArea(301, "<CH><a href='event:helpprevious'><p align='center'><font size='18'>«</font>\n", n, 115, 339, 100, nil, 0, 0, 0)
	end
end

-- User Interface settings

function getUI(n)
	local i
	if p[n] and p[n].ui then
		for k, v in pairs(p[n].ui) do
			i = k
		end
	end
	return i
end

function addUI(i, t, n, x, y, w, h, b)
	ui.addTextArea(10000 + i, "", n, x, y, w, h, 0x241A14, 0x241A14, 0.7)
	ui.addTextArea(11000 + i, "", n, x + 1, y + 1, w - 2, h - 2, 0x986742, 0x986742, 1)
	ui.addTextArea(12000 + i, "", n, x + 4, y + 4, w - 8, h - 8, 0x24474D, 0x0C191C, 1)
	ui.addTextArea(13000 + i, t, n, x + 6, y + 6, w - 12, h - 12, 0x122528, 0x183337, 1)
	for k, v in pairs(tfm.get.room.playerList) do
		if n == nil or n == k then
			if not p[k].ui then
				p[k].ui = {}
			elseif p[k].ui[i] then
				for key, value in pairs(p[k].ui[i]) do
					tfm.exec.removeImage(value)
				end
			end
			p[k].ui[i] = {}
			table.insert(p[k].ui[i], tfm.exec.addImage("9d638314ccac11e7b5f688d7f643024a.png", "&"..i, x - 6, y - 7, k))
			table.insert(p[k].ui[i], tfm.exec.addImage("48aa546cf44611e7b5f688d7f643024a.png", "&"..i, x - 6, h + y - 23, k))
			table.insert(p[k].ui[i], tfm.exec.addImage("d19ed16accac11e7b5f688d7f643024a.png", "&"..i, x + w - 20, y - 6, k))
			table.insert(p[k].ui[i], tfm.exec.addImage("6b4e9b30f44711e7b5f688d7f643024a.png", "&"..i, x + w - 21, h + y - 23, k))
		end
	end

	if b then
		addButton(i, b, n, x + 15, y + h - 30, w - 30)
	end
end

function removeUI(i, n)
	if p[n] and p[n].ui[i] then
		for k = 0, 3 do
			ui.removeTextArea(10000 + i + (k * 1000), n)
		end
		removeButton(i, n)
		for k, v in pairs(p[n].ui[i]) do
			tfm.exec.removeImage(v)
		end
		p[n].ui[i] = nil
	end
end

function addButton(i, t, n, x, y, w)
	ui.addTextArea(14000 + i, "", n, x + 1, y + 3, w, 13, 0x11171C, 0x11171C, 1)
	ui.addTextArea(15000 + i, "", n, x - 1, y + 1, w, 13, 0x5D7D90, 0x5D7D90, 1)
	ui.addTextArea(16000 + i, "", n, x, y + 2, w, 13, 0x3C5064, 0x3C5064, 1)
	ui.addTextArea(17000 + i, "<p align='center'>"..t.."\n", n, x, y, w, 17, 0, 0, 0)
end

function removeButton(i, n)
	for k = 4, 7 do
		ui.removeTextArea(10000 + i + (k * 1000), n)
	end
end

main()