admins = {["+wanheda"] = 1, Kmlcan = math.huge, Lee = 1, Kinta = 1, Yolocookies = 1, Sevenops = math.huge, Chomar = 1, Majestik = math.huge}
p = {}
room = {}
commands = {"admins", "tright", "vodka", "title", "proplist", "set", "snow", "prop", "bg", "fly", "a", "look", "p", "pw", "password", "lines", "ns", "next", "ch", "shaman", "v", "vampire", "vamp", "fix", "n", "name", "s", "size", "r", "res", "respawn", "m", "map", "play", "log", "k", "kill", "mort", "c", "cheese", "w", "win", "victory", "ai", "ri", "t", "time", "bind", "tp", "e", "emote", "con"}
backgrounds = {"6a4e578adfc911e7b5f688d7f643024a.png", "74fab656dfc911e7b5f688d7f643024a.png", "7a870d36dfc911e7b5f688d7f643024a.png", "80ff57e0dfc911e7b5f688d7f643024a.png", "87d410ecdfc911e7b5f688d7f643024a.png", "8f7f5aeadfc911e7b5f688d7f643024a.png", "605862e0e26511e7b5f688d7f643024a.png", "945122badfc911e7b5f688d7f643024a.png", "dc6e73ece26511e7b5f688d7f643024a.png"}

set = {
	cheese = "14c1d91ae7f111e7b5f688d7f643024a.png",
	v = {}
}

props = {
	seven = {"000119aae7f111e7b5f688d7f643024a.png", "0cdd63eae7f111e7b5f688d7f643024a.png", -15, -35}
}




function main()
	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	for k, v in pairs(commands) do
		system.disableChatCommandDisplay(v)
	end
end

function eventNewGame()
	room.mapName = nil
	if room.vodka then
		set.v = {}
		getVodka()
	end
end

function eventNewPlayer(n)
	if not p[n] then
		p[n] = {
			keys = {},
			tp = n,
			log = true
		}

		local b = system.bilgi(n)
		if b.isSentinel or b.isFuncorp or b.isMapcrew or b.isLuacrew or b.priv > 4 then
			admins[n] = admins[n] or 1
		end

		if admins[n] then
			for k, v in pairs(admins) do
				tfm.exec.chatMessage("<PS>Ξ <a href='event:x_mj;"..n.."'>"..fix(n).."</a> joined the room.", k)
			end
		end
	end

	for k, v in pairs({0, 2, 32, 75, 80, 70, 71, 90, 88, 67}) do system.bindKeyboard(n, v, true, true) end
	system.bindKeyboard(n, 80, false, true)
	system.bindMouse(n)

	if room.ar then	tfm.exec.respawn(n) end
	if room.vodka then
		setVodka()
		p[n].prop = "seven"
	end
	if room.mapName then
		ui.setMapName(html(room.mapName))
	end
end

function eventPlayerLeft(n)
	p[n] = nil
	if admins[n] then
		for k, v in pairs(admins) do
			tfm.exec.chatMessage("<PS>Ξ <a href='event:x_mj;"..n.."'>"..fix(n).."</a> left the room.", k)
		end
	end
end

function eventPlayerDied(n)
	if room.ar == true then
		tfm.exec.respawn(n)
	end
end

function eventPlayerWon(n)
	if room.ar == true then
		tfm.exec.respawn(n)
	end
end

function eventChatCommand(n, c, l)
	local a = split(c, "%s")

	-- !e name emote
	local target = (a[2] and a[3]) and a[2] or (a[2] or n)
	--sendLog(n, c.." | "..tostring(target))
	--[[if a[2] then
		if a[2] == "*" then
			target = nil
		elseif p[fix(a[2])] then
			target = fix(a[2])
		else
			target = n
		end
	else
		target = n
	end]]

	for k in pairs(admins) do
		if table.contains(commands, a[1]) and p[k] and p[k].log and not h(a[1], "a") and not l then
			msg(n, "!"..c, k)
		end
	end

	if 	h(a[1], "prop") then
		if a[2] and room.prop then
			if props[a[2]:lower()] then
				p[n].prop = a[2]:lower()
			end
		else
			p[n].prop = nil
			tfm.exec.killPlayer(n)
		end
	elseif h(a[1], "admins") then
		local aList = {}
		for k, v in pairs(admins) do
			if tfm.get.room.playerList[k] then
				table.insert(aList, k)
			end
		end
		sendLog(n, table.concat(aList, ", "))
	elseif h(a[1], "proplist") then
		local list = "<ROSE><p align='center'><b><font face='Courier New' size='20'>P R O P  L I S T</font></b></p><BL>"
		for k, v in pairs(props) do
			list = list.."<BL>!prop <J>"..k.."\n"
		end
		ui.addLog(list, n)
	elseif admins[n] then
		if h(a[1], "log") then
			p[n].log = not p[n].log
			sendLog(n, "log", p[n].log)
		elseif h(a[1], "a") and a[2] then
			table.remove(a, 1)
			tfm.exec.chatMessage("<R>• [<a href='event:x_mj;"..n.."'>"..n.."</a>] <N>"..table.concat(a, " "))
		elseif h(a[1], "r", "res", "respawn") then
			all("respawn", target)
		elseif h(a[1], "k", "kill", "mort") then
			all("killPlayer", target)
		elseif h(a[1], "c", "cheese") then
			all("giveCheese", target)
		elseif h(a[1], "w", "win", "victory") then
			all("giveCheese", target)
			all("playerVictory", target)
		elseif h(a[1], "e", "emote") then
			all("playEmote", tonumber(target) and n or target, tonumber(a[2]) or tonumber(a[3]) or 0, a[4] or a[3])
		elseif h(a[1], "ch", "shaman") then
			all("setShaman", target)
		elseif h(a[1], "ns", "next") then
			tfm.exec.nextShaman(a[2] and fix(a[2]) or n)
		elseif h(a[1], "n", "name") and a[2] then
			table.remove(a, 1)
			if tfm.get.room.playerList[fix(a[1])] or h(a[1], "*", "&amp;", "-", "+", "$") then
				target = fix(a[1])
				table.remove(a, 1)
			else
				target = n
			end
			all("changeName", target, table.concat(a, " "):gsub("&amp;", "&"):gsub("&lt;", "<"):gsub("&gt;", ">"))
		elseif h(a[1], "fix") then
			all("fixName", target)
		elseif h(a[1], "s", "size") and a[2] then
			all("changeSize", tonumber(a[2]) and n or target, tonumber(a[3]) or tonumber(a[2]) or 100)
		elseif h(a[1], "v", "vamp", "vampire") then
			all("setVampirePlayer", target)
		elseif h(a[1], "p", "pw", "password") then
			table.remove(a, 1)
			tfm.exec.setRoomPassword(table.concat(a, " "))
			sendLog(n, "Password: "..(#a > 0 and table.concat(a, " ") or "None"))
		elseif h(a[1], "m", "map", "play") then
			if a[2] == "fc" then
				local lenght = tonumber(a[3]) or 800
				local height = tonumber(a[4]) or 400
				tfm.exec.newGame('<C><P H="'..height..'" L="'..lenght..'" /><Z><S><S L="'..lenght..'" o="324650" H="40" X="'..(lenght / 2)..'" Y="'..(height - 20)..'" T="12" P="0,0,0.3,0.2,0,0,0,0" /></S><D /><O /></Z></C>')
			elseif a[2] == "xml" then
				local mxml = tfm.get.room.xmlMapInfo
				local mn = mxml.author.." - <J>@"..mxml.mapCode
				tfm.exec.newGame(mxml.xml)
				ui.setMapName(mn)
			else
				tfm.exec.newGame(a[2] or 386174)
			end
		elseif h(a[1], "t", "time") and tonumber(a[2]) then
			tfm.exec.setGameTime(tonumber(a[2]))
		elseif h(a[1], "tright") then
			ui.removeTextArea(999, n)
		elseif h(a[1], "ai") then
			local id = tfm.exec.addImage(a[2], a[3], tonumber(a[4]) or 0, tonumber(a[5]) or 0, a[6])
			sendLog(n, "Added IMG: "..id)
		elseif h(a[1], "ri") and tonumber(a[2]) then
			tfm.exec.removeImage(tonumber(a[2]))
			sendLog(n, "Removed IMG: "..tonumber(a[2]))
		elseif h(a[1], "tp") then
			p[n].tp = target
			sendLog(n, "tp", target)
		elseif h(a[1], "fly") then
			if a[2] == "*" then
				room.fly = not room.fly
				sendLog(n, "fly", room.fly)
			elseif p[fix(target)] then
				all("fly", fix(target))
				sendLog(n, "fly."..fix(target), p[fix(target)].fly)
			end
		elseif h(a[1], "con") and a[2] then
			local pN = p[fix(a[2])]
			system.giveConsumables(pN and a[2] or n, pN and tonumber(a[3]) or tonumber(a[2]) or 0, pN and tonumber(a[4]) or tonumber(a[3]) or 1)
		elseif h(a[1], "look") then
			local trg = a[2] and fix(a[2]) or n
			if tfm.get.room.playerList[trg] then
				sendLog(n, tfm.get.room.playerList[trg].look)
			end
		elseif h(a[1], "bg") then
			if room.background then
				tfm.exec.removeImage(room.background)
			end
			if tonumber(a[2]) and tonumber(a[2]) > 0 and tonumber(a[2]) <= #backgrounds then
				room.background = tfm.exec.addImage(backgrounds[tonumber(a[2])], "?1")
			end
		elseif h(a[1], "snow") then
			tfm.exec.snow(tonumber(a[2]) or 10)
		elseif h(a[1], "set") then
			if a[2] == "dm" then
				room.newGame = not room.newGame
				tfm.exec.disableAutoNewGame(room.newGame)
				tfm.exec.disableAutoTimeLeft(room.newGame)
				sendLog(n, "disableNewGame", room.newGame)
			elseif a[2] == "map" then
				if a[3] and not room.mapName then
					table.remove(a, 1)
					table.remove(a, 1)
					room.mapName = table.concat(a, " ")
					ui.setMapName(html(room.mapName))
				else
					room.mapName = nil
				end
				sendLog(n, "mapName", room.mapName)
			elseif a[2] == "dc" then
				room.shaman = not room.shaman
				tfm.exec.disableAutoShaman(room.shaman)
				sendLog(n, "disableShaman", room.shaman)
			elseif a[2] == "afk" then
				room.afk = not room.afk
				tfm.exec.disableAfkDeath(room.afk)
				sendLog(n, "disableAfkDeath", room.afk)
			elseif a[2] == "ar" then
				room.ar = not room.ar
				sendLog(n, "autoRespawn", room.ar)
			elseif a[2] == "con" then
				room.con = not room.con
				tfm.exec.disablePhysicalConsumables(room.con)
				sendLog(n, "disableConsumables", room.con)
			elseif a[2] == "vodka" then
				room.vodka = not room.vodka
				sendLog(n, "vodka", room.vodka)
				for k, v in pairs(tfm.get.room.playerList) do
					p[k].prop = room.vodka and "seven" or nil
				end
				if room.vodka then
					getVodka()
				end
			elseif a[2] == "all" then
				for k, v in pairs({"dm", "dc", "afk", "ar", "con"}) do
					eventChatCommand(n, "set "..v, true)
				end
			end
		elseif admins[n] > 99 then
			if a[1] == "title" then
				for k in pairs(tfm.get.room.playerList) do
					if a[2] == "seven" then
						system.giveTitle(k, 3067)
					elseif a[2] == "gin" then
						system.giveTitle(k, 3149)
					end
				end
			end
		end
	end
end

function eventKeyboard(n, k, d, x, y)
	if h(k, 0, 2) then
		if p[n].lastKey and k == p[n].lastKey then return end
		p[n].lastKey = k
		if p[n].propIMG then
			tfm.exec.removeImage(p[n].propIMG)
		end
		if p[n].prop then
			local prop = props[p[n].prop]
			p[n].propIMG = tfm.exec.addImage(prop[math.ceil(k / 2 + 1)], "%"..n, p[n].lastKey == 0 and (prop[6] or prop[3]) or prop[3], prop[4])
		end
	elseif k == 80 then
		p[n].keys[k] = d
	elseif k == 90 then -- Z
		tfm.exec.playEmote(n, 3)
	elseif k == 88 then -- X
		tfm.exec.playEmote(n, 6)
	elseif k == 67 then -- C
		tfm.exec.playEmote(n, 8)
	elseif k == 32 and (room.fly or p[n].fly) then
		tfm.exec.movePlayer(n, 0, 0, false, 0, -50)
	end
end

function eventMouse(n, x, y)
	if p[n].keys[80] and admins[n] then
		--tfm.exec.movePlayer(p[n].tp, x, y)
		--local target = p[n].tp == "*" and nil or p[n].tp
		all("respawn", p[n].tp)
		all("movePlayer", p[n].tp, x, y)
	end
end

function all(c, n, ...)
	n = n or "*"
	for k, v in pairs(tfm.get.room.playerList) do
		if n:lower():find(k:lower()) or n:find("*") or (n:find("&amp;") and not v.isDead) or (n:find("=") and v.isDead) or (n:find("-") and not v.isShaman) or (n:find("+") and v.isShaman) then
 			tfm.exec[c](k, ...)
		end
	end
end

function h(n, ...)
	for k, v in pairs({...}) do
		if v == n then
			return true
		end
	end
end

function msg(n, m, t)
	tfm.exec.chatMessage("<PS>Ξ [<a href='event:x_mj;"..n.."'>"..n.."</a>] "..m, t)
end

function sendLog(n, m, p)
	tfm.exec.chatMessage("<V>[#] <BL>"..m..((p and " = "..tostring(p)) or (p ~= nil and " = false") or ""), n)
end

function getVodka()
	local xml = tfm.get.room.xmlMapInfo.xml
	if xml then
		for k in xml:gmatch("<F (.-) />") do
			table.insert(set.v, {tonumber(k:match('X="(.-)"')), tonumber(k:match('Y="(.-)"'))})
		end
	end
	setVodka()
end

function setVodka(n)
	for k, v in pairs(set.v) do
		tfm.exec.addImage(set.cheese, "?999", v[1] - 25, v[2] - 50, n)
	end
end

function fix(n)
	if n then
		local plus = n:sub(1, 1) == "+" and true
		if plus then
			n = n:sub(2)
		end
		return (plus and "+" or "")..n:sub(1, 1):upper()..n:sub(2):lower()
	end
end

function tfm.exec.fly(n, b)
	p[n].fly = not p[n].fly
end

function tfm.exec.respawn(n)
	if n and tfm.get.room.playerList[n].isDead then
		tfm.exec.respawnPlayer(n)
	end
end

function tfm.exec.fixName(n)
	tfm.exec.changeName(n, n)
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function html(m)
	return m:gsub("&lt;", "<"):gsub("&gt;", ">")
end

function table.contains(t, e)
	if e ~= nil then
		for k, v in pairs(t) do
			if v == e then
				return true
			end
		end
	end
	return false
end

main()