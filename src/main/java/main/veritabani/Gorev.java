package veritabani;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * Peynir 0
 * Birincilik 1
 * Bootcamp 2
 * Hayatta kalma (Survivor) 3
 * Hayatta kalma (Kutier) 4
 * Round süresi 5
 * Fare kurtar 6
 * Divine Fare kurtar 7
 * Hard Fare kurtar 8
 * Defilante Puan 9
 * Kutier öldür 10
 * Survivor öldür 11
 * Racing'de birincilik  12
 */
public class Gorev {
    public ObjectId _id;
    public int pId;
    public boolean bitti;
    public String aciklama;
    public Document odul;
    public List<Document> oduller;
    public int ilerleme;
    public int son;
    public int bitis;
    public int tip;
    public Gorev(){

    }

    public Gorev(String aciklama, Document odul, int son, int bitis, int tip) {
        this.aciklama = aciklama;
        this.odul = odul;
        this.son = son;
        this.bitis = bitis;
        this.tip = tip;
    }

    public Gorev(String aciklama, List<Document> oduller, int son, int bitis, int tip) {
        this.aciklama = aciklama;
        this.oduller = oduller;
        this.son = son;
        this.bitis = bitis;
        this.tip = tip;
    }

    @Override
    public String toString() {
        return "Gorev{" +
                "_id=" + _id +
                ", pId=" + pId +
                ", bitti=" + bitti +
                ", aciklama='" + aciklama + '\'' +
                ", odul=" + odul +
                ", ilerleme=" + ilerleme +
                ", son=" + son +
                ", bitis=" + bitis +
                ", tip=" + tip +
                '}';
    }
}
