package veritabani;

import com.google.gson.internal.Primitives;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.DeleteOptions;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import mfserver.main.MFServer;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.lang.reflect.Field;

public class AltanPOJO {
    public static Document getUpdateFor(AltanPOJO altanPOJO, String... keys) throws NoSuchFieldException, IllegalAccessException {
        Document doc = new Document();
        for (String key : keys) {
            doc.append(key, fastLoad(altanPOJO, key));
        }
        return new Document("$set", doc);
    }

    private static Object fastLoad(AltanPOJO altanPOJO, String key) throws NoSuchFieldException, IllegalAccessException {

        return altanPOJO.getClass().getField(key).get(altanPOJO);
    }

    public static Document getSingle(AltanPOJO altanPOJO, String key) throws NoSuchFieldException, IllegalAccessException {
        Object fl = fastLoad(altanPOJO, key);
        BsonProperty annot = altanPOJO.getClass().getField(key).getAnnotation(BsonProperty.class);
        if(annot!=null&&annot.value().length()>0)key=annot.value();
        return new Document(key, fl);
    }

    public Object getDb() throws NoSuchFieldException, IllegalAccessException {
        return this.getClass().getField("db").get(this);
    }

    public UpdateResult updateOne(UpdateOptions updateOptions, String idKey, String... keys) throws NoSuchFieldException, IllegalAccessException {
        MongoCollection<Document> db = (MongoCollection<Document>) getDb();
        return db.updateOne(getSingle(this, idKey), getUpdateFor(this, keys));
    }

    public UpdateResult updateOne(String idKey, String... keys) throws NoSuchFieldException, IllegalAccessException {
        return updateOne(new UpdateOptions(), idKey, keys);
    }

    public UpdateResult updateMany(UpdateOptions updateOptions, String idKey, String... keys) throws NoSuchFieldException, IllegalAccessException {
        MongoCollection<Document> db = (MongoCollection<Document>) getDb();
        return db.updateMany(getSingle(this, idKey), getUpdateFor(this, keys));
    }

    public UpdateResult updateMany(String idKey, String... keys) throws NoSuchFieldException, IllegalAccessException {
        return updateMany(new UpdateOptions(), idKey, keys);
    }
    public DeleteResult deleteOne(String idKey) throws NoSuchFieldException, IllegalAccessException {
        MongoCollection<Document> db = (MongoCollection<Document>) getDb();
        return db.deleteOne(getSingle(this, idKey));
    }

    public void setObjectId() throws IllegalAccessException {
        Field objId=null;
        for (Field field:this.getClass().getFields()){
            if(field.getName().equals("_id")){
                objId=field;
                break;
            }else {
                if(field.getAnnotations().length>0){

                    BsonProperty annot = field.getAnnotation(BsonProperty.class);
                    if(annot!=null&&annot.value().length()>0&&annot.value().equals("_id")){objId=field;break;}
                }
            }
        }
        if(objId!=null){
            boolean nll=false;
            Class cl=objId.getType();
            if(cl==int.class){
                nll=(int)objId.get(this)==0;
            }
            else if(cl==String.class){
                String obj=(String)objId.get(this);
                nll=obj==null||obj.length()==0;
            }
            else if(cl==float.class){
                nll=(float)objId.get(this)==0.0f;
            }
            else if(cl==double.class){
                nll=(double)objId.get(this)==0.0;
            }
            if(nll) objId.set(this, MFServer.getSeq(this.getClass().getName()));
        }

    }
    public void insertOne() throws NoSuchFieldException, IllegalAccessException {
        MongoCollection db = (MongoCollection) getDb();
        setObjectId();
        db.insertOne(this);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        for (Field field : this.getClass().getFields()) {
            if (field.getName().equals("db")) continue;
            try {
                stringBuilder.append(field.getName() + ": " + field.get(this));
                stringBuilder.append(",\n");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}
