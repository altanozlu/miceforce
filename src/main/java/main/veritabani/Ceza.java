package veritabani;

public class Ceza {
    public String ad;
    public String yetkili="";
    public String sebep ="None";
    public String oda="";
    public String ip="";
    public long surev;
    public int surec;
    public int tip=0;
    public boolean isNano;
    public Ceza(){
    }


    public Ceza(String ad, String yetkili, long surev, int surec) {
        this.ad = ad;
        this.yetkili = yetkili;
        this.surev = surev;
        this.surec = surec;
    }
}
