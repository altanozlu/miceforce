package veritabani;

public class Ban {
    public int when = 0;
    public int saat = 0;
    public String sebep = "";

    public Ban(int when, int saat, String sebep) {
        this.when = when;
        this.saat = saat;
        this.sebep = sebep;
    }

    public Ban() {

    }
}