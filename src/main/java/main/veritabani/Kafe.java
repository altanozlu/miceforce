package veritabani;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import mfserver.util.BenchMark;

import java.io.IOException;
import java.sql.*;

/**
 * Created by sevendr on 23.02.2017.
 */
public class Kafe {
    public static void konular( PlayerConnection pc) {
        MFServer.executor.execute(new BenchMark(() -> {
            try (Connection connection = MFServer.DS.getConnection();
                 PreparedStatement statement = connection.prepareStatement(
                         "select * from cafe_thread where language = ? order BY time DESC LIMIT 25")) {

                int T = MFServer.timestamp();

                statement.setInt(1, pc.LangByte);
                ResultSet rs = statement.executeQuery();

                ByteBufOutputStream p = MFServer.getStream();
                p.writeByte(30);
                p.writeByte(40);
                while (rs.next()) {
                    int id =rs.getInt("id");
                    int player_id =rs.getInt("player_id");
                         if (pc.kullanici.engellenenler.contains(player_id)) continue;
                    p.writeInt(id);
                    p.writeUTF(rs.getString("title"));
                    p.writeInt(MFServer.get_avatar(player_id));
                    p.writeInt(rs.getInt("post_count"));
                    p.writeUTF(MFServer.plusReady(rs.getString("last_player")));
                    p.writeInt(T - rs.getInt("time"));
                }
                pc.sendPacket(p);
            } catch (SQLException | IOException ex) {
                ex.printStackTrace();
            }
        },"KONULAR"));
    }

    public static void konuyu_ac(int konu,
                                PlayerConnection pc) throws SQLException, IOException {
        try (Connection connection = MFServer.DS.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select * from cafe_post where thread_id=? order BY time ASC")) {
            pc.konular.add(konu);
            ByteBufOutputStream p = MFServer.getStream();
            p.writeByte(30);
            p.writeByte(41);
            p.writeByte(1);
            p.writeInt(konu);
            int T = MFServer.timestamp();
            statement.setInt(1, konu);
            ResultSet rs = statement.executeQuery();
            int i =0;
            while (rs.next()) {
                String[] ver = MFServer.getIsim(rs.getInt("player_id"));
                //   if (pc.ignores.contains(isim)) continue;
                int t = rs.getInt("time");
                int point = rs.getInt("vote");

                if (i++==0&&point < -10) {
                    PreparedStatement statement2 = connection
                            .prepareStatement("delete from cafe_post where thread_id=?");
                    statement2.setInt(1, konu);
                    statement2.execute();
                    statement2.close();

                    statement2 = connection.prepareStatement("delete from cafe_thread where id=?");
                    statement2.setInt(1, konu);
                    statement2.execute();
                    statement2.close();
                    continue;
                }
                int player_id =rs.getInt("player_id");
                if (pc.kullanici.engellenenler.contains(player_id)) continue;
                boolean canvote = true;
                p.writeInt(rs.getInt("id"));
                p.writeInt(MFServer.get_avatar(player_id));
                p.writeInt(T - t);

                p.writeUTF(MFServer.plusReady(ver[0],ver[1]));
                p.writeUTF(rs.getString("message"));
                p.writeBoolean(canvote);
                p.writeShort(point);

            }
            pc.sendPacket(p);
        }
    }

    public static void konu_ac(String title, String ileti,
                              PlayerConnection pc) {
        MFServer.executor.execute(new BenchMark(() -> {
            try (Connection connection = MFServer.DS.getConnection();
                 PreparedStatement statement = connection.prepareStatement(
                         "INSERT INTO  `cafe_thread` (`player_id`, `title`, `post_count`, `last_player`, `time`,  `language`) VALUES(?,?,0,'',?,?)",
                         Statement.RETURN_GENERATED_KEYS)) {

                int t = MFServer.timestamp();
                statement.setInt(1, pc.kullanici.code);
                statement.setString(2, title);
                statement.setInt(3, t);
                statement.setInt(4, pc.LangByte);
                int aff = statement.executeUpdate();
                int code = 0;
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        code = (int) generatedKeys.getLong(1);
                    }
                }
                Kafe.ileti_yolla(ileti, code, pc);

                Kafe.konular(pc);


            } catch (SQLException ex) {
                ex.printStackTrace();
            }

        },"INSERT_KONU"));
    }

    public static void ileti_yolla(String ileti, Integer tid,
                                  PlayerConnection pc) {
        MFServer.executor.execute(new BenchMark(() -> {
            try (Connection connection = MFServer.DS.getConnection();
                 PreparedStatement statement = connection.prepareStatement(
                         "INSERT INTO  `cafe_post` (`thread_id`,`player_id`, `message`, `time`) VALUES(?,?,?,?)");
                 PreparedStatement st = connection.prepareStatement(
                         "UPDATE cafe_thread SET post_count = post_count+1, last_player = ?, time = ? WHERE id =?");
                 PreparedStatement statement1 = connection
                         .prepareStatement("select post_count from cafe_thread where id=?")) {
                pc.server.incr_logs("cafe", 1);
                int t = MFServer.timestamp();
                statement.setInt(1, tid);
                statement.setInt(2, pc.kullanici.code);
                statement.setString(3, ileti);
                statement.setInt(4, t);
                statement.execute();
                st.setString(1, MFServer.plusReady(pc.kullanici.isim,pc.kullanici.tag));
                st.setInt(2, t);
                st.setInt(3, tid);
                st.execute();
                statement1.setInt(1, tid);
                ResultSet rs = statement1.executeQuery();
                if (rs.next()) {
                    int p_c = rs.getInt(1);
                    ByteBufOutputStream bs = MFServer.getStream();
                    bs.writeByte(30);
                    bs.writeByte(44);
                    bs.writeInt(tid);
                    bs.writeUTF(MFServer.plusReady(pc.kullanici.isim,pc.kullanici.tag));
                    bs.writeInt(p_c);
                    ByteBuf buf = bs.buffer();
                    for (PlayerConnection cl : pc.server.Cafers.values()) {
                        if (cl.konular.contains(tid)) {
                            cl.sendPacket(buf.retainedSlice());
                        }
                    }
                    buf.release();
                    Kafe.konuyu_ac(tid, pc);
                }


            } catch (SQLException ex) {
                ex.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }
        },"INSERT_POST"));
    }
}
