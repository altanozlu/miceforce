package veritabani;

import org.bson.types.ObjectId;

import java.util.List;

public class Uyari {
    public ObjectId _id;
    public String lang;
    public String key;
    public List<Integer> hours;
    public long deadline = 60_000 * 30;
}
