/**
 * This file is part of Ogar.
 * <p>
 * Ogar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Ogar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Ogar.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfserver.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;
import mfserver.main.MFServer;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
    public static Map<String, Integer> IP_LIST = new ConcurrentHashMap<>();
    public static Map<String, Integer> IP_LIST2 = new ConcurrentHashMap<>();
    public static Map<String, ArrayList<Long>> IP_LOGINS = new ConcurrentHashMap<>();
    public static Map<String, Integer> Banned = new ConcurrentHashMap<>();
    public final MFServer server;
    public static ConcurrentLinkedQueue<String> lastIps = new ConcurrentLinkedQueue<>();
    public static ConcurrentLinkedQueue<String> lastLogins = new ConcurrentLinkedQueue<>();
    public String ip;
    private PlayerConnection player;

    public ClientHandler(MFServer server, String ip) {
        this.server = server;
        this.ip = ip;

    }

    public static boolean add_LOGIN_IP(Channel channel, String ip) {

        ArrayList<Long> inc = IP_LOGINS.getOrDefault(ip, new ArrayList<>());
        inc.add(System.currentTimeMillis());
        IP_LOGINS.put(ip, inc);
        if (inc.size() > 10) inc.remove(0);
        var d = inc.stream().mapToLong(k -> k).average();
        if (d.isPresent() && inc.size() > 5) {
            if (System.currentTimeMillis() - d.getAsDouble() < 2_000) {
                MFServer.executor.execute(() -> {
                    System.out.println("(LOGIN) Attacker ip :" + ip);
                    banIP(ip);
                    System.out.println("(LOGIN) Ip banned :" + ip);
                    IP_LOGINS.remove(ip);
                });
                MFServer.instance.disconnectIPaddress(ip);
                MFServer.IP_BANS.put(ip, System.currentTimeMillis() + 3600 * 24 * 300);
                channel.close();
                return true;
            }
        }
        return false;
    }

    public static void banIP(String ip) {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("ufw insert 1  deny from " + ip);
            //   pr.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        if (MFServer.IP_BANS.containsKey(ip)) {
            ctx.channel().close();
            return;
        }

        Integer count = IP_LIST.getOrDefault(ip, 0);
        if (count > 50) {
            System.out.println(ip);
            banIP(ip);
            System.out.println("Ip banned2 :" + ip);
            ctx.channel().close();
            return;
        }
        if (!ip.equals("127.0.0.1")) {
            lastIps.add(ip);

            if (lastIps.size() > 300) {
                String IP = lastIps.poll();
                Integer inc = IP_LIST2.getOrDefault(IP, 0);
                if (inc > 0) {
                    inc--;
                    IP_LIST2.put(IP, inc);
                    if (inc <= 0) {
                        IP_LIST2.remove(IP);
                    }
                }
            }

            Integer inc = IP_LIST2.getOrDefault(ip, 0);
            inc++;
            IP_LIST2.put(ip, inc);
            if (IP_LIST2.getOrDefault(ip, 0) > 50) {
                MFServer.executor.execute(() -> {
                    System.out.println("Attacker ip :" + ip);
                    banIP(ip);
                    System.out.println("Ip banned :" + ip);
                    IP_LIST2.remove(ip);
                });
                server.disconnectIPaddress(ip);
                MFServer.IP_BANS.put(ip, System.currentTimeMillis() + 3600 * 24 * 300);
                ctx.channel().close();
                return;
            }
        }


        count += 1;
        IP_LIST.put(ip, count);
        this.player = new PlayerConnection(this, ctx.channel());

    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
        //  System.out.println(evt);
        if (evt instanceof IdleStateEvent) {
            ctx.channel().close();
            if (player != null) player.disc();

        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // server.getPlayerList().removePlayer(player);
        Integer count = IP_LIST.getOrDefault(ip, 0);
        count -= 1;
        if (count > 0) IP_LIST.put(ip, count);
        else IP_LIST.remove(ip);
        if (player != null) player.disc();
        player = null;


    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {
        player.handle(buf);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //  System.out.println(cause);
        // MFServer.log.warning( MFServer.stackTraceToString(cause));
        if (!(cause instanceof IOException)) {
            String name = "";
            if (player != null && player.kullanici != null) name = player.kullanici.isim;
            MFServer.log.warning("[" + name + "]" + MFServer.stackTraceToString(cause));

        }
        ctx.channel().close();
    }

}
