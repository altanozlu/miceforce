/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author sevendr
 */
public class I_ShopCustomize extends Packet {

    public int item;
    public ArrayList<Integer> colors;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        item = buf.readShort();
        byte size = buf.readByte();
        colors = new ArrayList();
        for (int i = 0; size > i; i++) {
            colors.add(buf.readInt());
        }

    }

}
