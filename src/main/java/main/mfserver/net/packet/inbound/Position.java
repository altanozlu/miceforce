/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class Position extends Packet {

    public int round;
    public boolean direction;
    public short x, y;
    public boolean direction2;
    public short vx;
    public short vy;
    public boolean jump;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        round = buf.readInt();
        direction = buf.readBoolean();
        direction2 = buf.readBoolean();
        x = buf.readShort();
        y = buf.readShort();
        vx = buf.readShort();
        vy = buf.readShort();
        jump = buf.readBoolean();

    }
}
