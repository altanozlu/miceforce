/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.net.packet.inbound;

import io.netty.buffer.ByteBufInputStream;
import mfserver.net.Packet;

import java.io.IOException;

/**
 * @author sevendr
 */
public class I_EnterHole extends Packet {

    public byte Object;
    public int round;
    public short distance;

    @Override
    public void readData(ByteBufInputStream buf) throws IOException {
        Object = buf.readByte();
        round = buf.readInt();
        distance = buf.readShort();

    }

}
