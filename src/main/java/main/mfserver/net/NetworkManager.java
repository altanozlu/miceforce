/**
 * This file is part of Ogar.
 * <p>
 * Ogar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Ogar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Ogar.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfserver.net;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import mfserver.main.MFServer;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class NetworkManager {

    private final MFServer server;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private Channel channel;

    public NetworkManager(MFServer server) {
        this.server = server;
    }

    public void start() throws IOException, InterruptedException {
        Class<? extends ServerSocketChannel> SS = NioServerSocketChannel.class;

        if (Epoll.isAvailable()) {
            SS = EpollServerSocketChannel.class;
            bossGroup = new EpollEventLoopGroup(16);
            workerGroup = new EpollEventLoopGroup(16);
        } else {
            bossGroup = new NioEventLoopGroup(16);
            workerGroup = new NioEventLoopGroup(16);
        }
        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup, workerGroup).channel(SS)
                .childHandler(new ClientInitializer(server));
        ChannelFuture ft = b.bind(4747).sync();
        // b.bind(44440);
        // b.bind(6112);
        // b.bind(3724);
        // b.bind(443);
        // b.bind(5555);
        MFServer.channel = ft.channel();
    //    MFServer.channel.config().setAllocator(PooledByteBufAllocator.DEFAULT);
    }


    public boolean shutdown() {
        if (channel == null) {
            return false;
        }

        try {
            channel.close().sync();
            return true;
        } catch (InterruptedException ex) {
            return false;
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    private static class ClientInitializer extends ChannelInitializer<SocketChannel> {

        private final MFServer server;

        public ClientInitializer(MFServer server) {
            this.server = server;
        }

        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            //ch.config().setTcpNoDelay(true);
            ch.pipeline().addLast("idleStateHandler", new IdleStateHandler(0, 0, 180));
          //  ch.pipeline().addLast("ChannelTrafficShapingHandler", new ChannelTrafficShapingHandler(5_000, 5_000));
            ch.pipeline().addLast(new PacketDecoder());
            ch.pipeline().addLast(new RawData());
            ch.pipeline().addLast(new PacketEncoder());
          //  ch.pipeline().addLast("traffic", new ChannelTrafficShapingHandler(0, 60000, 1000));
            ch.pipeline().addLast(new ClientHandler(server, ch.remoteAddress().getHostString()));

        }
    }
}
