/**
 * This file is part of Ogar.
 * <p>
 * Ogar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Ogar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Ogar.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfserver.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;

import java.io.IOException;

public abstract class Packet {

    /**
     * Writes packet data, excluding the packet ID, to the specified buffer.
     * <p>
     *
     * @param buf
     */

    /**
     * Reads packet data, excluding the packet ID, from the specified buffer.
     * <p>
     *
     * @param buf
     */
    public ByteBuf BUF;

    public static void decode(ByteBuf buf) {
        /**
         * buf.markReaderIndex(); int i=0;
         *
         * while (buf.readableBytes()!=0){ buf.setByte(i++, buf.readByte()^77);
         *
         * } buf.resetReaderIndex();
         **/
    }

    public abstract void readData(ByteBufInputStream buf) throws IOException;

    public void readData_orj(ByteBuf buf) throws IOException {
        BUF = buf;
        readData(new ByteBufInputStream(buf));

    }

    public void done() {
        if (this.BUF != null) {
            BUF.release();
        }
    }

}
