/**
 * This file is part of Ogar.
 * <p>
 * Ogar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * Ogar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with Ogar.  If not, see <http://www.gnu.org/licenses/>.
 */
package mfserver.net;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import mfserver.main.MFServer;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // boolean policy = false;
       // if(in.readableBytes()==0)return;
        if (in.readableBytes() == 23) {
            in.markReaderIndex();
            if (in.readByte() == 60) {
                // policy = true;
                out.add(in.retainedSlice(0, 23));
                in.readerIndex(23);

                return;
            } else {
                in.resetReaderIndex();
            }

        }
        in.markReaderIndex();
        if (in.readByte() == 7) {
            if (in.readInt() == 7) {
                out.add(in.retainedSlice());
                in.skipBytes(in.readableBytes());
                return;
            }
            in.skipBytes(in.readableBytes());
        }
        in.resetReaderIndex();
        if (in.readableBytes() < 4) {
        } else {
          //  System.out.println(in.readableBytes());
            // System.out.println(OgarServer.bytesToHex(in));
            in.markReaderIndex();
            byte type = in.readByte();

            int length = 0;
            if (type == 1) {
                length = in.readUnsignedByte();
            } else if (type == 2) {
                length = in.readUnsignedShort();
            } else if (type == 3) {
                length = in.readMedium();
            }
            length+=1;
            if (in.readableBytes() == length) {
                out.add(in.readBytes(length));
            } else if (in.readableBytes() < length) {
                in.resetReaderIndex();
            } else {
                out.add(in.readBytes(length));
            }

        }

    }
}
