package mfserver.util;

import mfserver.main.MFServer;
import org.bson.Document;

public class BenchMark implements Runnable {
    Runnable runnable;
    String key;

    public BenchMark(Runnable runnable) {
        this.key = "BILINMEYEN";
        this.runnable = runnable;
    }

    public BenchMark(Runnable runnable, String key) {
        this.key = key;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        if (!MFServer.benchLog.get()) {
            runnable.run();
        } else {
            long start = System.nanoTime();
            runnable.run();
            long delta = System.nanoTime() - start;
            MFServer.colBenchLog.insertOne(new Document("key", key).append("delta", delta));
        }
    }
}
