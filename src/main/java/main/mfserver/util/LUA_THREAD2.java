/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.util;

import mfserver.data_out.LuaChat;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.L_Room;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * @author sevendr
 */
public class LUA_THREAD2 extends LUA_THREAD {

    public String script;
    public boolean interrupted;
    public String ad;

    public LUA_THREAD2(L_Room l_room, String script) {
        super(l_room, null);
        this.script = script;
        this.l_room = l_room;
        this.limit = 1000;
    }

    @Override
    public String toString() {
        return ad;
    }

    @Override
    public void run() {

        l_room.playerList = l_room.getPlayerList();
        ad = "LUA2 Thread " + this.l_room.toString();
        boolean hasLoop = false;
        long t = System.currentTimeMillis();
        Globals globals = JsePlatform.standardGlobals(this.l_room);
        developer = this.l_room.developer;
        PlayerConnection cl = MFServer.instance.clients.get(developer);
        if (cl != null && cl.kullanici != null && (cl.kullanici.yetki == 10 || cl.kullanici.isLuacrew)) {
            this.limit = 10000;
        }
        if (developer.equals("an")) {
            this.limit = 30_000;
        }
        this.time = System.currentTimeMillis();
        try {
            LuaValue chunk = globals.load(script, this.l_room.developer);
            this.l_room.global = globals;
            //this.time = System.currentTimeMillis()+2000;
            try {
                chunk.call();
            } catch (Exception ex) {
                // System.out.println(ex.toString());
                //ex.printStackTrace();

                this.l_room.sendError(ex);
                return;

            }
            if (!this.l_room.developer.equals("an"))
                this.l_room.sendLuaMessage("Script Executed in " + (System.currentTimeMillis() - t) + " ms.");
        } catch (Exception ex) {
            if (developer.equals("an")) {
                System.out.println(ex.toString());
            }
            cl = MFServer.instance.clients.get(developer);
            if (cl != null) {
                try {
                    cl.sendPacket(new LuaChat(cl, ex.toString()).data());
                } catch (IOException ee) {
                    //Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return;
        }
        if (!this.l_room.custom) {

        }
        l_room.playerList = l_room.getPlayerList();
        LuaValue loop = globals.get("eventLoop");
        hasLoop = loop != LuaValue.NIL;
        this.time = System.currentTimeMillis();
        long lastLoop = 0;
        int eventCount = 0;
        while (l_room != null && !l_room.get_interrupted()) {
            try {
                l_room.playerList = l_room.getPlayerList();
                while (!l_room.get_interrupted()) {
                    if (eventCount++ > 50)
                        break;
                    ArrayList<Event> removeList = new ArrayList<>();
                    for (Event ev : events2) {
                        if (ev.deadline <= System.currentTimeMillis()) {
                            ev.fonk.invoke(ev.degerler);
                            if (ev.loop) {
                                ev.deadline = System.currentTimeMillis() + ev.time;
                            } else {

                                removeList.add(ev);
                            }
                        }
                    }
                    for (Event ev : removeList) {
                        events2.remove(ev);
                    }

                    Event event = events.poll(5, TimeUnit.MILLISECONDS);
                    if (event == null)
                        break;
                    LuaValue fonk = this.l_room.global.get(event.fonksiyon);
                    if (!fonk.isnil()) {
                        fonk.invoke(event.degerler);
                    }

                }
                if (hasLoop && System.currentTimeMillis() - lastLoop >= this.l_room.eventMs && !l_room.get_interrupted()) {
                    l_room.isLooping = true;
                    LuaValue[] l = this.l_room.room.getRoundTime2();
                    loop.call(l[0], l[1]);
                    lastLoop = System.currentTimeMillis();
                    l_room.isLooping = false;
                }
                Thread.sleep(Math.max(this.l_room.eventMs - 200, 15));
                this.time = System.currentTimeMillis();
                this.l_room.call_count.set(0);
                eventCount = 0;
            } catch (Exception ex) {
                if (this.l_room != null) {
                    this.l_room.sendError(ex);
                } else {
                    cl = MFServer.instance.clients.get(developer);
                    if (cl != null) {
                        try {
                            cl.sendPacket(new LuaChat(cl, ex.toString()).data());
                        } catch (IOException ee) {
                            //Logger.getLogger(L_Room.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                break;
            }
        }
       /* if (this.l_room != null) {
            this.l_room.room.l_room = l_room;
        }*/

    }

}
