package mfserver.util;

import io.netty.buffer.ByteBuf;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sevendr on 25.04.2017.
 */
public class Beceriler {
    static int[] arr_1 = new int[]{110, 120, -126, -116, -106};

    public static ByteBuf sendShamanSkillCode(int skillCode, int skillVal) {

        return MFServer.getBuf().writeByte(8).writeByte(10).writeByte(skillCode).writeByte(skillVal);
    }

    public static void Yolla(PlayerConnection pc, boolean yeniden) {
        for (Map.Entry<String, Integer> entry : pc.kullanici.beceriler.entrySet()) {
            Integer key = Integer.valueOf(entry.getKey());
            Integer val = entry.getValue();
            if (val > 5) val = 5;
            boolean surv = pc.room.isSurvivor || pc.room.isKutier;
            switch (key) {
                case 1:
                    if (!yeniden) pc.sendPacket(sendShamanSkillCode(key, arr_1[val - 1]));
                    break;
                case 2:
                    if (!yeniden) pc.sendPacket(sendShamanSkillCode(key, 112 + (val * 2)));
                    break;
                case 5:
                    if (!yeniden) pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 6:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 7:
                    if (!yeniden) pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
                case 8:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 9:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 10:
                    pc.sendPacket(sendShamanSkillCode(key, 3));
                    break;
                case 11:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 12:
                    if(!surv)pc.sendPacket(sendShamanSkillCode(key, val * 4));
                    break;
                case 13:
                    pc.sendPacket(sendShamanSkillCode(key, 3));
                    break;
                case 14:
                    pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
                case 20:
                    pc.sendPacket(sendShamanSkillCode(key, 110 + (val * 4)));
                    break;
                case 22:
                    pc.sendPacket(sendShamanSkillCode(key, 20 + (val * 5)));
                    break;
                case 23:
                    pc.sendPacket(sendShamanSkillCode(key, 30 + (val * 10)));
                    break;
                case 26:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 27:
                    if (!surv && !yeniden) pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
                case 28:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 29:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 30:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 31:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 33:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 34:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 40:
                    pc.sendPacket(sendShamanSkillCode(key, 20 + (val * 10)));
                    break;
                case 41:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 42:
                    pc.sendPacket(sendShamanSkillCode(key, -6 - (val * 10)));
                    break;
                case 43:
                    pc.sendPacket(sendShamanSkillCode(key, -6 - (val * 10)));
                    break;
                case 44:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 45:
                    pc.sendPacket(sendShamanSkillCode(key, arr_1[val - 1]));
                    break;
                case 46:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 47:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 48:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 49:
                    if (!yeniden &&!surv) pc.sendPacket(sendShamanSkillCode(key, 96 - (val * 6)));
                    break;
                case 50:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 51:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 52:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 53:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 54:
                    pc.sendPacket(sendShamanSkillCode(key, -126));
                    break;
                case 60:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 61:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 62:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 63:
                    if(!surv)pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 64:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 65:
                    if(!surv)pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 66:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 67:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 68:
                    pc.sendPacket(sendShamanSkillCode(key, 120 - val * 10));
                    break;
                case 69:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 70:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 71:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 72:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 73:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 74:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 80:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 81:
                    if (!surv) pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 82:
                    if (!surv) pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 83:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 84:
                    pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 85:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 86:
                    pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
                case 87:
                    pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
                case 88:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 89:
                    if (!surv) pc.sendPacket(sendShamanSkillCode(49, val));
                    break;
                case 90:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 92:
                    if (!yeniden)pc.sendPacket(sendShamanSkillCode(key, 1));
                    break;
                case 93:
                    pc.sendPacket(sendShamanSkillCode(key, val));
                    break;
                case 94:
                    pc.sendPacket(sendShamanSkillCode(key, 100));
                    break;
            }
        }
    }
}
