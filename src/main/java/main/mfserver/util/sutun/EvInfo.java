package mfserver.util.sutun;

import com.google.common.primitives.Ints;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import mfserver.main.MFServer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by sevendr on 12/06/17.
 */
public class EvInfo {
    public int n23 = 0;
    public int e31 = 0;
    public int fircalar = 0;
    public int unvanYenilemesi = 0;
    public HashSet<Integer> titles;

    public EvInfo(String json) {
        Gson gson = MFServer.get_Gson();
        Type type = new TypeToken<HashMap<String, Object>>() {
        }.getType();
        HashMap<String, Object> evinfo = gson.fromJson(json, type);
        n23 = MFServer.obj_to_int(evinfo.getOrDefault("23n", 0));
        e31 = MFServer.obj_to_int(evinfo.getOrDefault("31e", 0));
        fircalar = MFServer.obj_to_int(evinfo.getOrDefault("fircalar", 0));
        unvanYenilemesi = MFServer.obj_to_int(evinfo.getOrDefault("unvanYenilemesi2", 0));
        ArrayList arrayList = (ArrayList) evinfo.getOrDefault("titles", new ArrayList<>());
        titles = new HashSet<>();
        arrayList.forEach(k -> titles.add(MFServer.obj_to_int(k)));
        if (unvanYenilemesi == 0) {
            titles.remove(3077);
            titles.remove(3152);
            titles.remove(3151);
            titles.remove(3150);
            titles.remove(3036);
            unvanYenilemesi = 1;
        }
        titles.removeAll(MFServer.zodiacTitles);

    }

    public String serialize() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("23n", n23);
        jsonObject.addProperty("31e", e31);
        jsonObject.addProperty("fircalar", fircalar);
        jsonObject.addProperty("unvanYenilemesi2", unvanYenilemesi);
        JsonArray jsonArray = new JsonArray();
        titles.forEach(k -> jsonArray.add(new JsonPrimitive(k)));
        jsonObject.add("titles", jsonArray);
        return jsonObject.toString();
    }
}
