/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.util;

import mfserver.util.haritalar.*;

/**
 * @author sevendr
 */
public class VanillaMap extends map {
    public VanillaMap(Integer id) {

        this.id = id;
        this.perm=0;
    }

    // public Integer id;
    public static map get(Integer id) {
        switch (id) {
            case 31:
                return new _31();
            case 41:
                return new _41();
            case 42:
                return new _42();
            case 54:
                return new _54();
            case 59:
                return new _59();
            case 60:
                return new _60();
            case 62:
                return new _62();
            case 89:
                return new _89();
            case 92:
                return new _92();
            case 114:
                return new _114();
            case 301:
                return new _301();
            case 302:
                return new _302();
            case 303:
                return new _303();
            case 304:
                return new _304();
            case 305:
                return new _305();
            case 306:
                return new _306();
            case 307:
                return new _307();
            case 308:
                return new _308();
            case 309:
                return new _309();


            default:
                return new VanillaMap(id);

        }
    }
}
