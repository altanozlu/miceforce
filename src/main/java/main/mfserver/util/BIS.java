package mfserver.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by sevendr on 11.03.2017.
 */
public class BIS extends ByteBufInputStream {
    private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    private ByteBuf bf;

    public BIS(ByteBuf buffer) {
        super(buffer);
        bf=buffer;
    }

    @Override
    public String readUTF() throws IOException {
        int length = this.readUnsignedShort();
        byte[] bytearr = new byte[length];
        this.read(bytearr, 0, length);
        return new String(bytearr, UTF8_CHARSET);
    }

    public String readUTFMedium() throws IOException {
        int length = this.bf.readMedium();
        byte[] bytearr = new byte[length];
        this.read(bytearr, 0, length);
        return new String(bytearr, UTF8_CHARSET);
    }
}
