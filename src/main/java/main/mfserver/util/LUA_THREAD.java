/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.util;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.L_Room;
import org.luaj.vm2.lib.jse.JsePlatform;
import veritabani.SRC;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @author sevendr
 */
public class LUA_THREAD extends Thread {

    public L_Room l_room;
    public volatile boolean interrupted;
    public long time;
    public BlockingDeque<Event> events = new LinkedBlockingDeque<>();
    public List<Event> events2 = Collections.synchronizedList(new ArrayList<Event>());
    private SRC cb;
    public int limit=0;
    public String developer="";

    public LUA_THREAD(L_Room l_room, SRC cb) {
        this.l_room = l_room;
        this.time = System.currentTimeMillis();
        this.cb = cb;
        this.limit=15_000;
    }

    @Override
    public void run() {
        this.setName("LUA Thread " + this.l_room.toString());
        boolean hasLoop = false;
        l_room.playerList=l_room.getPlayerList();

        this.time = System.currentTimeMillis();
        Globals globals = JsePlatform.standardGlobals(this.l_room);

        try {

            this.l_room.global = globals;
            LuaValue chunk = globals.load(globals.finder.findResource(this.l_room.room.LUAFile),"@"+this.l_room.room.LUAFile,"t",globals);

            this.time = System.currentTimeMillis();

            try {
                chunk.call();
            } catch (Exception ex) {
            //    ex.printStackTrace();
                // this.l_room.sendLuaMessage(ex.getMessage());
                this.l_room.room.l_room = null;
                this.l_room.room.isMinigame = false;
                // this.l_room.sendError(ex.getMessage());
                return;

            }

        } catch (LuaError ex) {
            //ex.printStackTrace();
            this.l_room.sendLuaMessage(ex.getMessage());
            this.l_room.room.l_room = null;
            this.l_room.room.isMinigame = false;
        }
        if (!this.l_room.custom) {

                this.l_room.room.NewRound();

        }
        cb.onResult(null);
        this.cb = null;
        LuaValue loop = globals.get("eventLoop");

        hasLoop = loop != LuaValue.NIL;
        this.time = System.currentTimeMillis();

        l_room.playerList=l_room.getPlayerList();
        long lastLoop = 0;
        int eventCount = 0;
        while (!l_room.get_interrupted()) {
            try {
                l_room.playerList=l_room.getPlayerList();
                while (!l_room.get_interrupted()) {
                    if (eventCount++ > 50)
                        break;
                    ArrayList<Event> removeList = new ArrayList<>();
                    for (Event ev : events2) {
                        if (ev.deadline <= System.currentTimeMillis()) {
                            ev.fonk.invoke(ev.degerler);
                            if (ev.loop) {
                                ev.deadline = System.currentTimeMillis() + ev.time;
                            } else {

                                removeList.add(ev);
                            }
                        }
                    }
                    for (Event ev : removeList) {
                        events2.remove(ev);
                    }
                    Event event = events.poll(5, TimeUnit.MILLISECONDS);
                    if (event == null)
                        break;
                    LuaValue fonk = this.l_room.global.get(event.fonksiyon);
                    if (!fonk.isnil()) {
                        fonk.invoke(event.degerler);
                    }

                }
                if (hasLoop && System.currentTimeMillis() - lastLoop >= this.l_room.eventMs) {
                    l_room.isLooping=true;
                    LuaValue[] l = this.l_room.room.getRoundTime2();
                    loop.call(l[0], l[1]);
                    lastLoop = System.currentTimeMillis();
                    l_room.isLooping=false;
                }
                this.l_room.call_count.set(0);
                this.time = System.currentTimeMillis();
                eventCount = 0;
            } catch (Exception ex) {
                this.l_room.sendError(ex);

                break;
            }
        }
        if (this.l_room != null) {
            this.l_room.room.closeLua();
        }

    }

}
