/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.main;

import com.google.common.primitives.Ints;
import io.netty.buffer.ByteBuf;
import mfserver.net.PlayerConnection;
import mfserver.util.map;
import org.bson.Document;
import org.luaj.vm2.parser.Token;
import veritabani.Harita;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sevendr
 */
public class OldData {

    PlayerConnection client;

    public OldData(PlayerConnection client) {
        this.client = client;
    }

    public void parse(String old_data) throws Exception {
        //    client.dataCountOld+=1;
        if (client.isSyncer && client.dataCountOld > 1000 && !client.room.isFuncorp) {
            client.server.banPlayer(client.kullanici.isim, 1800, "HACK DATA", "Tengri", false,null);
            client.sendChatServer("Tengri banned " + client.kullanici.isim + " for 1800 hours. Reason : HACK DATA");
        }

        // String[] values = old_data.split(MFServer.x01);
        List<String> values = new ArrayList<String>(Arrays.asList(old_data.split(MFServer.x01)));
        // list.
        int Token1 = 0;
        int Token2 = 0;
        if (values.size() > 0) {
            String Tokens = values.remove(0);
            Token1 = (int) (Tokens.charAt(0));
            Token2 = (int) (Tokens.charAt(1));
        } else {
            return;
        }
        if (Token1 == 25) {
            boolean quiz = this.client.room.isQuiz && this.client.room.codes[0].kullanici.code != client.kullanici.code;
            if (!(quiz)) {
                if (client.room.isofficial && client.kullanici.yetki < 9) {
                    return;
                } else if (client.room.isFuncorp && !(client.kullanici.isFuncorp || client.kullanici.yetki >= 9)) {
                    return;
                } else if (!client.kullanici.isArtist) {
                    return;
                }
            }


            if (Token2 == 3) {//Siliyor
                client.isDrawing = false;
                this.client.room.sendAllOldOthers(Token1, Token2, values.toArray(), client.kullanici.code);
            } else if (Token2 == 5) {//Çiziyor
                this.client.room.sendAllOldOthers(Token1, Token2, values.toArray(), client.kullanici.code);
            } else if (Token2 == 4) {//Çizmeye başladı
                if (!client.isDrawing && System.currentTimeMillis() > client.room.drawCache.getOrDefault(client.kullanici.isim, 0L) && !quiz) {
                    client.sendChatServer("<J>" + client.kullanici.isim + " drawing in room " + client.room.name);
                    client.room.drawCache.put(client.kullanici.isim, System.currentTimeMillis() + 1000 * 60 * 5);
                }
                client.isDrawing = true;
                this.client.room.sendAllOldOthers(Token1, Token2, values.toArray(), client.kullanici.code);
            }
        } else if (Token1 == 5) {
            if (Token2 == 7) {
                // Anchors
                for (PlayerConnection pc : this.client.room.clients.values()) pc.sendAnchors.addAll(values);
                //this.client.room.sendAllOld(Token1, Token2, values.toArray());
                // this.client.room.sendAllOld(Token1, Token2, values.toArray());
                this.client.room.anchors.addAll(values);

            } else if (Token2 == 8) {
                // Shaman Begin Spawn

                if (!this.client.isDead && this.client.isShaman) {
                    values.add(0, String.valueOf(this.client.kullanici.code));
                    // Object[] newValues = ArrayUtils.addAll(new
                    // Object[]{this.client.playerCode}, v);
                    this.client.room.sendAllOld(Token1, Token2, values.toArray());

                    if (this.client.room.l_room != null) {
                        // this.room.eventSummoningStart(this.Username,
                        // Integer.valueOf(values[0]),
                        // Integer.valueOf(values[1]),
                        // Integer.valueOf(values[2]),
                        // Integer.valueOf(values[3]));
                    }
                }

            } else if (Token2 == 9) {
                // Shaman Spawn Cancel
                this.client.room.sendAllOld(Token1, Token2, new Object[]{String.valueOf(this.client.kullanici.code)});

                if (this.client.room.l_room != null) {
                    // this.room.eventSummoningCancel(this.Username);
                }

            } else if (Token2 == 13) {
                // Totem Anchors
                String code = values.get(0);
                String x = values.get(1);
                String y = values.get(2);
                if (this.client.room.isTotemEditeur) {
                    if (this.client.totem2.count <= 20) {
                        this.client.totem2.count++;
                        ByteBuf p = MFServer.getBuf();
                        p.writeByte(28).writeByte(11);
                        p.writeShort(this.client.totem2.count * 2);
                        p.writeShort(0);
                        this.client.sendPacket(p);
                        this.client.totem2.data += "#3#" + code + MFServer.x01 + x + MFServer.x01 + y;
                    }
                }
            } else if (Token2 == 16) {
                // Move Cheese
                if (client.room.localSync) return;
                if (this.client.isSyncer && this.client.room.Map.id < 1000 && this.client.room.Map.perm == 0 && client.room.Map.id == 7) {
                    this.client.room.sendAllOld(Token1, Token2, values.toArray());
//                    System.out.println(this.client.kullanici.isim+" - "+this.client.room.Map.id);

                }
            } else if (Token2 == 22) {
                if (client.room.localSync) return;
                if (client.isShaman || client.isSyncer) this.client.room.sendAllOld(Token1, Token2, values.toArray());

            } else if (Token2 == 14) {
                if (client.room.localSync) return;
                if (client.isShaman || client.isSyncer) this.client.room.sendAllOld(Token1, Token2, values.toArray());

            } else if (Token2 == 17) {
                if (client.room.localSync) return;
                client.dataCountOld += 1;
                if (client.isSyncer && client.room.isRacing) {
                    if (client.lastroundBomb != client.room.Round) {
                        client.roundCountBomb += 1;
                        client.lastroundBomb = client.room.Round;
                    }
                    if ((client.roundCountBomb > 1 || client.dataCountOld > 3) && !client.room.isFuncorp) {
                        client.server.banPlayer(client.kullanici.isim, 1800, "HACK DATA", "Tengri", false,null);
                        client.sendChatServer("Tengri banned " + client.kullanici.isim + " for 1800 hours. Reason : HACK DATA");
                    }
                }


                // Bombs
                if (client.isShaman || client.isSyncer) this.client.room.sendAllOld(Token1, Token2, values.toArray());

            } else {
                System.out.format("%s,%s [%s] \n", Token1, Token2, values);
            }
        } else if (Token1 == 4) {
            if (client.room.localSync) return;
            if (Token2 == 6) {
                if (client.isShaman || client.isSyncer) this.client.room.sendAllOld(Token1, Token2, values.toArray());

            } else if (Token2 == 12) {

                if (client.isShaman || client.isSyncer)
                    this.client.room.sendAllOldOthers(Token1, Token2, values.toArray(), this.client.kullanici.code);

            } else if (Token2 == 13) {

                if (client.isShaman || client.isSyncer)
                    this.client.room.sendAllOldOthers(Token1, Token2, values.toArray(), this.client.kullanici.code);

            } else if (Token2 == 14) {

                if (this.client.isShaman && !this.client.isDead) {
                    this.client.room.addConjuration(Integer.valueOf(values.get(0)), Integer.valueOf(values.get(1)), 10);
                }
            }
        } else if (Token1 == 8) {
            if (Token2 == 16) {
                //	values.clear();
                //values.add(String.valueOf(client.code));
                this.client.room.sendAllOld(Token1, Token2, values.toArray());
            } else if (Token2 == 17) {
                values.clear();
                values.add(String.valueOf(client.kullanici.code));
                values.add("0");
                this.client.room.sendAllOld(Token1, 16, values.toArray());
            } else if (Token2 == 5) {
                this.client.room.sendAllOld(Token1, Token2, values.toArray());
            }
        } else if (Token1 == 14) {
            if (!this.client.room.isEditeur)
                return;
            if (Token2 == 10) {
                if (!this.client.room.isEditeur)
                    return;
                String xml = values.get(0);
                if (MFServer.checkValidXML(xml)) {
                    this.client.room.editeurID = -1;
                    this.client.room.editeurXML = xml;
                    this.client.room.workEditeur = true;
                    this.client.sendPacket(MFServer.pack_old(14, 14, new Object[]{""}));
                    this.client.room.NewRound();

                }
            } else if (Token2 == 11) {
                this.client.room.editeurXML = values.get(0);
            } else if (Token2 == 14) {

                this.client.sendPacket(MFServer.pack_old(14, 14, new Object[]{"", ""}));
                this.client.room.workEditeur = false;
            } else if (Token2 == 18) {
                String xml = this.client.room.editeurXML;
                if (xml.length() == 0)
                    return;
                if (MFServer.checkValidXML(xml)) {
                    if (this.client.room.editeurID == -1) {
                        MFServer.executor.submit(() -> {
                            try {
                                try (Connection connection = MFServer.DS.getConnection();
                                     PreparedStatement statement = connection.prepareStatement(
                                             "INSERT INTO maps(name,`xml`,`perm`,`yes`,`no`)VALUES(?,?,?,0,0)",
                                             Statement.RETURN_GENERATED_KEYS)) {
                                    statement.setString(1, client.kullanici.isim);
                                    statement.setString(2, xml);
                                    statement.setInt(3, 22);
                                    statement.executeUpdate();
                                    ResultSet rs = statement.getGeneratedKeys();
                                    if (rs.next()) {
                                        int last_inserted_id = rs.getInt(1);

                                        this.client
                                                .sendPacket(MFServer.pack_old(14, 5, new Object[]{last_inserted_id}));
                                        this.client.sendPacket(MFServer.pack_old(14, 14, new Object[]{"0"}));
                                        this.client.enterRoom("1", false);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                    } else {
                        if (this.client.room.editeurName.equals(client.kullanici.isim) || this.client.kullanici.isMapcrew || this.client.kullanici.yetki == 10) {

                            MFServer.executor.submit(() -> {
                                MFServer.colPermLog.insertOne(new Document("type", 1).append("name", client.kullanici.isim).append("map", this.client.room.editeurID).append("isOwner", this.client.room.editeurName.equals(client.kullanici.isim)).append("time", MFServer.timestamp()));

                                try {
                                    try (Connection connection = MFServer.DS.getConnection();
                                         PreparedStatement statement = connection
                                                 .prepareStatement("UPDATE maps SET xml = ? WHERE id = ?")) {
                                        statement.setString(1, xml);
                                        statement.setInt(2, this.client.room.editeurID);
                                        statement.executeUpdate();
                                        this.client.sendPacket(MFServer.pack_old(14, 14, new Object[]{"0"}));
                                        this.client.enterRoom("1", false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                    }
                }

            } else if (Token2 == 6) {
                String _map = values.get(0).replace("@", "");
                Integer code = Ints.tryParse(_map);
                if (code != null) {
                    if(client.isLocal){
                        String a="<C><P /><Z><S><S H=\"30\" L=\"170\" Y=\"335\" X=\"291\" P=\"0,0,0,0.2,-35,0,0,0\" T=\"1\" /><S H=\"30\" L=\"235\" Y=\"253\" X=\"264\" P=\"0,0,0,0.2,-35,0,0,0\" T=\"1\" /><S H=\"30\" L=\"300\" Y=\"172\" X=\"236\" P=\"0,0,0,0.2,-35,0,0,0\" T=\"1\" /><S H=\"30\" L=\"300\" Y=\"172\" X=\"564\" P=\"0,0,0,0.2,35,0,0,0\" T=\"1\" /><S H=\"30\" L=\"235\" Y=\"253\" X=\"536\" P=\"0,0,0,0.2,35,0,0,0\" T=\"1\" /><S H=\"30\" L=\"170\" Y=\"335\" X=\"509\" P=\"0,0,0,0.2,35,0,0,0\" T=\"1\" /><S H=\"36\" L=\"100\" Y=\"293\" X=\"400\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"0\" /><S H=\"36\" L=\"100\" Y=\"193\" X=\"400\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"0\" /><S H=\"36\" L=\"100\" Y=\"93\" X=\"400\" P=\"0,0,0.3,0.2,0,0,0,0\" T=\"0\" /></S><D><F X=\"400\" Y=\"170\" /><DS X=\"400\" Y=\"60\" /><DC X=\"400\" Y=\"60\" /><T X=\"400\" Y=\"274\" /></D><O /></Z></C>";
                     //   this.client.sendPacket(MFServer.pack_old(14, 9, new Object[]{a, "ew", "lel", 0}));
                    }
                    else{
                        MFServer.executor.execute(() -> {
                            map m = Harita.al(code);
                            try {
                                if (m.id == 0) {
                                    this.client.sendPacket(MFServer.pack_old(14, 8, new Object[0]));
                                } else {
                                    String MapName = m.name;
                                    String MapXML = m.xml;
                                    int perm = m.perm;
                                    if (MapName.equals(client.kullanici.isim) || this.client.kullanici.isMapcrew || this.client.kullanici.yetki == 10) {
                                        this.client.room.editeurXML = MapXML;
                                        this.client.room.editeurPerm = perm;
                                        this.client.room.editeurName = MapName;
                                        this.client.room.editeurID = m.id;
                                        this.client.sendPacket(MFServer.pack_old(14, 9, new Object[]{MapXML, 0, 0, 0}));
                                    } else {
                                        this.client.sendPacket(MFServer.pack_old(14, 8, new Object[0]));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                    }


                } else {
                    this.client.sendPacket(MFServer.pack_old(14, 8, new Object[0]));
                }

            } else if (Token2 == 26) {

                this.client.sendPacket(MFServer.pack_old(14, 14, new Object[]{"0"}));
                // this.client.sendPacket(MFServer.getBuf().writeByte(7).writeByte(1).writeByte(0));
                // this.client.sendPacket(MFServer.getBuf().writeByte(7).writeByte(30).writeByte(1));
                this.client.enterRoom("1", false);
            }
        } // else {System.out.format("%s,%s [%s] \n", Token1, Token2, values);}

    }
}
