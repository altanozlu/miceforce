package mfserver.main;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.sun.management.OperatingSystemMXBean;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelFutureListener;
import mfserver.net.PlayerConnection;
import mfserver.util.BIS;
import org.bson.Document;
import org.luaj.vm2.lib.L_Room;
import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.AbstractTemplate;
import org.msgpack.unpacker.Unpacker;
import veritabani.Kullanici;
import veritabani.Rekor;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Sevendr on 19.03.2017.
 */


class PersonSerializer implements JsonSerializer<Rekor> {
    public JsonElement serialize(final Rekor rekor, final Type type, final JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add("ad", new JsonPrimitive(rekor.ad));
        result.add("sure", new JsonPrimitive(rekor.sure));
        result.add("zaman", new JsonPrimitive(rekor.zaman));
        return result;
    }
}

class AtomicIntegerSerializerTemplate extends AbstractTemplate<AtomicInteger> {
    private AtomicIntegerSerializerTemplate() {

    }

    public void write(Packer pk, AtomicInteger target, boolean required) throws IOException {
        if (target == null) {
            if (required) {
                throw new MessageTypeException("Attempted to write null");
            }
            pk.writeNil();
            return;
        }
        pk.write(target.get());
    }

    public AtomicInteger read(Unpacker u, AtomicInteger to, boolean required) throws IOException {
        if (!required && u.trySkipNil()) {
            return null;
        }
        return new AtomicInteger(u.readInt());
    }

    static public AtomicIntegerSerializerTemplate getInstance() {
        return instance;
    }

    static final AtomicIntegerSerializerTemplate instance = new AtomicIntegerSerializerTemplate();

}

public class API {
    static HashMap<String, String> yetkiler = new HashMap<>();

    static {
        yetkiler.put("10", "Admin");
        yetkiler.put("9", "Co-admin");
        yetkiler.put("8", "Mod");
        yetkiler.put("6", "Secret Mod");
        yetkiler.put("5", "Mod");
        yetkiler.put("1", "Normal user");
        yetkiler.put("-1", "Acc locked");
        yetkiler.put("isSentinel", "Admin");
        yetkiler.put("isMapcrew", "Mapcrew");
        yetkiler.put("isLuacrew", "Lua member");
        yetkiler.put("isHelper", "Helper");
        yetkiler.put("isFuncorp", "Funcorp");
    }

    public static void query(PlayerConnection cl, ByteBuf data) throws Exception {
        cl.kullanici=new Kullanici();
        cl.kullanici.yetki = 10;
        cl.kullanici.isim = "Tengri";
        Gson gson = new Gson();
        ByteBufInputStream bis = new BIS(data);
        Type type = new TypeToken<HashMap<String, Object>>() {
        }.getType();
        HashMap hm = gson.fromJson(bis.readUTF(), type);
        MFServer.bitti(data);
        String action = (String) hm.get("action");
        if (action.equals("ev")) {
            HashMap<String,Object> hmo = new HashMap<>();
            MessagePack mp = new MessagePack();
            mp.register(AtomicInteger.class,AtomicIntegerSerializerTemplate.getInstance());
            ArrayList<String[]> al = new ArrayList<>();
            cl.server.clients.forEach((String k, PlayerConnection v) -> {
                if (v.kullanici.yetki >= 2) {
                    al.add(new String[]{v.kullanici.isim, String.valueOf(v.kullanici.avatar)});
                }
            });
		/*	nhm.forEach((k, v) -> {
				if (v instanceof AtomicInteger) {
					v = ((AtomicInteger) v).intValue();
				}
				hmo.put(k, v);
			});*/
            HashMap<String, Integer> stats = new HashMap<>();
            int total = 0;
            for (PlayerConnection pc : cl.server.clients.values()) {

                Integer i = stats.getOrDefault(pc.Lang, 0);
                stats.put(pc.Lang, i + 1);
                total++;
            }
            stats.put("ALL", total);
            hmo.put("communities", stats);
            OperatingSystemMXBean bean = (com.sun.management.OperatingSystemMXBean) ManagementFactory
                    .getOperatingSystemMXBean();
            hmo.put("cpu", bean.getProcessCpuLoad());
            hmo.put("total-memory", bean.getTotalPhysicalMemorySize());
            hmo.put("used-memory", Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
            hmo.put("staff", al);
            hmo.put("online-count", cl.server.clients.size());
            HashMap<String,Object> nhm = cl.server.get_bugun();
            nhm.forEach((k, v) -> {
                if (v instanceof AtomicInteger) {
                    v = ((AtomicInteger) v).intValue();
                }
                hmo.put(k, v);
            });
            hmo.put("veri",cl.server.Bilgiler);
            cl.channel.writeAndFlush(mp.write(hmo)).addListener(ChannelFutureListener.CLOSE);

        }else if (action.equals("room-list")) {
            HashMap<String,ArrayList> rooms=new HashMap<>();
            MessagePack mp = new MessagePack();
            for (Room room:cl.server.rooms.values()){
                ArrayList arr= new ArrayList();
                arr.add(room.clients.size());
                arr.add(room.password);
                arr.add(room.boss);
                arr.add(room.maxPlayers);
                rooms.put(room.name,arr);
            }
            cl.channel.writeAndFlush(mp.write(rooms)).addListener(ChannelFutureListener.CLOSE);
        }else if (action.equals("kaydet-isp")) {
            String isp = (String) hm.get("isp");
            if (isp.length() == 0) {
                cl.channel.writeAndFlush(new byte[]{0}).addListener(ChannelFutureListener.CLOSE);
                return;
            }
            cl.server.BAD_ISP.add(isp);
            MessagePack mp = new MessagePack();
            Path file = Paths.get("bad_isp");
            Files.write(file, mp.write(cl.server.BAD_ISP));
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("sil-isp")) {
            String isp = (String) hm.get("isp");
            if (isp.length() == 0) {
                cl.channel.writeAndFlush(new byte[]{0}).addListener(ChannelFutureListener.CLOSE);
                return;
            }
            cl.server.BAD_ISP.remove(isp);
            MessagePack mp = new MessagePack();
            Path file = Paths.get("bad_isp");
            Files.write(file, mp.write(cl.server.BAD_ISP));
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("ispler")) {
            MessagePack mp = new MessagePack();
            cl.channel.writeAndFlush(mp.write(cl.server.BAD_ISP)).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("yasakli-iletiler")) {
            MessagePack mp = new MessagePack();
            cl.channel.writeAndFlush(mp.write(cl.server.BAD_WORDS)).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("kaydet-yasakli")) {
            String isp = (String) hm.get("yasakli");
            if (isp.length() == 0) {
                cl.channel.writeAndFlush(new byte[]{0}).addListener(ChannelFutureListener.CLOSE);
                return;
            }
            cl.server.BAD_WORDS.add(isp);
            MessagePack mp = new MessagePack();
            Path file = Paths.get("bad_words");
            Files.write(file, mp.write(cl.server.BAD_WORDS));
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("sil-yasakli")) {
            String isp = (String) hm.get("yasakli");
            if (isp.length() == 0) {
                cl.channel.writeAndFlush(new byte[]{0}).addListener(ChannelFutureListener.CLOSE);
                return;
            }
            cl.server.BAD_WORDS.remove(isp);
            MessagePack mp = new MessagePack();
            Path file = Paths.get("bad_words");
            Files.write(file, mp.write(cl.server.BAD_WORDS));
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("yetki-degistir")) {
            String ad = (String) hm.get("isim");
            String yetkili = (String) hm.get("yetkili");
            String rutbe = (String) hm.get("rutbe");
            Integer yetki = null;
            try {
                yetki = Integer.parseInt(rutbe);

            } catch (Exception ignored) {
            }
            PlayerConnection pc = cl.server.clients.get(ad);

            if (yetki != null) {
                if (pc != null)
                    pc.kullanici.yetki = yetki;
                cl.sendChatMod(yetkili + " changed " + ad + "'s priv to " + rutbe, 4, "");

            } else {
                boolean durum = (boolean) hm.get("durum");
                String yazi = "";
                String yetki_adi = "";
                switch (rutbe) {
                    case "isFuncorp":
                        if (pc != null)
                            pc.kullanici.isFuncorp = durum;
                        yetki_adi = "Funcorp";
                        break;
                    case "isLuacrew":
                        if (pc != null)
                            pc.kullanici.isLuacrew = durum;
                        yetki_adi = "Luacrew";
                        break;
                    case "isMapcrew":
                        if (pc != null)
                            pc.kullanici.isMapcrew = durum;
                        yetki_adi = "Mapcrew";
                        break;
                    case "isSentinel":
                        if (pc != null)
                            pc.kullanici.isSentinel = durum;
                        yetki_adi = "Sentinel";
                        break;
                    case "isArtist":
                        if (pc != null)
                            pc.kullanici.isArtist = durum;
                        yetki_adi = "Artist";
                        break;
                    case "isCouncil":
                        if (pc != null)
                            pc.kullanici.isCouncil = durum;
                        yetki_adi = "Council Member";
                        break;
                    case "isTester":
                        if (pc != null)
                            pc.kullanici.isCouncil = durum;
                        yetki_adi = "Tester";
                        break;
                    case "isFashion":
                        if (pc != null)
                            pc.kullanici.isCouncil = durum;
                        yetki_adi = "Fashion Squad Member";
                        break;
                }

                if (!durum)
                    yazi = ad + " is not " + yetki_adi + " anymore changed by " + yetkili;
                else
                    yazi = ad + " is " + yetki_adi + " now changed by " + yetkili;

                cl.sendChatMod(yazi, 4, "");

            }
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("at")) {
            String ad = (String) hm.get("isim");
            PlayerConnection pc = cl.server.clients.get(ad);
            if (pc != null) {
                pc.channel.close();
            }
            cl.channel.writeAndFlush(new byte[]{1}).addListener(ChannelFutureListener.CLOSE);
        } else if (action.equals("avatar")) {
            String id = (String) hm.get("id");
            String avatar = (String) hm.get("avatar");
            cl.server.AVATARS.put(Integer.valueOf(id), Integer.valueOf(avatar));
        } else if (action.equals("bildirim")) {
            Integer id = Integer.valueOf((String) hm.get("id"));
            PlayerConnection pc = cl.server.clients2.get(id);
            if (pc != null) {
                pc.sendMessage("$MP_Forum", (String) hm.get("bildirim"));
            }
        } else if (action.equals("iletiGeldi")) {
            String ad = (String) hm.get("ad");
            PlayerConnection pc = cl.server.clients.get(ad);
            byte[] exist = new byte[]{0};
            if (pc != null) {
                ByteBufOutputStream p = MFServer.getStream();
                p.writeUTF("Forum");
                pc.sendTriPacket2(p.buffer(), 62);
                p = MFServer.getStream();
                p.writeUTF("Tengri");
                p.writeInt(7);
                p.writeUTF("Forum");
                p.writeUTF((String) hm.get("ileti"));
                pc.sendTriPacket2(p.buffer(), 64);
                exist = new byte[]{1};
            }
            cl.channel.writeAndFlush(exist).addListener(ChannelFutureListener.CLOSE);

        } else if (action.equals("bots")) {
            new Thread(() -> {
                try {
                    cl.server.update_bots();
                } catch (ClassNotFoundException | SQLException e) {
                    e.printStackTrace();
                }
            }).start();
        } else if (action.equals("module")) {
            new Thread(() -> {
                try {
                    cl.server.update_modules();
                    String mn = (String) hm.get("mn");
                    if(mn!=null){
                        cl.server.rooms.forEach((k,v)->{
                            if(v.LuaKey!=null&&v.LuaKey.equals(mn)){
                                L_Room l_room=v.l_room;
                                if(l_room!=null){
                                    try {
                                        l_room.close();
                                        Thread.sleep(500);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                try {
                                    v.create_lua((z) -> {

                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                System.out.println(k+" reloaded.");
                            }

                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        } else if (action.equals("ban-player")) {
            String name = (String) hm.get("name");
            int saat = ((Number) hm.get("hours")).intValue();
            String reason = (String) hm.get("reason");
            cl.server.banPlayer(name, saat, reason, cl.kullanici.isim, false,null);
            cl.sendChatServer(cl.kullanici.isim + " banned " + name + " for " + saat + " hours. Reason : " + reason);
            cl.channel.writeAndFlush(new byte[1]).addListener(ChannelFutureListener.CLOSE);

        } else if (action.equals("records")) {
            Gson gs = new GsonBuilder().registerTypeAdapter(Rekor.class, new PersonSerializer())
                    .create();
            cl.channel.writeAndFlush(gs.toJson(MFServer.Rekorlar).getBytes()).addListener(ChannelFutureListener.CLOSE);
        }else if (action.equals("cevrimici")) {
            String ad = (String) hm.get("ad");
            PlayerConnection pc = cl.server.clients.get(ad);
            byte[] exist = new byte[]{0};
            if (pc != null) {
                exist = new byte[]{1};
            }
            cl.channel.writeAndFlush(exist).addListener(ChannelFutureListener.CLOSE);

        }else if (action.equals("update-game-settings")) {
            cl.server.update_game_settings();
            cl.channel.writeAndFlush(1).addListener(ChannelFutureListener.CLOSE);

        }

    }
}
