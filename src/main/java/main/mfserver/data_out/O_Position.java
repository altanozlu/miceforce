/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import io.netty.buffer.ByteBuf;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class O_Position extends base {

    public O_Position(PlayerConnection client, ByteBuf buf) throws IOException {
        super((byte) 4, (byte) 4, client);
        ByteBuf BUF = this.buf.buffer();
        buf.resetReaderIndex();
        // System.out.println(MFServer.bytesToHex(buf));
        // buf.skipBytes(2);
        BUF.writeInt(client.kullanici.code);
        BUF.writeBytes(buf);
    }
}
