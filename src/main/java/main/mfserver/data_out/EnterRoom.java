/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.Room;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class EnterRoom extends base {

    public EnterRoom(PlayerConnection client, Room room) throws IOException {
        super((byte) 5, (byte) 21, client);
        this.buf.writeBoolean(!room.password.isEmpty());
        this.buf.writeUTF(room.name);

    }

}
