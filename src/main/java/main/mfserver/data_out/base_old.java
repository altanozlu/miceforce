/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package mfserver.data_out;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import mfserver.main.MFServer;
import mfserver.main.Room;
import mfserver.net.PlayerConnection;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * @author sevendr
 */
public class base_old {
    public Object[] objects = new Object[0];
    private int t1;
    private int t2;
    private ByteBufOutputStream Buffer;

    protected base_old(int t1, int t2, PlayerConnection client) throws IOException {
        this.t1 = t1;
        this.t2 = t2;
        this.Buffer = client.getStream();

    }

    protected base_old(int t1, int t2) throws IOException {
        this.t1 = t1;
        this.t2 = t2;
        this.Buffer = MFServer.getStream();

    }
    protected base_old(int t1, int t2, Room room) throws IOException {
        this.t1 = t1;
        this.t2 = t2;
        this.Buffer = MFServer.getStream();

    }

    public ByteBuf data() throws IOException {

        String nPacket = "";
        if (objects.length >= 1) {
            nPacket = MFServer.x01 + StringUtils.join(objects, MFServer.x01);
        }
        int len2 = nPacket.length() + 2;
        this.Buffer.writeByte(1);
        this.Buffer.writeByte(1);
        this.Buffer.writeShort(len2);
        this.Buffer.writeByte(t1);
        this.Buffer.writeByte(t2);
        this.Buffer.writeBytes(nPacket);
        return this.Buffer.buffer();

    }

}
