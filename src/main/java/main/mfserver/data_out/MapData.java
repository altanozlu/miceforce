/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.MFServer;
import mfserver.main.Room;
import mfserver.util.map;

import java.io.IOException;

/**
 * @author sevendr
 */
public class MapData extends base {

    public MapData(Room room) throws IOException {
        super((byte) 5, (byte) 2, room);
        this.write(room, 0, "", "", 0);

    }

    public MapData(Room room, map Map) throws IOException {
        super((byte) 5, (byte) 2, room);
        if(Map==null){
            this.write(room, 0, "", "", 0);
        }
        else{

            this.write(room, Map.id, Map.xml, Map.name, Map.perm);
        }

    }

    public void write(Room room, int id, String xml, String name, int perm) throws IOException {
        Integer isLev = MFServer.leveMaps2.get(id);
        if (isLev != null) {
            id = isLev;
        }
        this.buf.writeInt(id);
        this.buf.writeShort(room.playerCount());
        this.buf.writeByte(room.Round);
        if (xml.length() == 0) {
            this.buf.writeInt(0);
        } else {
            MFServer.Zlib(this.buf, xml);
        }
        this.buf.writeUTF(name);
        this.buf.writeByte(perm);
        this.buf.writeBoolean(room.ayna);
        // this.buf.write(new byte[9000]);
    }

}
