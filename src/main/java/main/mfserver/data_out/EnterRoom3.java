/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.Room;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class EnterRoom3 extends base {
    public EnterRoom3(PlayerConnection client, Room room) throws IOException {
        super((byte) 7, (byte) 30, client);
        if (room.isBootcamp) {
            this.buf.writeByte(2);
        } else if (room.isVanilla) {
            this.buf.writeByte(3);
        } else if (room.isSurvivor) {
            this.buf.writeByte(8);
        } else if (room.isRacing) {
            this.buf.writeByte(9);
        } else if (room.isDefilante) {
            this.buf.writeByte(10);
        } else if (room.isMusic) {
            this.buf.writeByte(11);
        } else if (room.is801Room) {
            this.buf.writeByte(16);
        } else {
            this.buf.writeByte(1);
        }

    }
}
