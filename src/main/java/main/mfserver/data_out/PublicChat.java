/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mfserver.data_out;

import mfserver.main.MFServer;
import mfserver.main.Room;
import mfserver.net.PlayerConnection;

import java.io.IOException;

import static mfserver.main.Room.FuncorpSettings.packColor;

/**
 * @author sevendr
 */
public class PublicChat extends base {

    public PublicChat(PlayerConnection cl, String message,boolean original) throws IOException {
        super((byte) 6, (byte) 6, cl);
        String name= MFServer.plusReady(cl.kullanici);
        Room room=cl.room;
        if(room!=null&&room.isFuncorp && room.funcorpSettings != null&& !original){
            String color = room.funcorpSettings.chatColors.getOrDefault(MFServer.plusReady(MFServer.noTag(name)), "");
            message=packColor(message,color);
            name = room.funcorpSettings.miceNames.getOrDefault(MFServer.noTag(name), name);
        }
        this.buf.writeInt(cl.kullanici.code);//101
        this.buf.writeUTF(name);//29
        this.buf.writeByte(cl.LangByte);//7150
        this.buf.writeUTF(message);//207

    }

}
