/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package mfserver.data_out;

import io.netty.buffer.ByteBuf;
import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;

/**
 * @author sevendr
 */
public class ExampleTribulle {
    private final ByteBuf Buffer;
    private final int Req;
    private final int Stat;
    private int t1;

    public ExampleTribulle(int Req, int Stat, PlayerConnection client) throws IOException {

        this.Buffer = client.getBuf();
        this.Req = Req;
        this.Stat = Stat;

    }

    public ExampleTribulle(int Req, int Stat) throws IOException {

        this.Buffer = MFServer.getBuf();
        this.Req = Req;
        this.Stat = Stat;

    }

    public ByteBuf data() throws IOException {

        this.Buffer.writeInt(this.Req);
        this.Buffer.writeByte(this.Stat);
        return this.Buffer;

    }

}
