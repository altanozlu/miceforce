package mfserver.komutlar;

import mfserver.net.PlayerConnection;

import java.io.IOException;

public class isimrenk extends temel {
    public isimrenk(){
        super("isimrenk");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (client.room.isIsimRenkDisabled){
            return;
        }
        if (args.length == 1) {
            client.kullanici.isimRenk = 0;
            return;
        }
        client.LuaGame();
        client.room.showColorPicker(777778, client.kullanici.isim, client.kullanici.isimRenk, "Isim Renk");

    }
}
