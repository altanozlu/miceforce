package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import veritabani.Kullanici;

import java.io.IOException;

import static com.mongodb.client.model.Filters.eq;

public class bf extends temel {
    public bf() {
        super("bf");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (args.length == 1) {
            MFServer.executor.execute(() -> {
                try {
                    Kullanici kul = MFServer.colKullanici.find(eq("isim", MFServer.noTag(MFServer.firstLetterCaps(args[0])))).first();
                    if (kul == null) {

                        client.sendMessage("<R>User doesn't exist.");

                    } else {

                        if (client.kullanici.bestFriends.contains(kul.code)) {
                            client.kullanici.bestFriends.remove(kul.code);
                            client.sendMessage("<VP>Has been removed from list.");
                        } else {
                            client.kullanici.bestFriends.add(kul.code);
                            client.sendMessage("<VP>Has been added to list.");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            // PlayerConnection playerConnection= client.server.clients.get( MFServer.noTag(MFServer.firstLetterCaps(args[0])));

        } else {

            client.sendMessage("<VP>Example usage is /bf sevenops.");
        }


    }
}
