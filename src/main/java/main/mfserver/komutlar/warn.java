package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import veritabani.Ceza;
import veritabani.Uyari;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Indexes.text;

public class warn extends temel {

    public warn() {
        super("warn");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (client.kullanici.yetki < 2) return;
        if (args.length >= 2) {
            String name = MFServer.noTag(MFServer.firstLetterCaps(args[0]));
            if (MFServer.Forbidden_names.contains(name) && !client.kullanici.isim.equals("Sevenops")) {
                client.sendMessage("<R>Forbidden");
                return;
            }
            String reason = StringUtils.split(StringUtils.join(args, " "), " ", 2)[1];
            PlayerConnection pc = client.server.clients.get(name);
            if (pc == null) {
                client.sendMessage("<R>User is not online!");
            } else {
                MFServer.executor.execute(() -> {
                    try {
                        String lang = pc.room.lang;
                        if (lang.equals("XX")) {
                            lang = "YASA";
                        }
                        Uyari uyari = MFServer.colUyarilar.find(new Document("$text", new Document("$search", reason)).append("lang", lang)).projection(new Document("score", new Document("$meta", "textScore"))).sort(new Document("score", new Document("$meta", "textScore"))).first();
                        if (uyari == null) {
                            uyari = MFServer.colUyarilar.find(new Document("$text", new Document("$search", reason)).append("lang", "YASA")).projection(new Document("score", new Document("$meta", "textScore"))).sort(new Document("score", new Document("$meta", "textScore"))).first();

                        }
                        if (uyari == null) {
                            client.sendMessage("<R>Couldn't found any rule");
                            return;
                        } else {
                            ArrayList<Ceza> cezalar = new ArrayList<>();
                            for (Ceza ceza : MFServer.colCezaLog.find(new Document("ad", name).append("tip", 1)).sort(new Document("surev", -1)).limit(100)) {
                                Uyari eslesme = MFServer.colUyarilar.find(new Document("$text", new Document("$search", ceza.sebep)).append("_id", uyari._id)).first();
                                if (eslesme != null) {
                                    if (ceza.surec == 0 && ceza.surev + 1_000 * 60 * 30 > System.currentTimeMillis())
                                        cezalar.add(ceza);
                                    else if (ceza.surec > 0 && ceza.surev + 1_000 * 60 * 60 * 24 * 14 > System.currentTimeMillis()) {
                                        cezalar.add(ceza);
                                    }
                                }
                            }
                            if (cezalar.isEmpty()) {
                                client.server.mutePlayer(name, client, uyari.hours.get(0), reason, true);
                            } else {


                                int saat = uyari.hours.get(Math.min(uyari.hours.size() - 1, cezalar.size()));
                                boolean hidden = false;
                                if (client.kullanici.yetki <= 4) hidden = true;
                                client.server.mutePlayer(name, client, saat, reason, hidden);
                              /*  int index = 0;
                                int hours = 0;
                                if (cezalar.size() > uyari.hours.size()) {
                                    cezalar = cezalar.stream().skip(cezalar.size() - uyari.hours.size()).collect(Collectors.toCollection(ArrayList::new));
                                }
                                for (Ceza ceza : cezalar) {

                                    if (uyari.hours.size() >= index + 1) {
                                        //   if(uyari.hours.get())
                                        System.out.println(ceza.surec + " " + ceza.ad);
                                    } else System.out.println("else " + ceza.surec + " " + ceza.ad);
                                }*/
                            }
                          /*  Ceza ceza = new Ceza(ad, client.kullanici.isim, System.currentTimeMillis(), saat);
                            ceza.sebep = reason;

                            ceza.tip = 1;
                            MFServer.colCezaLog.insertOne(ceza);*/
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
            //String lang
        } else {
            client.sendMessage("<R>Wrong usage type /help");
        }

    }

    public String getDoc(PlayerConnection client) {
        if (client.kullanici.yetki >= 2) return "command_warn";
        return null;
    }
}
