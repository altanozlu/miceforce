package mfserver.komutlar;

import mfserver.main.MFServer;
import mfserver.net.PlayerConnection;

import java.io.IOException;
import java.util.regex.Matcher;

public class music extends temel {
    public music() {
        super("music", 1);
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        if (client.T_Music != null) {
            client.T_Music.cancel(false);
            client.T_Music = null;
        }

        if (client.room.isTribeHouse && client.tribe == client.room.tribe
                && client.tribe.checkPermissions("can_music", client)) {
            client.room.sendAllOld(26, 12, args);
        } else if (client.kullanici.yetki >= 4 || (client.kullanici.isFuncorp && client.room.isFuncorp)) {
            client.room.sendAllOld(26, 12, args);
        } else {
            client.sendPacket(MFServer.pack_old(26, 12, args));
        }


    }
}
