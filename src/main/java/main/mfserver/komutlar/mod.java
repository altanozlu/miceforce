package mfserver.komutlar;

import mfserver.net.PlayerConnection;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class mod extends temel {
    public mod(){
        super("mod");
    }

    public void isle(PlayerConnection client, String[] args) throws IOException {
        ArrayList<String> txt = new ArrayList<>();
        HashMap<String, ArrayList<String>> stat = new HashMap<>();
        for (Map.Entry<String, PlayerConnection> entry : client.server.clients.entrySet()) {
            PlayerConnection pc = entry.getValue();
            if (pc.kullanici != null && pc.kullanici.yetki >= 5 && !pc.topsecret && pc.kullanici.yetki != 6 && !pc.hidden) {
                stat.computeIfAbsent(pc.OrjLang, k -> new ArrayList<>()).add(pc.kullanici.isim);
            }
            if (pc.kullanici == null) {
                client.server.clients.remove(entry.getKey());
            }
        }
        if (stat.size() == 0) {
            txt.add("$ModoPasEnLigne");
        } else {
            txt.add("$ModoEnLigne");
        }
        for (Map.Entry<String, ArrayList<String>> entry : stat.entrySet()) {
            txt.add("<BL>[" + entry.getKey().toLowerCase() + "] <BV>"
                    + StringUtils.join(entry.getValue(), "<BL>, <BV>"));
        }
        client.sendMessage(StringUtils.join(txt, "\n"));

    }
}
