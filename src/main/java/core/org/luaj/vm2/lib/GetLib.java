/**
 * *****************************************************************************
 * Copyright (c) 2009-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import org.luaj.vm2.*;

/**
 * Subclass of {@link LibFunction} which implements the lua standard
 * {@code string} library.
 * <p>
 * Typically, this library is included as part of a call to either
 * {@link org.luaj.vm2.lib.jse.JsePlatform#standardGlobals()} or
 * <pre> {@code
 * Globals globals = JsePlatform.standardGlobals();
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * To instantiate and use it directly, link it into your globals table via
 * {@link LuaValue#load(LuaValue)} using code such as:
 * <pre> {@code
 * Globals globals = new Globals();
 * globals.load(new JseBaseLib());
 * globals.load(new PackageLib());
 * globals.load(new StringLib());
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * This is a direct port of the corresponding library in C.
 *
 * @see LibFunction
 * @see org.luaj.vm2.lib.jse.JsePlatform
 * @see <a href="http://www.lua.org/manual/5.2/manual.html#6.4">Lua 5.2 String
 * Lib Reference</a>
 */
public class GetLib extends TwoArgFunction {

    private L_Room room;

    /**
     * Construct a StringLib, which can be initialized by calling it with a
     * modname string, and a global environment table as arguments using
     * {@link #call(LuaValue, LuaValue)}.
     */
    public GetLib(L_Room room) {
        this.room = room;
    }

    public LuaValue call(LuaValue modname, LuaValue env) {

        LuaTable mnt = new LuaTable();
        mnt.set(LuaString.INDEX, new print());
        LuaTable t1 = new LuaTable();
        LuaTable t2 = new LuaTable();
        t2.setmetatable(mnt);
        try {
            t2.set("currentMap", room.currentMap);
            t2.set("name", room.room.outname);
            t2.set("community", room.room.community);
            t2.set("xmlMapInfo", room.xmlMapInfo);
        } catch (Exception e) {
            System.out.println(e);
        }
        LuaTable t3 = new LuaTable();
        t1.set("room", t2);
        env.get("tfm").set("get", t1);
        return env;
    }

    /**
     * Perform one-time initialization on the library by creating a table
     * containing the library functions, adding that table to the supplied
     * environment, adding the table to package.loaded, and returning table as
     * the return value. Creates a metatable that uses __INDEX to fall back on
     * itself to support string method operations. If the shared strings
     * metatable instance is null, will set the metatable as the global shared
     * metatable for strings.
     * <p>
     * All tables and metatables are read-write by default so if this will be
     * used in a server environment, sandboxing should be used. In particular,
     * the {@link LuaString#s_metatable} table should probably be made
     * read-only.
     */


    final class print extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String kom = args.tojstring(2);
                //  System.out.println(args.tojstring());
                switch (kom) {
                    case "playerList":
                        return room.playerList;
                    case "round":
                        return LuaInteger.valueOf(room.room.Round);
                    case "objectList":
                        return room.getObjectList();
                    case "currentMap":
                        return LuaString.valueOf(room.room.Map.id);
                    case "name":
                        return LuaString.valueOf(room.room.outname);
                    case "community":
                        return LuaString.valueOf(room.room.community);
                    case "unique":
                        return room.unique();
                    case "xmlMapInfo":
                        LuaTable xmlMapInfo = new LuaTable();

                        xmlMapInfo.set("xml", LuaString.valueOf(room.room.Map.xml));

                        xmlMapInfo.set("author", LuaString.valueOf(room.room.Map.name));
                        xmlMapInfo.set("mapCode", LuaInteger.valueOf(room.room.Map.id));
                        xmlMapInfo.set("permCode", LuaInteger.valueOf(room.room.Map.perm));
                        return xmlMapInfo;

                }
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

}
