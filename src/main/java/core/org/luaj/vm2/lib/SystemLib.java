/**
 * *****************************************************************************
 * Copyright (c) 2009-2011 Luaj.org. All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ****************************************************************************
 */
package org.luaj.vm2.lib;

import mfserver.main.MFServer;
import org.luaj.vm2.*;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Subclass of {@link LibFunction} which implements the lua standard
 * {@code string} library.
 * <p>
 * Typically, this library is included as part of a call to either
 * {@link org.luaj.vm2.lib.jse.JsePlatform#standardGlobals()} or
 * <pre> {@code
 * Globals globals = JsePlatform.standardGlobals();
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * To instantiate and use it directly, link it into your globals table via
 * {@link LuaValue#load(LuaValue)} using code such as:
 * <pre> {@code
 * Globals globals = new Globals();
 * globals.load(new JseBaseLib());
 * globals.load(new PackageLib());
 * globals.load(new StringLib());
 * System.out.println( globals.get("string").get("upper").call( LuaValue.valueOf("abcde") ) );
 * } </pre>
 * <p>
 * This is a direct port of the corresponding library in C.
 *
 * @see LibFunction
 * @see org.luaj.vm2.lib.jse.JsePlatform
 * @see <a href="http://www.lua.org/manual/5.2/manual.html#6.4">Lua 5.2 String
 * Lib Reference</a>
 */
public class SystemLib extends TwoArgFunction {

    public static HashSet<Integer> CL = new HashSet();

    static {
        CL.add(1);
        CL.add(2);
        CL.add(3);
        CL.add(4);
        CL.add(5);
        CL.add(6);
        CL.add(7);
        CL.add(8);
        CL.add(9);
        CL.add(10);
        CL.add(11);
        CL.add(12);
        CL.add(13);
        CL.add(14);
        CL.add(15);
        CL.add(16);
        CL.add(17);
        CL.add(18);
        CL.add(19);
        CL.add(20);
        CL.add(21);
        CL.add(22);
        CL.add(23);
        CL.add(24);
        CL.add(26);
        CL.add(27);
        CL.add(28);
        CL.add(29);
        CL.add(30);
        CL.add(31);
        CL.add(32);
        CL.add(33);
        CL.add(34);
        CL.add(35);
        CL.add(36);
        CL.add(37);
        CL.add(38);
        CL.add(39);
        CL.add(40);
        CL.add(41);
        CL.add(42);
        CL.add(43);
        CL.add(44);
        CL.add(45);
        CL.add(46);
        CL.add(47);
        CL.add(48);
        CL.add(49);
        CL.add(50);
        CL.add(51);
        CL.add(52);
        CL.add(53);
        CL.add(54);
        CL.add(55);
        CL.add(56);
        CL.add(57);
        CL.add(58);
        CL.add(59);
        CL.add(60);

    }

    private L_Room room;

    /**
     * Construct a StringLib, which can be initialized by calling it with a
     * modname string, and a global environment table as arguments using
     * {@link #call(LuaValue, LuaValue)}.
     */
    public SystemLib(L_Room room) {
        this.room = room;
    }

    public LuaValue call(LuaValue modname, LuaValue env) {

        LuaTable string = new LuaTable();

        string.set("bindKeyboard", new bindKeyboard());
        string.set("disableChatCommandDisplay", new disableChatCommandDisplay());
        string.set("newTimer", new newTimer());
        string.set("removeTimer", new removeTimer());
        string.set("bindMouse", new bindMouse());
        string.set("addCharacter", new addCharacter());
        string.set("sendBots", new sendBots());
        string.set("giveTitle", new giveTitle());
        string.set("giveBadge", new giveBadge());
        string.set("giveConsumables", new giveConsumables());
        string.set("loadPlayerData", new loadPlayerData());
        string.set("savePlayerData", new savePlayerData());
        string.set("addBot", new addBot());
        string.set("bilgi", new bilgi());
        string.set("getUsernameById", new getUsernameById());
        string.set("kickPlayer", new kickPlayer());
        string.set("blockPlayer", new blockPlayer());
        string.set("eventMs", new eventMs());
        string.set("disableStats", new disableStats());
        string.set("disableTags", new disableTags());
        string.set("localSync", new localSync());
        string.set("deleteTitle", new deleteTitle());
        string.set("playMusic", new playMusic());
        string.set("movePlayer", new movePlayer());
        string.set("consumableCount", new consumableCount());
        string.set("deleteConsumable", new deleteConsumable());
        string.set("newRecord", new newRecord());
        string.set("rooms", new rooms());
        env.set("system", string);
        return env;
    }

    final class newRecord extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String name = args.checkjstring(1);
                boolean R = args.checkboolean(2);
                room.newRecord(name, R);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class consumableCount extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String name = args.checkjstring(1);
                int R = args.checkint(2);
                return LuaInteger.valueOf(room.consumableCount(name, R));
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class deleteConsumable extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String name = args.checkjstring(1);
                int R = args.checkint(2);
                int miktar = args.checkint(3);
                return LuaInteger.valueOf(room.deleteConsumable(name, R,miktar));
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class rooms extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String name = args.checkjstring(1);
                return room.rooms(name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class movePlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String name = args.checkjstring(1);
                String R = args.checkjstring(2);
                room.movePlayer(name, R);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class playMusic extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String link = args.optjstring(1, "");
                String name = args.optjstring(2, "");
                room.playMusic(link, name);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class localSync extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean localSync = args.optboolean(1, true);

                room.room.localSync = localSync;
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableTags extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean ms = args.optboolean(1, false);

                room.room.TagsEnabled = ms;
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableStats extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                boolean ms = args.checkboolean(1);

                room.setStats(ms);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class eventMs extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                int ms = args.checkint(1);
                if (ms > 10) room.setMs(ms);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class kickPlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String playerName = args.checkjstring(1);
                room.kickPlayer(playerName);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class blockPlayer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String playerName = args.checkjstring(1);
                boolean block = args.optboolean(2, true);
                room.blockPlayer(playerName, block);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class savePlayerData extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String playerName = args.checkjstring(1);
                String veri = args.checkjstring(2);
                room.savePlayerData(playerName, veri);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class bilgi extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String playerName = args.checkjstring(1);
                return room.bilgi(playerName);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }
    final class getUsernameById extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                int playerID = args.checkint(1);
                return room.getUsernameById(playerID);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class addBot extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int code = args.checkint(1);
                String name = args.checkjstring(2);
                int title = args.optint(3, 0);
                String look = args.optjstring(4, "1;0,0,0,0,0,0");
                int x = args.optint(5, 0);
                int y = args.optint(6, 0);
                String targetPlayer = args.optjstring(7, "");
                room.addBot(code, name, title, look, x, y, targetPlayer);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class loadPlayerData extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String playerName = args.checkjstring(1);
                room.loadPlayerData(playerName);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveTitle extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String TARGET = args.checkjstring(1);
                int title = args.checkint(2);
                room.giveTitle(TARGET, title);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class deleteTitle extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String TARGET = args.checkjstring(1);
                int title = args.checkint(2);
                room.deleteTitle(TARGET, title);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveBadge extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String TARGET = args.checkjstring(1);
                int badge = args.checkint(2);
                room.giveBadge(TARGET, badge);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class giveConsumables extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {

                String TARGET = args.checkjstring(1);
                int esya = args.checkint(2);
                int sayi = args.checkint(3);
                room.giveConsumables(TARGET, esya, sayi);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }


    final class sendBots extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String TARGET = args.optjstring(1, "");
                for (ArrayList x : MFServer.instance.bots.values()) {
                    room.addBot((int) x.get(0), (String) x.get(1), (int) x.get(2), (String) x.get(3), (int) x.get(4), (int) x.get(5), TARGET);

                }
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;

        }
    }

    final class addCharacter extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int p_id = args.checkint(2);
                String name = args.checkjstring(1);
                if (CL.contains(p_id)) {

                    return LuaInteger.valueOf(room.addImage("foto_character_" + p_id, "%" + name, -25, -40, ""));

                }
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class bindMouse extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                boolean yes = args.optboolean(2, true);
                room.bindMouse(name, yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class removeTimer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                int id = args.checkint(1);
                room.removeTimer(id);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class newTimer extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                LuaFunction callback = args.checkfunction(1);
                int time = args.checkint(2);
                boolean loop = args.checkboolean(3);
                int len = args.narg() - 3;
                LuaValue[] vals = new LuaValue[len];
                for (int i = 4; i <= args.narg(); i++) {
                    vals[i - 4] = args.arg(i);
                }

                return LuaInteger.valueOf(room.newTimer(callback, time, loop, vals));
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class disableChatCommandDisplay extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String message = args.checkjstring(1);

                room.disableChatCommandDisplay(message);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

    final class bindKeyboard extends VarArgFunction {

        public Varargs invoke(Varargs args) {
            try {
                String name = args.checkjstring(1);
                int code = args.checkint(2);
                boolean down = args.checkboolean(3);
                boolean yes = args.optboolean(4, true);

                room.bindKeyboard(name, code, down, yes);
            } catch (LuaError er) {
                room.sendError(er);
            }
            return LuaValue.NIL;
        }
    }

}
