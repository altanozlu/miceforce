players = {}
resimler = {{x = 508, y = 215}, {x = 930, y = 225}, {x = 1602, y = 110}, {x = 1830, y = 250}, {x = 2135, y = 260}, {x = 2910, y = 260}, {x = 3250, y = 140}, {x = 3647, y = 250}, {x = 4045, y = 260}, {x = 4335, y = 120}}
sekerler = {"halloween_candy.12573", "halloween_candy.12574", "halloween_candy.12575", "halloween_candy.12576", "halloween_candy.12577"}
map = '<C><P defilante="-28,1,999,1,1" G="0,30" L="4800" Ca="" aie="" /><Z><S><S X="100" L="200" Y="400" H="20" P="0,0,0.3,0.2,0,0,0,0" T="0" /><S X="516" L="25" Y="386" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="515" L="25" Y="-8" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="719" L="25" Y="79" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="938" L="25" Y="33" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="939" L="25" Y="395" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="1195" L="25" Y="20" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="1374" L="25" Y="286" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="1539" L="25" Y="288" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="1840" L="25" Y="69" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="1840" L="25" Y="406" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="2137" L="25" Y="101" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="2138" L="25" Y="474" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="2405" L="25" Y="17" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="2462" L="25" Y="402" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="2739" L="25" Y="101" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="3077" L="25" Y="102" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="3355" L="25" Y="380" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="3553" L="25" Y="101" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="3799" L="25" Y="299" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="4047" L="25" Y="101" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="4049" L="25" Y="424" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /><S X="4335" L="25" Y="300" H="200" P="0,0,0,20,0,0,0,0" T="3" m="" /></S><D><DS X="71" Y="375" /><F X="4777" Y="24" /><F X="4778" Y="52" /><F X="4778" Y="79" /><F X="4778" Y="106" /><F X="4778" Y="131" /><F X="4778" Y="160" /><F X="4778" Y="187" /><F X="4778" Y="214" /><F X="4779" Y="242" /><F X="4779" Y="270" /><F X="4779" Y="297" /><F X="4779" Y="324" /><F X="4779" Y="350" /><F X="4779" Y="375" /><F X="4779" Y="402" /></D><O /></Z></C>'

function main()
    tfm.exec.disableAutoShaman(true)
    tfm.exec.disableAfkDeath(true)
    
    for n in pairs(tfm.get.room.playerList) do
    	mf.freeze(n, true)
        eventNewPlayer(n)
        mf.changeLook(n, "1;17_1E1E1E+E29C33+B58D45,8_3F3F3F,0,0,9_,4_36271E,0,0,0,3,0")
    end
end

function eventNewPlayer(n)
	players[n] = {
    	resimler = {},
    	score = 0
	}
	system.bindKeyboard(n, 32, true, true)
	system.bindKeyboard(n, 38, true, true)
	system.bindKeyboard(n, 65, true, true)
end

function eventNewGame()		
	ui.setMapName("Halloween <bl>- @2")
	local xml = tfm.get.room.xmlMapInfo
	xml = xml and xml.xml or ''
  
	for params in xml:gmatch('<S (.-) />') do
    	if params:match('T="3" m=""') then
    		local prop = {}
			for attr, val in params:gmatch('(%S+)="(%S+)"') do
				prop[attr:lower()] = tonumber(val) or val
			end

			tfm.exec.addImage("e66d504e9e2111e7af4488d7f643024a.png", "_100", prop.x - 27, prop.y - prop.h + (prop.y >= 200 and 100 or -100))
		end
	end

	for n,p in pairs(tfm.get.room.playerList) do
		players[n].resimler = {}
		players[n].score = 0
		--tfm.exec.addImage("93cbf2929d6711e7af4488d7f643024a.png", "%"..n, -60,-30)
		--tfm.exec.addImage("broomxdx-png.12557", "$"..n, -40,-10)
		mf.freeze(n, true)
		for a, b in pairs(resimler) do
	        table.insert(players[n].resimler, {x = b.x, y = b.y, img = tfm.exec.addImage(sekerler[math.random(#sekerler)], "!31", b.x, b.y-20, n)})
	    end

  		addBackground()
  	end
end

function particle(id, x, y, r, s)
	for i = 0, 360, 5 do
		tfm.exec.displayParticle(id, x + math.cos(i) * r, y + math.sin(i) * r, math.cos(i) * s, math.sin(i) * s)
	end
end

function addBackground(n)
  for i = 0, 5 do
    tfm.exec.addImage("1b8a541e9e3311e7af4488d7f643024a.jpg", "?0", 800 * i, 0, n)
    tfm.exec.addImage("b92ca12c9e3311e7af4488d7f643024a.png", "!0", 800 * i, 293, n)
  end
end

function allahefekti(x, y, n)
    local particles = {3}
    for i = 0, 5 do
        tfm.exec.displayParticle(particles[math.random(#particles)], x, y, math.cos(i) * 5 * math.random(50), math.sin(i) * 5 * math.random(50), 0, 0, n)
    end
end

function eventKeyboard(n, k, d, x, y)
	if k == 32 or k==38 or k == 65 then
        for a, b in pairs(players[n].resimler) do
            if (math.abs(b.x - x) < 40 and math.abs(b.y - y) < 40) and not b.got then
                b.got = true
                tfm.exec.removeImage(b.img)
                allahefekti(x, y, n)
				system.giveConsumables(n, 2202, 1)
            end
        end
    end
end

function eventPlayerGetCheese(n)
	tfm.exec.playerVictory(n)
	system.giveConsumables(n, 2338, 1)
end

main()
tfm.exec.newGame("@948")