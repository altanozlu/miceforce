tfm.exec.disableAfkDeath(true)
tfm.exec.disableAutoShaman(true)
tfm.exec.disablePhysicalConsumables(true)
players={}

translation = {
	["TR"] = {
		["welcome"] = "<vp>[Jesse]<n> Hey <j><b>${x}</b><n>! Arılara yakalanmadan peyniri kovana getirmeye çalış! Altın oklar, tünel giriş ve çıkışlarını gösterir.",
		["stung"] = "<r><b>~ Ah! Bir arıya yakalandın!</b><n>",
		["mn"] = "Kovana istila!"
	},

	["EN"] = {
		["welcome"] = "<vp>[Jesse]<n> Hey <j><b>${x}</b><n>! Bring the cheese to hives and try not to get stung! The golden arrows point to tunnels that you can go in and out of.",
		["stung"] = "<r><b>~ Ow! A bee stung you!</b><n>",
		["mn"] = "Invade the Hive!"
	},

	["ES"] = {
        ["welcome"] = "<vp>[Jesse]<n> ¡Hola <j><b>${x}</b><n>! ¡Trae el queso a las Colmenas evitando ser picado! Las flechas doradas indican tuneles por los que puedes entrar y salir.",
        ["stung"] = "<r><b>~ ¡Ay! ¡Una abeja te ha picado!</b><n>",
        ["mn"] = "¡Invade la Colmena!"
    },

	["RO"] = {
		["welcome"] = "<vp>[Jesse]<n> Bună! <j><b>${x}</b><n>! Adu brânza la stupi și încearcă să nu fii înțepat! Săgețile aurii indică tunelele prin care poți intra și ieși.",
		["stung"] = "<r><b>~ Ow! O albină te-a înțepat!</b><n>",
		["mn"] = "Invadează Stupul!"
	},

	["PL"] = {
		["welcome"] = "<vp>[Jesse]<n> Hej <j><b>${x}</b><n>! Przynieś ser do uli i postaraj się nie zostać ukąszonym! Złote strzałki wskazują na tunele, do których można wejść oraz wyjść.", 
		["stung"] = "<r><b>~ Ow! Pszczoła ukąsiła cię!</b><n>",
		["mn"] = "Najedź na ul!"    
    },

    ["AR"] = {
        ["welcome"] = "<vp>[Jesse]<n> اهلا <j><b>${x}</b><n>! خذ الجبن الى الحفرة و حاول ان لا تتعرض للقرص ! الأسهم الذهبية تبين أنفاقا يمكنك أن تدخل إليها و تخرج منها",
        ["stung"] = "<r><b>آه! قرصتك نحلة~</b><n>",
        ["mn"] = "!اغزوا الخلية"
    },

    ["RU"] = {
        ["welcome"] = "<vp>[Jesse]<n> Приветики <j><b>${x}</b><n>!  Принесите сыр к ульям и постарайтесь не попасться! Золотые стрелочки указывают на туннели, в которые вы можете входить и выходить.",
        ["stung"] = "<r><b>~ Ауу! Злая пчела ужалила тебя!</b><n>",
        ["mn"] = "Вторжение в улей!"
    },

    ["FR"] = {
        ["welcome"] = "<vp>[Jesse]<n> Salut <j><b>${x}</b><n>! Apporte le fromage à la ruche sans te faire piquer! Les flèches d'or pointent les tunnels dont vous pouvez entrer et sortir de.",
        ["stung"] = "<r><b>~ Ow! Une abeille vous a piqué!</b><n>",
        ["mn"] = "Envahissez la ruche!"
    },

    ["CZ"] = {
        ["welcome"] = "<vp>[Jesse]<n> Dobrý den <j><b>${x}</b><n>! Přiveďte sýr na včely a snažte se neubližovat! Zlaté šipky ukazují na tunely, kam můžete vstoupit a opustit. ",
        ["stung"] = "<r><b>~ Oh! Včelka vás udřela! </b><n>",
        ["mn"] = "Útočí úl!"
    },

    ["BR"] = {
        ["welcome"] = "<vp>[Jesse]<n> Hey <j><b>${x}</b><n>! Traga o queijo para as colmeias e tente não ser picado! As flechas de ouro apontam para túneis que você pode entrar e sair.",
        ["stung"] = "<r><b>~ Ow! Uma abelha te picou!</b><n>",
        ["mn"] = "Invada a Colmeia!"
    },

    ["HU"] = {
        ["welcome"] = "<vp>[Jesse]<n> Szia <j><b>${x}</b><n>! Juttasd el a sajtot a méhkashoz és kerüld el a méheket! Az arany nyílak mutatják hogy merre található az alagút amiből ki-be mászkálhatsz.",
        ["stung"] = "<r><b>~ Áucs! Egy méh megcsípett!</b><n>",
        ["mn"] = "Támad a méhkast!"
    }
}

tfm.exec.newGame('<C><P H="1000" Ca="" D="map1bg.png" d="asdqwdqwdqwd.png" /><Z><S><S X="400" H="100" L="801" Y="950" P="0,0,0.3,0.2,0,0,0,0" m="" T="6" /><S X="405" o="324650" L="107" Y="874" H="10" m="" T="12" P="0,0,0.3,0.2,35,0,0,0" /><S X="356" o="324650" L="107" Y="826" H="10" m="" T="12" P="0,0,3,0.2,49,0,0,0" /><S X="320" o="324650" L="107" Y="777" H="10" m="" T="12" P="0,0,3,0.2,64,0,0,0" /><S X="302" o="324650" L="107" Y="731" H="10" m="" T="12" P="0,0,3,0.2,74,0,0,0" /><S X="101" o="324650" L="107" Y="880" H="10" m="" T="12" P="0,0,0.3,0.2,130,0,0,0" /><S X="130" o="324650" L="107" Y="847" H="10" m="" T="12" P="0,0,3,0.2,115,0,0,0" /><S X="148" o="324650" L="107" Y="834" H="10" m="" T="12" P="0,0,3,0.2,95,0,0,0" /><S X="220" o="ff" L="177" Y="731" H="10" m="" T="12" P="0,0,0.3,0.2,141,0,0,0" /><S X="205" o="ff" L="111" Y="601" H="10" m="" T="12" P="0,0,0.3,0.2,211,0,0,0" /><S X="156" o="324650" L="107" Y="621" H="10" m="" T="12" P="0,0,0.3,0.2,90,0,0,0" /><S X="273" o="324650" L="107" Y="524" H="10" m="" T="12" P="0,0,0.3,0.2,90,0,0,0" /><S X="294" o="324650" L="65" Y="451" H="10" m="" T="12" P="0,0,0.3,0.2,133,0,0,0" /><S X="345" o="324650" L="65" Y="425" H="10" m="" T="12" P="0,0,0.3,0.2,175,0,0,0" /><S X="316" o="324650" L="107" Y="407" H="10" m="" T="12" P="0,0,0.3,0.2,191,0,0,0" /><S X="341" o="324650" L="87" Y="406" H="10" m="" T="12" P="0,0,0.3,0.2,175,0,0,0" /><S X="387" o="324650" L="87" Y="399" H="10" m="" T="12" P="0,0,0.3,0.2,157,0,0,0" /><S X="420" o="324650" L="87" Y="420" H="10" m="" T="12" P="0,0,0.3,0.2,191,0,0,0" /><S X="279" o="324650" L="63" Y="265" H="10" m="" T="12" P="0,0,20,0.2,100,0,0,0" /><S X="288" o="324650" L="234" Y="117" H="10" m="" T="12" P="0,0,20,0.2,92,0,0,0" /><S X="273" o="324650" L="84" Y="306" H="10" m="" T="12" P="0,0,20,0.2,91,0,0,0" /><S X="229" o="ff" L="157" Y="462" H="10" m="" T="12" P="0,0,3,0.2,121,0,0,0" /><S X="197" o="324650" L="103" Y="651" H="10" m="" T="12" P="0,0,0.3,0.2,147,0,0,0" /><S X="214" o="324650" L="146" Y="371" H="10" m="" T="12" P="0,0,0.3,0.2,145,0,0,0" /><S X="159" o="324650" L="146" Y="366" H="10" m="" T="12" P="0,0,20,0.2,270,0,0,0" /><S X="70" o="324650" L="78" Y="623" c="2" H="10" m="" T="12" P="0,0,0.3,0.2,0,0,0,0" /><S X="69" o="324650" L="78" Y="300" c="2" H="10" m="" T="12" P="0,0,0.3,0.2,0,0,0,0" /><S X="398" H="25" L="800" Y="-8" P="0,0,0,0.2,0,0,0,0" m="" T="1" /><S X="7" H="25" L="800" Y="-364" P="0,0,0,0.2,89,0,0,0" m="" T="1" /><S X="250" o="ff" L="11" Y="632" H="10" m="" T="12" P="0,0,20,0.2,265,0,0,0" /><S X="49" o="324650" L="326" Y="464" c="2" H="10" m="" T="12" P="0,0,0.3,0.2,270,0,0,0" /><S X="92" o="324650" L="327" Y="464" c="2" H="10" m="" T="12" P="0,0,0.3,0.2,270,0,0,0" /><S X="94" H="10" L="331" Y="206" P="0,0,20,0.2,62,0,0,0" m="" T="4" /><S X="230" o="324650" L="107" Y="557" H="16" m="" T="12" P="0,0,0.3,0.2,27,0,0,0" /><S X="619" H="100" L="361" Y="942" P="0,0,0.3,0.2,0,0,0,0" m="" T="6" /><S X="148" H="10" L="75" Y="315" P="0,0,20,0.2,71,0,0,0" m="" T="4" /><S X="363" o="324650" L="31" Y="525" H="10" m="" T="12" P="0,0,0.3,0.2,144,0,0,0" /><S X="338" o="324650" L="31" Y="533" H="10" m="" T="12" P="0,0,0.3,0.2,181,0,0,0" /><S X="317" o="324650" L="31" Y="522" H="10" m="" T="12" P="0,0,0.3,0.2,235,0,0,0" /><S X="307" o="324650" L="31" Y="496" H="10" m="" T="12" P="0,0,0.3,0.2,262,0,0,0" /><S X="307" o="324650" L="31" Y="467" H="10" m="" T="12" P="0,0,0.3,0.2,276,0,0,0" /><S X="316" o="324650" L="31" Y="442" H="10" m="" T="12" P="0,0,0.3,0.2,303,0,0,0" /><S X="70" o="324650" L="31" Y="194" H="10" m="" T="12" P="0,0,0.3,0.2,332,0,0,0" /><S X="51" o="324650" L="31" Y="213" H="10" m="" T="12" P="0,0,0.3,0.2,296,0,0,0" /><S X="45" o="324650" L="31" Y="239" H="10" m="" T="12" P="0,0,0.3,0.2,270,0,0,0" /></S><D><F X="39" Y="897" /><DS X="736" Y="888" /><T X="342" Y="530" /><T X="352" Y="527" /><T X="354" Y="520" /><T X="354" Y="514" /><T X="356" Y="504" /><T X="358" Y="497" /><T X="359" Y="488" /><T X="359" Y="481" /><T X="352" Y="472" /><T X="349" Y="469" /><T X="342" Y="467" /><T X="335" Y="475" /><T X="329" Y="493" /><T X="329" Y="508" /><T X="339" Y="524" /><T X="344" Y="511" /><T X="319" Y="262" /><T X="324" Y="260" /><T X="331" Y="252" /><T X="334" Y="234" /><T X="338" Y="228" /><T X="337" Y="217" /><T X="327" Y="208" /><T X="324" Y="206" /><T X="318" Y="204" /><T X="306" Y="222" /><T X="307" Y="238" /><T X="315" Y="255" /><T X="96" Y="281" /><T X="86" Y="286" /><T X="97" Y="270" /><T X="76" Y="278" /><T X="75" Y="276" /><T X="93" Y="251" /><T X="64" Y="252" /><T X="61" Y="244" /><T X="80" Y="228" /></D><O /></Z></C>')

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end

function contains(table, val) for i=1,#table do if table[i] == val then return true end end return false end
function table.delete(tab, val) for k,v in pairs(tab) do if val == v then table.remove(tab, k) break end end end
function alive() local i = 0 for k, v in pairs(oyuncular) do if not v.isDead then i = i + 1 end end return i end

function eventNewGame()
	for n in pairs(tfm.get.room.playerList) do
		tfm.exec.chatMessage(param(getMsg("welcome"), { x = n }), n)
	end

	balon = 0

	system.addBot(-989, "Jesse", 3296, "104;25_896B51,0,0,2_B75857,21_896B51+DBD7D2,3_DBD7D2,0,0,0,0", 640, 890, nil)
	ui.setMapName(getMsg("mn"))
	anann=tfm.exec.addShamanObject(28, 70, 613, 0, 0, 0)
	tfm.exec.addImage("event-character-bee.8131","#"..anann, -40, -40)

	tfm.exec.addImage("whoot_whoot-png.8874", "?3", 95, 730)
	tfm.exec.addImage("whoot_whoota-png.8875", "?4", 300, 600)
	tfm.exec.addImage("whoot_whoot-png.8874", "?5", 110, 500)
	tfm.exec.addImage("whoot_whoota-png.8875", "?6", 300, 350)
end

function eventNewPlayer(n)
	ui.setMapName(getMsg("mn"), n)
	system.addBot(-989, "Jesse", 3296, "104;25_896B51,0,0,2_B75857,21_896B51+DBD7D2,3_DBD7D2,0,0,0,0", 640, 890, n)


	tfm.exec.addImage("whoot_whoot-png.8874", "?3", 95, 730, n)
	tfm.exec.addImage("whoot_whoota-png.8875", "?4", 300, 600, n)
	tfm.exec.addImage("whoot_whoot-png.8874", "?5", 110, 500, n)
	tfm.exec.addImage("whoot_whoota-png.8875", "?6", 300, 350, n)
end


function eventLoop(t,r)
	balon = balon + 1

	if balon == 25 then
		tfm.exec.removeObject(anann)

		anann=tfm.exec.addShamanObject(28, 70, 613, 0, 0, 0)
		tfm.exec.addImage("event-character-bee.8131","#"..anann, -40, -40)
		balon = 0
	end
end

function eventPlayerWon(n)
	system.giveConsumables(n, 783, math.random(1,3))
end