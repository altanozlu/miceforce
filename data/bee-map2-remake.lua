tfm.exec.disableAfkDeath(true)
tfm.exec.disableAutoShaman(true)
tfm.exec.disablePhysicalConsumables(true)
tfm.exec.disableAutoTimeLeft(true)
system.eventMs(11)
players = {}
oyuncular = {}
alive = 0
translation = {
	["TR"] = {
		["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Biraz tehlikeli olsa da, bu bal lezzetli bir peynir yapmak için harika. Arılara yakalanmadan toplayabildiğiniz kadar bal toplayın!",
		["welcome2"] = "<j><b>~ Peteklerden bal toplamak için SPACE (BOŞLUK) tuşuna basın.</b>",
		["stung"] = "<r><b>~ Ah! Bir arıya yakalandın!</b><n>",
		["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> ARILAR UYANDI!!!!! KOŞUUUUNNNNNN!!!!!!!",
		["mn"] = "Arı kovanı"
	},

	["EN"] = {
		["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> It's a bit dangerous, but this honey is great for making a delicious cheese. Don't wake the bees up and try to get as much honey as you can!",
		["welcome2"] = "<j><b>~ Press SPACEBAR to collect honey from honeycombs.</b>",
		["stung"] = "<r><b>~ Ow! A bee stung you!</b><n>",
		["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> BEES ARE AWAKE!!!!! RUUUUUUUNNNNNN!!!!!!!",
		["mn"] = "Beehive"
	},

	["ES"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Es un poco peligroso, pero ésta miel es perfecta para preparar un delicioso queso. ¡No despiertes a las abejas e intenta obtener la mayor cantidad de miel que puedas!",
        ["welcome2"] = "<j><b>~ Presiona la tecla ESPACIO para recolectar miel de los panales.</b>",
        ["stung"] = "<r><b>~ !Ay! ¡Una abeja te ha picado!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> ¡¡¡¡¡LAS ABEJAS HAN DESPERTADO!!!!! ¡¡¡¡¡CORREEEEEEEEEE!!!!!",
        ["mn"] = "Colmena"
    },

	["RO"] = {
		["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Este puțin periculos, dar această miere este minunată pentru a face o brânză delicioasă. Nu trezi albinele și încearcă să obții cât de multă miere poți!",
		["welcome2"] = "<j><b>~ Apasă SPACE pentru a colecta mierea din faguri.</b>",
		["stung"] = "<r><b>~ Ow! O albină te-a înțepat!</b><n>",
		["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> ALBINELE SUNT TREZEEE!!!!! FUUUUUUUGI!!!!!!!",
		["mn"] = "Stup de albine"
	},

    ["PL"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> To jest trochę niebezpieczne, lecz ten miód jest świetny do zrobienia pysznego sera. Nie obudź pszczół i postaraj się zebrać tyle miodu, ile potrafisz!",
        ["welcome2"] = "<j><b>~ Wciśnij SPACJĘ, aby zebrać miód z plastrów miodu.</b>",
        ["stung"] = "<r><b>~ Ow! Pszczoła ukąsiła cię!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> PSZCZOŁY SIĘ OBUDZIŁY!!!!! UCIEEEEEEEKAAAAJ!!!!!!!",
        ["mn"] = "Beehive"
    },

    ["AR"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> ان الامر خطير قليلا، لكن هذا العسل ممتاز لصنع جبن رائع. لا توقظوا النحل و حاولوا ان تحصلوا على اكبر كمية من العسل !",
        ["welcome2"] = "<j><b>.اضغطوا على زر المسافة لتآخذوا العسل من اقراص العسل~</b>",
        ["stung"] = "<r><b>آه! قرصتك نحلة~</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n>!!! النحل مستيقظ!!!! اهربوا",
        ["mn"] = "خلية النحل"
    },

    ["RU"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Это немного опасно, но этот мед отлично подходит для приготовления вкусного сыра. Не разбудите пчел и постарайтесь получить как можно больше меда!",
        ["welcome2"] = "<j><b>~ Нажмите ПРОБЕЛ для сбора меда из сот.</b>",
        ["stung"] = "<r><b>~ Ауу! Пчелка-злюка ужалила тебя!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> ПЧЕЛЫ ПРОСНУЛИСЬ!!!!! БЕЕГИИИИИИ!!!!!!!",
        ["mn"] = "Улей"
    },

    ["FR"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> C'est un peu dangereux, mais ce miel est génial pour préparer un fromage délicieux. Ne réveillez pas les abeilles et essayez de prendre le plus de miel que vous pouvez!",
        ["welcome2"] = "<j><b>~ Cliquez la BARRE D'ESPACE pour collecter du miel des nids d'abeilles.</b>",
        ["stung"] = "<r><b>~ Ow! Une abeille vous a piqué!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> LES ABEILLES SONT ÉVEILLÉES!!!!! COUUUUUUREEEEEZ!!!!!!!",
        ["mn"] = "Ruche"
    },

    ["CZ"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Je to trochu nebezpečné, ale tento med je vynikající pro výrobu lahodného sýra. Nebuď probouzet včely a pokusit se získat tolik medu, jak jen můžete!",
        ["welcome2"] = "<j><b>~ Stiskněte MEZERNÍK pro vyzvednutí medu .</b>",
        ["stung"] = "<r><b>~ Oh! Včelka vás udřela!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Včely jsou vzhůru!!!! RUNNNNNNNNNNNN!!!!",
        ["mn"] = "Úl"
    },

    ["BR"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n>  É um pouco perigoso, mas esse mel é ótimo para fazer um delicioso queijo. Não acorde as abelhas e tente obter o máximo de mel possível!",
        ["welcome2"] = "<j><b>~ Pressione a BARRA DE ESPAÇO para coletar o mel das colméias.</b>",
        ["stung"] = "<r><b>~ Ow! Uma abelha te picou!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> AS ABELHAS ACORDARAM!!!!! COOOOOOOORREEE!!!!!!!",
        ["mn"] = "Colméia"
    },

    ["HU"] = {
        ["welcome"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> Egy kicsit veszélyes, de a méz tökéletes hozzávaló egy finom sajt elkészítéséhez. Ne ébreszd fel a méheket és próbálj annyi mézet összegyűjteni amenyit csak tudsz!",
        ["welcome2"] = "<j><b>~ Nyomd meg a SZÓKÖZT , hogy összegyűjtsd a mézet.</b>",
        ["stung"] = "<r><b>~ Áucs! Egy méh megcsípett!</b><n>",
        ["awake"] = "<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> A MÉHEK JÖNNEK!!!!! FUTÁS!!!!!!!",
        ["mn"] = "Méhkas"
    }
}

tfm.exec.newGame('<C><P D="beehive_v2_by_wolvrns-dcgb1hh.png" Ca="" /><Z><S><S X="400" Y="388" L="799" H="29" T="4" P="0,0,20,0.2,0,0,0,0" m="" /><S X="400" Y="394" L="799" H="29" T="4" P="0,0,20,0.2,-2,0,0,0" m="" /><S X="350" Y="372" L="44" H="10" T="4" P="0,0,20,0.2,-28,0,0,0" m="" /><S X="373" Y="352" L="44" H="10" T="4" P="0,0,20,0.2,-53,0,0,0" m="" /><S X="390" Y="317" L="44" H="10" T="4" P="0,0,20,0.2,-75,0,0,0" m="" /><S X="378" Y="288" L="44" H="17" T="4" P="0,0,20,0.2,-90,0,0,0" m="" /><S X="376" Y="269" L="44" H="17" T="4" P="0,0,20,0.2,-98,0,0,0" m="" /><S X="357" Y="240" L="24" H="17" T="4" P="0,0,20,0.2,-98,0,0,0" m="" /><S X="341" Y="239" L="24" H="17" T="4" P="0,0,20,0.2,-98,0,0,0" m="" /><S X="324" Y="239" L="24" H="17" T="4" P="0,0,20,0.2,-98,0,0,0" m="" /><S X="309" Y="242" L="24" H="17" T="4" P="0,0,20,0.2,-98,0,0,0" m="" /><S X="295" Y="260" L="49" H="13" T="4" P="0,0,20,0.2,-92,0,0,0" m="" /><S X="289" Y="246" L="16" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="307" Y="231" L="46" H="10" T="4" P="0,0,20,0.2,-10,0,0,0" m="" /><S X="351" Y="228" L="46" H="10" T="4" P="0,0,20,0.2,2,0,0,0" m="" /><S X="389" Y="233" L="33" H="10" T="4" P="0,0,20,0.2,13,0,0,0" m="" /><S X="405" Y="249" L="33" H="10" T="4" P="0,0,20,0.2,82,0,0,0" m="" /><S X="407" Y="281" L="33" H="10" T="4" P="0,0,20,0.2,90,0,0,0" m="" /><S X="407" Y="308" L="24" H="10" T="4" P="0,0,20,0.2,90,0,0,0" m="" /><S X="404" Y="330" L="24" H="10" T="4" P="0,0,20,0.2,108,0,0,0" m="" /><S X="408" Y="345" L="24" H="10" T="4" P="0,0,20,0.2,66,0,0,0" m="" /><S X="428" Y="367" L="42" H="10" T="4" P="0,0,20,0.2,36,0,0,0" m="" /><S X="462" Y="373" L="104" H="10" T="4" P="0,0,20,0.2,13,0,0,0" m="" /><S X="383" Y="328" L="44" H="17" T="4" P="0,0,20,0.2,-105,0,0,0" m="" /><S X="488" Y="214" L="16" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="610" Y="220" L="16" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="610" Y="213" L="16" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="700" Y="316" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="721" Y="291" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="149" Y="246" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="87" Y="196" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="154" Y="168" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="157" Y="161" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="157" Y="153" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="162" Y="141" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="147" Y="117" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="149" Y="194" L="104" H="10" T="4" P="0,0,20,0.2,85,0,0,0" m="" /><S X="143" Y="197" L="104" H="10" T="4" P="0,0,20,0.2,89,0,0,0" m="" /><S X="86" Y="151" L="80" H="17" T="4" P="0,0,20,0.2,89,0,0,0" m="" /><S X="104" Y="134" L="14" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="133" Y="132" L="14" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="71" Y="116" L="142" H="17" T="4" P="0,0,20,0.2,0,0,0,0" m="" /><S X="38" Y="144" L="97" H="17" T="4" P="0,0,20,0.2,-30,0,0,0" m="" /><S X="24" Y="148" L="48" H="13" T="4" P="0,0,20,0.2,-90,0,0,0" m="" /><S X="24" Y="169" L="10" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="502" Y="199" L="46" H="10" T="4" P="0,0,20,0.2,-14,0,0,0" m="" /><S X="545" Y="196" L="46" H="10" T="4" P="0,0,20,0.2,5,0,0,0" m="" /><S X="587" Y="200" L="46" H="10" T="4" P="0,0,20,0.2,5,0,0,0" m="" /><S X="569" Y="211" L="16" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="548" Y="212" L="11" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="510" Y="206" L="11" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="531" Y="205" L="11" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="701" Y="288" L="46" H="13" T="4" P="0,0,20,0.2,89,0,0,0" m="" /><S X="707" Y="256" L="17" T="13" P="0,0,20,0.2,0,0,0,0" o="324650" m="" /><S X="745" Y="231" L="86" H="13" T="4" P="0,0,20,0.2,153,0,0,0" m="" /><S X="819" Y="193" L="86" H="13" T="4" P="0,0,20,0.2,153,0,0,0" m="" /><S X="1062" Y="305" L="528" H="660" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="536" Y="-89" L="1584" H="184" T="1" P="0,0,0,0.2,0,0,0,0" m="" /><S X="-126" Y="150" L="255" H="661" T="1" P="0,0,0,0.2,0,0,0,0" m="" /></S><D><DS X="770" Y="196" /></D><O></O></Z></C>')

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end
function table.delete(tab, val) for k,v in pairs(tab) do if val == v then table.remove(tab, k) break end end end

function test(n) 
if not players[n].sonradan then
		alive = alive - 1
		if alive <= 0 then
			tfm.exec.newGame()
		end
	end
end

function eventPlayerDied(n)
	test(n) 
end

function eventPlayerLeft(n)
	test(n) 
end

function eventPlayerWon(n)
	test(n) 
end


function eventNewGame()
	alive = 0
	awakeWarning = false
	countdown3 = false
	countdown2 = false
	countdown1 = false
	countdown = false

	ui.setMapName(getMsg("mn"))
	system.addBot(-989, "Jesse", 3296, "104;25_896B51,0,0,2_B75857,21_896B51+DBD7D2,3_DBD7D2,0,0,0,0", 191, 367, nil)
	tfm.exec.chatMessage(getMsg("welcome"))
	tfm.exec.chatMessage(getMsg("welcome2"))

	for n in pairs(tfm.get.room.playerList) do
		alive = alive + 1
    	players[n] = {
    		timestamp=os.time(),
    		uyari = false,
    		sonradan=false,
    		sans = 0,
    		bal = 0,
    		x = 0,
    		y = 0
    	}
    	tfm.exec.bindKeyboard(n, 32, true, true)
    	ui.removeTextArea(999, n)
    	bal1 = tfm.exec.addImage("153001147760966161-png.8877", "!996", 705, 25, n)
		bal2 = tfm.exec.addImage("153001147760966161-png.8877", "!997", 725, 25, n)
		bal3 = tfm.exec.addImage("153001147760966161-png.8877", "!998", 745, 25, n)
		bal4 = tfm.exec.addImage("153001147760966161-png.8877", "!999", 765, 25, n)
	end
end


function eventNewPlayer(n)
    players[n] = {
		timestamp=os.time(),
		uyari = false,
		sonradan=false,
    	sans = 0,
    	bal = 0,
		x = 0,
		y = 0
	}

	ui.setMapName(getMsg("mn"), n)
	system.addBot(-989, "Jesse", 3296, "104;25_896B51,0,0,2_B75857,21_896B51+DBD7D2,3_DBD7D2,0,0,0,0", 191, 367, n)

	players[n].sonradan = true
end

function falanfilan(n)
	players[n].bal = players[n].bal + 1

	if players[n].bal == 1 then
		tfm.exec.removeImage(bal1, n)
		tfm.exec.addImage("a_drop_of_honey_by_wolvrns-dcgagpf-png.8876", "!996", 705, 25, n)
		system.giveConsumables(n, 780, math.random(1,2))
		tfm.exec.animZelda(n)
	end

	if players[n].bal == 2 then
		tfm.exec.removeImage(bal2, n)
		tfm.exec.addImage("a_drop_of_honey_by_wolvrns-dcgagpf-png.8876", "!997", 725, 25, n)
		system.giveConsumables(n, 780, math.random(1,2))
		tfm.exec.animZelda(n)
	end

	if players[n].bal == 3 then
		tfm.exec.removeImage(bal3, n)
		tfm.exec.addImage("a_drop_of_honey_by_wolvrns-dcgagpf-png.8876", "!998", 745, 25, n)
		system.giveConsumables(n, 780, math.random(1,2))
		tfm.exec.animZelda(n)
	end

	if players[n].bal == 4 then
		tfm.exec.removeImage(bal4, n)
		tfm.exec.addImage("a_drop_of_honey_by_wolvrns-dcgagpf-png.8876", "!999", 765, 25, n)
		system.giveConsumables(n, 780, math.random(1,2))
		tfm.exec.animZelda(n)
	end
end
	
function eventKeyboard(n, k, d, x, y)
	if (k == 32 and tfm.get.room.playerList[n].x>=680 and tfm.get.room.playerList[n].x<= 800 and tfm.get.room.playerList[n].y>=280 and tfm.get.room.playerList[n].y<=368) and not tfm.get.room.playerList[n].isDead then
		if players[n].timestamp < os.time()-4000 then
			players[n].sans = math.random(1,1600)

			if players[n].sans >= 1 and players[n].sans <= 350 then
				tfm.exec.addShamanObject(24, tfm.get.room.playerList[n].x, tfm.get.room.playerList[n].y, 0, 0, 0)
				tfm.exec.chatMessage(getMsg("stung"), n)
				tfm.exec.killPlayer(n)
				tfm.exec.addImage("busted-beeeeeeeeeeeeeee.8429", "!2", 102, 5, n)
			elseif players[n].sans >= 350 and players[n].sans <= 1600 then
				falanfilan(n)
				
			end

			players[n].timestamp = os.time()
		end
	end

	if (k == 32 and tfm.get.room.playerList[n].x>=485 and tfm.get.room.playerList[n].x<= 612 and tfm.get.room.playerList[n].y>=25 and tfm.get.room.playerList[n].y<=202) and not tfm.get.room.playerList[n].isDead then
		if players[n].timestamp < os.time()-4000 then
			players[n].sans = math.random(1,1300)

			if players[n].sans >= 1 and players[n].sans <= 300 then
				tfm.exec.addShamanObject(24, tfm.get.room.playerList[n].x, tfm.get.room.playerList[n].y, 0, 0, 0)
				tfm.exec.chatMessage(getMsg("stung"), n)
				tfm.exec.killPlayer(n)
				tfm.exec.addImage("busted-beeeeeeeeeeeeeee.8429", "!2", 102, 5, n)
			elseif players[n].sans >= 300 and players[n].sans <= 1300 then
				falanfilan(n)
			end

			players[n].timestamp = os.time()
		end
	end

	if (k == 32 and tfm.get.room.playerList[n].x>=238 and tfm.get.room.playerList[n].x<= 361 and tfm.get.room.playerList[n].y>=250 and tfm.get.room.playerList[n].y<=380) and not tfm.get.room.playerList[n].isDead then
		if players[n].timestamp < os.time()-4000 then
			players[n].sans = math.random(1,2100)

			if players[n].sans >= 1 and players[n].sans <= 400 then
				tfm.exec.addShamanObject(24, tfm.get.room.playerList[n].x, tfm.get.room.playerList[n].y, 0, 0, 0)
				tfm.exec.chatMessage(getMsg("stung"), n)
				tfm.exec.killPlayer(n)
				tfm.exec.addImage("busted-beeeeeeeeeeeeeee.8429", "!2", 102, 5, n)
			elseif players[n].sans >= 400 and players[n].sans <= 2100 then
				falanfilan(n)
			end

			players[n].timestamp = os.time()
		end
	end

	if (k == 32 and tfm.get.room.playerList[n].x>=1 and tfm.get.room.playerList[n].x<= 171 and tfm.get.room.playerList[n].y>=332 and tfm.get.room.playerList[n].y<=380) and not tfm.get.room.playerList[n].isDead then
		if players[n].timestamp < os.time()-4000 then
			players[n].sans = math.random(1,3100)

			if players[n].sans >= 1 and players[n].sans <= 350 then
				tfm.exec.addShamanObject(24, tfm.get.room.playerList[n].x, tfm.get.room.playerList[n].y, 0, 0, 0)
				tfm.exec.chatMessage(getMsg("stung"), n)
				tfm.exec.killPlayer(n)
				tfm.exec.addImage("busted-beeeeeeeeeeeeeee.8429", "!2", 102, 5, n)
			elseif players[n].sans >= 350 and players[n].sans <= 3100 then
				falanfilan(n)
			end

			players[n].timestamp = os.time()
		end
	end

	if (k == 32 and tfm.get.room.playerList[n].x>=1 and tfm.get.room.playerList[n].x<= 152 and tfm.get.room.playerList[n].y>=26 and tfm.get.room.playerList[n].y<=113) and not tfm.get.room.playerList[n].isDead then
		if players[n].timestamp < os.time()-4000 then
			players[n].sans = math.random(1,3000)

			if players[n].sans >= 1 and players[n].sans <= 320 then
				tfm.exec.addShamanObject(24, tfm.get.room.playerList[n].x, tfm.get.room.playerList[n].y, 0, 0, 0)
				tfm.exec.chatMessage(getMsg("stung"), n)
				tfm.exec.killPlayer(n)
				tfm.exec.addImage("busted-beeeeeeeeeeeeeee.8429", "!2", 102, 5, n)
			elseif players[n].sans >= 320 and players[n].sans <= 3000 then
				falanfilan(n)
			end

			players[n].timestamp = os.time()
		end
	end
end

function eventLoop(t,r)
	    if r<=0 then
	    	tfm.exec.newGame()
	    end

	    if r <= 48000 and not awakeWarning then
			tfm.exec.chatMessage(getMsg("awake"))
			awakeWarning = true

		elseif r<=41000 and not countdown3 then
			tfm.exec.chatMessage("<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> 3!!!!")
			countdown3 = true
		elseif r<=39000 and not countdown2 then
			tfm.exec.chatMessage("<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> 2!!!!")
			countdown2 = true
		elseif r<=37000 and not countdown1 then
			tfm.exec.chatMessage("<v>[<a href=\"event:x_mj;Jesse\">Jesse</a>]<n> 1!!!!")
			countdown1 = true
		elseif r<=35000 and not countdown then
			for n,p in pairs(tfm.get.room.playerList) do
			
				if players[n] and not players[n].sonradan then
					if not (p.x>=693 and p.x<= 798 and p.y>=79 and p.y<=248) and not p.isDead then
						tfm.exec.chatMessage(getMsg("stung"), n)
						countdown = true
						tfm.exec.killPlayer(n)
					else
						if not p.isDead then
							system.giveConsumables(n, 780, math.random(1,2))
							tfm.exec.playerVictory(n)

						end
					end

					tfm.exec.newGame()
				end
			end
		end
	
end