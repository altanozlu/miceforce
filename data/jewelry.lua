tfm.exec.disablePhysicalConsumables(true)
players = {}
arttir = 0
translation = {
	["EN"] = {
		["welcome"] = "<font color='#FEB1FC'>Welcome to the Jewelry Store.<br>You'll have to reach to the diamond at top right.</font>",
		["gotDiamond"] = "<font color='#FADE55'>This diamond is all yours! Now take this back to Ariana to complete the task.</font>",
		["completed"] = "<rose>Congratulations! You've completed the task.</rose>",
		["mapName"] = "Valentine's Day 2019 <bl>- Misha Jewelry | Paris, France (FR)"
	},

	["TR"] = {
		["welcome"] = "<font color='#FEB1FC'>Kuyumcuya hoş geldiniz.<br>Sağ üst taraftaki elmasa gitmeniz gerekiyor.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Elmas senindir! Şimdi bunu Ariana'ya götür ve görevi tamamla.</font>",
		["completed"] = "<rose>Tebrikler! Görevi tamamladınız.</rose>",
		["mapName"] = "Sevgililer Günü 2019 <bl>- Misha Kuyumculuk | Paris, Fransa (FR)"
	},

	["ES"] = {
        ["welcome"] = "<font color='#FEB1FC'>Bienvenido a la Joyería.<br> Debes alcanzar el diamante en la esquina superior derecha.</font>",
        ["gotDiamond"] = "<font color='#FADE55'>¡El diamante es todo tuyo! Ahora llévalo de vuelta donde Ariana y completa la tarea.</font>",
        ["completed"] = "<rose>¡Felicidades! Has completado la tarea.</rose>",
        ["mapName"] = "Día de San Valentín 2019 <bl>- Joyería de Misha | París, Francia (FR)"
    },

    ["BR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bem-vindo à Joalheria.<br>Você terá que alcançar o diamante no canto superior direito.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Esse diamante é todo seu! Agora leve isso de volta para Ariana para completar a tarefa.</font>",
		["completed"] = "<rose>Parabéns! Você acaba de completar a tarefa.</rose>",
		["mapName"] = "Dia dos Namorados 2019 <bl>- Joalheria da Misha | Paris, França (FR)"
	},

	["FR"] = {
		["welcome"] = "<font color='#FEB1FC'>Bienvenu au magasin de bijoux.<br>Vous devriez atteindre le diamant en haut à droite.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Ce diamant est tout à vous! Amenez ceci maintenant à Ariana pour compléter la tâche.</font>",
		["completed"] = "<rose>Félicitations! Vous avez complété la tâche.</rose>",
		["mapName"] = "Saint Valentin 2019 <bl>- Bijoux Misha | Paris, France (FR)"
	},

	["RU"] = {
		["welcome"] = "<font color='#FEB1FC'>Добро пожаловать в Ювелирный магазин.<br> Вы должны добраться до алмаза в правом верхнем углу.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Этот бриллиант - все твоё! Теперь отнеси это Ariana, чтобы выполнить задание.</font>",
		["completed"] = "<rose>Поздравляем! Вы выполнили задание.</rose>",
		["mapName"] = "День Святого Валентина 2019 <bl>- Мишка Ювелирные изделия | Paris, France (FR)"
	},

	["RO"] = {
		["welcome"] = "<font color='#FEB1FC'>Bine ai venit la magazinul de bijuterii.<br>Va trebui să ajungi la diamantul din dreapta sus.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Acest diamant este al tău! Acum, du-l înapoi la Ariana pentru a finaliza sarcina.</font>",
		["completed"] = "<rose>Felicitări! Ai completat sarcina.</rose>",
		["mapName"] = "Ziua Îndrăgostiților 2019 <bl>- Bijuterii Misha | Paris, Franța (FR)"
	},

	["HU"] = {
		["welcome"] = "<font color='#FEB1FC'>Üdv az Ékszerboltban.<br>El kell érned a gyémántot a jobb felső sarokban.</font>",
		["gotDiamond"] = "<font color='#FADE55'>Ez a gyémánt mind a tiéd! Most vidd vissza Arianahoz, hogy befejezd a feladatot.</font>",
		["completed"] = "<rose>Gratulálok! Befejezted a feladatot.</rose>",
		["mapName"] = "Valentin-nap 2019 <bl>- Misha Ékszer | Párizs, Franciaország (FR)"
	}
}

function main()
	map = '<C><P Ca="" /><Z><S><S X="400" m="" o="324650" L="800" Y="404" H="23" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="344" m="" o="324650" L="141" Y="366" H="45" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="496" m="" o="324650" L="102" Y="282" H="64" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="609" m="" o="324650" L="96" Y="234" H="38" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="-10" m="" L="19" Y="204" H="407" P="0,0,0,0.2,0,0,0,0" T="1" /><S X="811" m="" L="19" Y="202" H="407" P="0,0,0,0.2,0,0,0,0" T="1" /><S X="284" m="" o="324650" L="476" Y="193" H="11" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="49" m="" o="324650" L="10" Y="174" H="46" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="444" m="" o="324650" L="633" Y="127" H="12" P="0,0,0.3,0.2,0,0,0,0" T="12" /><S X="737" m="" o="324650" L="58" Y="67" H="12" P="0,0,0.3,0.2,0,0,0,0" T="12" /></S><D><DS X="69" Y="379" /></D><O /></Z></C>'
	tfm.exec.newGame(map)
end

system.community = tfm.get.room.community
if not translation[system.community] then system.community = "EN" end
function getMsg(msgName) return translation[system.community][msgName] end
function param(s, tab) return (s:gsub('($%b{})', function(w) return tab[w:sub(3, -2)] or w end)) end

function eventNewPlayer(n)
	tfm.exec.addImage("vday_jewelry_map.15324", "?1", 0, 0, n)
	tfm.exec.addImage("vday_diamond.15268", "?2", 715, 30, n, -90)
	ui.setMapName(getMsg("mapName"), n)
	system.addBot(-9826, "Ariana", 3051, "39;0,16_DFDFDF+EAACF8,0,0,26_DFDFDF+D77FEC,20_DFDFDF+EAACF8,0,0,0,3,0", 350, 325, n)
	tfm.exec.bindKeyboard(n, 32, true, true)
	players[n] = { 
		aldin = false,
		bitti = false
	}
end

function eventNewGame()
	for n,p in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
		tfm.exec.chatMessage(getMsg("welcome"), n)
	end
end

function test(h, x, y, n)
	local particles = {5}
    for i = 0, h do
      tfm.exec.displayParticle(particles[math.random(#particles)], x, y, math.cos(i) * 6 * math.random(50), math.sin(i) * 6 * math.random(50), 0, 5, n)
    end
end

function eventLoop()
	arttir = arttir + 1
	if arttir >= 5 then
		for n,p in pairs(tfm.get.room.playerList) do
			if not tfm.get.room.playerList[n].isDead then
				if not players[n].aldin then
					if (math.abs(735 - tfm.get.room.playerList[n].x) < 10 and math.abs(45 - tfm.get.room.playerList[n].y) < 10) then
						tfm.exec.removeImage(-90, n)
						tfm.exec.addImage("vday_diamond_x.15291", "?2", 715, 25, n, -91)
						tfm.exec.chatMessage(getMsg("gotDiamond"), n)
						test(5, 735, 45, n)
						players[n].aldin = true
					end					
				else
					if not players[n].bitti then
						if (math.abs(350 - tfm.get.room.playerList[n].x) < 20 and math.abs(325 - tfm.get.room.playerList[n].y) < 20) then
							tfm.exec.chatMessage(getMsg("completed"), n)
							system.giveConsumables(n, math.random(2125,2130), 1)
							players[n].bitti = true
						end
					end
				end
			end
		end
	end
end

main()