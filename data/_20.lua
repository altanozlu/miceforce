p = {}
s = {}
items = {
	choc = {cost = 10, image = "box_of_chocolates.4295", small = "date_smallchoc.4372"},
	ring = {cost = 25, image = "diamond_ring.4296", small = "date_smallring.4374"},
	neck = {cost = 50, image = "necklace.4297", small = "date_smallneck.4373"},
	rose = {cost = 75, image = "bouquet_of_rose.4294", small = "date_smallrose.4375"},
	wine = {cost = 100, image = "bottle_of_wine.4293", small = "date_smallwine.4376"},
	bear = {cost = 150, image = "teddy_bear.4298", small = "date_smallbear.4371"},
}

titles = {
	choc = {buy = 3196, give = 3197},
	ring = {buy = 3198, give = 3199},
	neck = {buy = 3200, give = 3201},
	rose = {buy = 3202, give = 3203},
	wine = {buy = 3204, give = 3205},
	bear = {buy = 3206, give = 3207},
}

event = {
	shop = {
		xml = '<C><P Ca="" APS="x_transformice/x_sevgili/x_shopfg.png,1,0,0,0,0,0,0" D="x_transformice/x_sevgili/x_shopbg.png" L="1600" /><Z><S><S P="0,0,0.3,0.2,0,0,0,0" L="750" o="" H="20" X="375" Y="385" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="890" o="" H="20" X="635" Y="387" T="12" /><S P="0,0,0.3,0.2,-13,0,0,0" L="120" o="" H="20" X="1135" Y="367" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="420" o="" H="20" X="1400" Y="350" T="12" /><S P="0,0,0.3,0.2,-2,0,0,0" L="420" o="" H="20" X="1470" Y="345" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="10" o="" H="101" X="1077" Y="255" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="200" o="" H="20" X="992" Y="208" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="200" o="" H="20" X="956" Y="159" T="12" /><S P="0,0,0.3,0.2,-36,0,0,0" L="10" o="" H="61" X="1070" Y="176" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="220" o="" H="10" X="745" Y="160" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="55" o="" H="10" X="796" Y="146" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="55" o="" H="10" X="693" Y="146" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="283" o="" H="10" X="719" Y="57" T="12" /><S P="0,0,0.3,0.2,-1,0,0,0" L="221" o="" H="10" X="516" Y="141" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="300" o="" H="33" X="256" Y="128" T="12" /><S P="0,0,0.3,0.2,0,0,0,0" L="10" o="" H="182" X="111" Y="234" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="" H="400" X="-5" Y="200" T="12" /><S P="0,0,0,0.2,0,0,0,0" L="10" o="" X="1605" H="400" Y="200" T="12" /></S><D><DS Y="360" X="455" /></D><O /></Z></C>',
		name = "Valentine's Shopping"
	},
	date = {
		xml = '<C><P L="2400" Ca="" D="x_transformice/x_sevgili/x_sevgililer2018_bg.png" DS="m;350,375,950,120,2040,275,930,213,415,150,1850,107" APS="x_transformice/x_sevgili/x_foreground.png,1,0,0,0,0,0,0;x_transformice/x_sevgili/x_sevgililer2018_cafe.png,0,545,150,670,390,0,0;x_transformice/x_sevgili/x_treehouse.png,0,0,0,250,150,0,0" /><Z><S><S m="" P="0,0,0.3,0.2,0,0,0,0" L="900" o="36D087" H="25" X="450" Y="400" T="12" /><S m="" P="0,0,0.3,0.2,-3,0,0,0" L="343" o="36D087" H="25" X="1042" Y="391" T="12" /><S m="" P="0,0,0.3,0.2,-6,0,0,0" L="280" o="36D087" H="25" X="1350" Y="368" T="12" /><S m="" P="0,0,0.3,0.2,-21,0,0,0" L="155" o="36D087" H="25" X="1525" Y="330" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="500" o="36D087" H="25" X="1842" Y="302" T="12" /><S m="" P="0,0,0.3,0.2,20,0,0,0" L="157" o="36D087" H="25" X="2161" Y="328" T="12" /><S m="" P="0,0,0.3,0.2,6,0,0,0" L="207" o="36D087" H="25" X="2314" Y="363" T="12" /><S m="" P="0,0,,,,0,0,0" L="55" H="140" X="860" Y="320" T="9" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="476" o="36D087" H="10" X="1130" Y="232" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="290" o="36D087" H="10" X="682" Y="235" T="12" /><S m="" P="0,0,0.3,0.2,-1,0,0,0" L="301" o="36D087" H="10" X="1047" Y="137" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="397" o="36D087" H="10" X="734" Y="140" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="210" X="540" Y="240" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="60" X="1200" Y="160" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="100" X="1200" Y="280" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="240" X="30" Y="270" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="190" X="95" Y="245" T="12" /><S m="" P="0,0,,,,0,0,0" L="60" H="220" X="60" Y="280" T="9" /><S m="" P="0,0,0.3,0.2,-28,0,0,0" L="10" o="36D087" H="83" X="119" Y="294" T="12" /><S m="" P="0,0,0.3,0.2,-96,0,0,0" L="10" o="36D087" H="45" X="120" Y="333" T="12" /><S m="" P="0,0,0.3,0.2,27,0,0,0" L="10" o="36D087" H="119" X="130" Y="213" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="190" o="36D087" H="10" X="185" Y="155" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="40" o="36D087" H="10" X="15" Y="155" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="10" o="36D087" H="400" X="-5" Y="200" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="10" o="36D087" H="400" X="2405" Y="200" T="12" /><S m="" P="0,0,0,0.2,0,0,0,0" L="2400" o="36D087" H="20" X="1200" Y="10" T="12" /><S m="" P="0,0,0.3,0.2,10,0,0,0" L="50" o="36D087" H="10" X="303" Y="159" T="12" /><S m="" P="0,0,0.3,0.2,5,0,0,0" L="75" o="36D087" H="10" X="362" Y="166" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="50" o="36D087" H="10" X="420" Y="169" T="12" /><S m="" P="0,0,0.3,0.2,-10,0,0,0" L="50" o="36D087" H="10" X="468" Y="165" T="12" /><S m="" P="0,0,0.3,0.2,-20,0,0,0" L="77" o="36D087" H="10" X="501" Y="153" T="12" /><S m="" P="0,0,0.3,0.2,-2,0,0,0" L="188" o="36D087" H="10" X="1505" Y="214" T="12" /><S m="" P="0,0,0.3,0.2,-2,0,0,0" L="159" o="36D087" H="10" X="1691" Y="161" T="12" /><S m="" P="0,0,0.3,0.2,1,0,0,0" L="284" o="36D087" H="10" X="1961" Y="128" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="100" o="36D087" H="10" X="2280" Y="208" T="12" /><S m="" P="0,0,0.3,0.2,0,0,0,0" L="90" o="36D087" H="15" X="145" Y="145" T="12" /><S m="" P="0,0,0.3,0.2,-1,0,0,0" L="200" o="36D087" H="10" X="1268" Y="230" T="12" /></S><D><P X="692" P="0,0" Y="190" T="70" /><P X="640" P="0,0" Y="230" T="67" /><P X="690" P="0,0" Y="230" T="68" /><P X="740" P="0,1" Y="230" T="67" /><P X="1000" P="0,0" Y="227" T="67" /><P X="1050" P="0,0" Y="227" T="68" /><P X="1100" P="0,1" Y="227" T="67" /><P X="470" P="0,0" Y="385" T="69" /><P X="280" P="0,0" Y="160" T="77" /><P X="990" P="0,0" Y="380" T="67" /><P X="1040" P="0,0" Y="377" T="68" /><P X="1090" P="0,1" Y="375" T="67" /><P X="640" P="0,0" Y="385" T="67" /><P X="690" P="0,0" Y="385" T="68" /><P X="740" P="0,1" Y="385" T="67" /><P X="1050" P="0,1" Y="188" T="73" /><P X="10" P="0,0" Y="149" T="72" /><P X="690" P="0,0" Y="346" T="73" /><P X="1360" P="0,1" Y="232" T="77" /><P X="1040" P="0,0" Y="338" T="71" /></D><O /></Z></C>',
		name = "Valentine's Date"
	},
}

t = {
	en = {
		buy = "Buy",
		close = "Close",
		shop = "Shop",
		notEnough = "Not enough",
		already = "Already bought",
		choc = "Box of Chocolates",
		rose = "Bouquet of Roses",
		ring = "Diamond Ring",
		neck = "Necklace",
		wine = "Bottle of Wine",
		bear = "Teddy Bear",
		another = "Please give your gift away before buying another one.",
		noGift = "You don't have a gift to give away. Please buy a gift from the shopping map.",
		soulmate = "You have a %s. Find your soulmate and give them the gift that you bought.",
		random = "Your soulmate is not in the room. You can give your gift to %s.",
		noMate = "Unfortunately, we couldn't find a date for you. Maybe next time you should try to find one yourself!",
		gifted = "%s has gifted you a %s!",
		sent = "Your gift has been sent!"
	},
	tr = {
		buy = "Satın al",
		close = "Kapat",
		shop = "Market",
		notEnough = "Yetersiz",
		already = "Satın alındı",
		choc = "Çikolata Kutusu",
		rose = "Gül Buketi",
		ring = "Elmas Yüzük",
		neck = "Gerdanlık",
		wine = "Şişe Şarap",
		bear = "Oyuncak Ayı",
		another = "Başka bir hediye almadan önce lütfen diğerini hediye edin.",
		noGift = "Verecek bir hediyeniz yok. Lütfen market haritasından bir hediye satın alın.",
		soulmate = "Elinizde bir %s var. Ruh ikizinizi bulup hediyeyi ona verin.",
		random = "Ruh ikiziniz odada değil. Hediyenizi %s kullanıcısına verebilirsiniz.",
		noMate = "Maalesef size bir ruh ikizi bulamadık. Belki bir dahaki sefere kendiniz bir tane bulabilirsiniz!",
		gifted = "%s size bir %s hediye etti!",
		sent = "Hediyenizi yolladınız!"
	},
	pl = {
		buy = "Kup",
		close = "Zamknij",
		shop = "Sklep",
		notEnough = "Za mało serc",
		already = "Już kupione",
		choc = "Pudełko Czekoladek",
		rose = "Bukiet Róż",
		ring = "Diamentowy Pierścień",
		neck = "Naszyjnik",
		wine = "Butelka Wina",
		bear = "Pluszowy miś",
		another = "Podaruj swój obecny prezent przed kupnem kolejnego",
		noGift = "Nie masz żadnego prezentu do podarowania. Kup je w sklepie.",
		soulmate = "Masz przy sobie przedmiot: %s. Znajdź swoją drugą połówkę i podaruj prezent.",
		random = "Twoja druga połówka nie jest w pokoju. Możesz podarować swój prezent graczowi %s.",
		noMate = "Niestety, nie znaleźliśmy dla Ciebie drugiej połówki w tej rundzie. Spróbuj ją znaleźć przed następną mapą!",
		gifted = "%s podarował/-a Ci %s!",
		sent = "Twój prezent został wysłany!",
	},
	ro = {
		buy = "Cumpără",
		close = "Închide",
		shop = "Magazin",
		notEnough = "Nu este suficient",
		already = "Deja cumpărat",
		choc = "Cutie de ciocolată",
		rose = "Buchet de trandafiri",
		ring = "Inel cu diamant",
		neck = "Colier",
		wine = "Sticlă de vin",
		bear = "Ursuleț de pluș",
		another = "Vă rugăm să oferiți primul cadou înainte de a cumpăra altul.",
		noGift = "Nu ai un cadou pentru oferit. Te rugăm să cumperi un cadou de pe mapa cu magazin.",
		soulmate = "Ai un %s. Găsește-ți sufletul și dăruiește-i cadoul pe care l-ai cumpărat.",
		random = "Sufletul tău pereche nu este pe sală. Îi poți oferi cadoul tău lui %s.",
		noMate = "Din păcate, nu am putut găsi nicio întâlnire pentru tine. Poate data viitoare ar trebui să-ți găsești unul chiar tu!",
		gifted = "%s ți-a oferit un %s!",
		sent = "Cadoul tău a fost trimis!",
	},
	hu = {
		buy = "Megveszem",
		close = "Bezár",
		shop = "Bolt",
		notEnough = "Nem elég",
		already = "Már megszerzett",
		choc = "Egy doboz csokoládé",
		rose = "Egy csokor rózsa",
		ring = "Gyémántgyűrű",
		neck = "Nyaklánc",
		wine = "Egy üveg bor",
		bear = "Plüssmackó",
		another = "Kérlek add el az ajándékot, mielőtt egy másikat vásárolnál",
		noGift = " Nincs elég ajándékot, hogy el tudd adni. Kérlek vegyél ajándékot a bevásárló pályáról.",
		soulmate = "Találd meg a lélektársad, majd add oda neki az ajándékot amit vettél.",
		random = "A lélektársad nincs a szobában. Odaadhatod az ajándékot %s-nak",
		noMate =  "Sajnos nem találtunk számodra megfelelő randipartnert. Legközelebb olyasvalakit kéne találnod, mint te magad.",
		gifted = "%s megajándékozott egy %s-vel",
		sent = "Az ajándékod el lett küldve!",
	},
	fr = {
		buy = "Acheter",
		close = "Fermer",
		shop = "Magasin",
		notEnough = "Ce n'est pas assez",
		already = "Déjà acquis",
		choc = "Boîte de chocolats",
		rose = "Bouquet de Roses",
		ring = "Bague en diamant",
		neck = "Collier",
		wine = "Bouteille de vin",
		bear = "Ours Teddy",
		another = "S'il vous plaît, donnez votre cadeau avant d'en acheter un autre.",
		noGift = "Vous n'avez pas de cadeau à donner, merci d'acheter un cadeau sur la carte du shopping.",
		soulmate = "Vous avez un% s. Trouvez votre âme soeur et donnez-leur le cadeau que vous avez acheté.",
		random = "Votre âme soeur n'est pas dans la pièce, vous pouvez donner votre cadeau à% s.",
		noMate = "Malheureusement, nous n'avons pas pu trouver de rencontre  pour vous, peut-être que la prochaine fois vous devriez essayer d'en trouver une vous-même!",
		gifted = "% s vous a donné un %s!",
		sent = "Votre cadeau a été envoyé!",
	},
	ru = {
		buy = "Купить",
		close = "Закрыть",
		shop = "Магазин",
		notEnough = "Недостаточно",
		already = "Уже куплено",
		choc = "Коробка конфет",
		rose = "Букет роз",
		ring = "Алмазное кольцо",
		neck = "Ожерелье",
		wine = "Бутылка вина",
		bear = "Плюшевый медведь",
		another = "Пожалуйста, отдайте свой подарок, прежде чем покупать еще один.",
		noGift = "У вас нет подарка, чтобы подарить. Пожалуйста, купите подарок в магазине.",
		soulmate = "У вас есть %s. Найдите свою вторую половинку и отдайте ей подарок, который вы купили.",
		random = "Вашей второй половинки нет в комнате. Вы можете подарить свой подарок игроку %s.",
		noMate = "К сожалению, мы не смогли найти для вас второй половинки. Возможно, в следующий раз вы должны попытаться найти ее сами!",
		gifted = "%s подарил/-а тебе %s!",
		sent = "Ваш подарок отправлен!",
	},
	es = {
		buy = "Comprar",
		close = "Cerrar",
		shop = "Tienda",
		notEnough = "No es suficiente",
		already = "Ya adquirido",
		choc = "Caja de Chocolates",
		rose = "Ramo de Rosas",
		ring = "Anillo de Diamantes",
		neck = "Collar",
		wine = "Botella de Vino",
		bear = "Oso Teddy",
		another = "Por favor, obsequia tu regalo primero antes de comprar otro.",
		noGift = "No tienes nada que regalar. Por favor compra algo en el mapa de compra de regalos.",
		soulmate = "Tienes un/una %s. Encuentra a tu pareja y regálale el obsequio que compraste.",
		random = "Tu pareja no está en tu sala. Puedes darle tu obsequio a %s.",
		noMate = " Desafortunadamente no pudimos encontrarte una pareja. ¡Quizás a la próxima deberías tratar de buscar una por tu cuenta!",
		gifted = "%s te ha obsequiado un/una %s!",
		sent = "¡Tu obsequio ha sido enviado!",
	},
	br = {
		buy = "Comprar",
		close = "Fechar",
		shop = "Loja",
		notEnough = "Não é suficiente",
		already = "Já adquirido",
		choc = "Caixa de Chocolates",
		rose = "Buquê de Rosas",
		ring = "Anel de Diamante",
		neck = "Colar",
		wine = "Garrafa de Vinho",
		bear = "Urso de Pelúcia ",
		another = "Por favor, dê o seu presente primeiro antes de comprar outro.",
		noGift= "Você não tem um presente para doar. Por favor, compre um presente no mapa shopping.",
		soulmate = "Você tem %s. Encontre sua alma gêmea e dê à ela o presente que você comprou",
		random = "Sua alma gêmea não está na sala. Você pode dar seu presente para %s.",
		noMate = " Infelizmente, nós não conseguimos arranjar um encontro pra você. Talvez na próxima vez você deva tentar encontrar um você mesmo!",
		gifted = "%s. presenteou você uma %s!",
		sent = "Seu presente foi enviado!",
	},
	ar = {
		buy = "اشتري",
		close = "أغلق",
		shop = "َالمتجر",
		notEnough = "غير كافي",
		already = "ِاشتريته سابقا",
		choc = "صندوق من الشكلاطة",
		rose = "باقة ورود",
		ring = "خاتم من الألماس",
		neck = "قلادة",
		wine = "قنينة شراب",
		bear = "دب محشو",
		another = "َ.المرجو إعطاء هديتك قبل ٱشتراء واحدة أخرى",
		noGift = ".ليس لديك هدية لإهدائها. المرجو إشتراء هدية من خريطة الإشتراء",
		soulmate = ".ابحث عن رفيق روحك لإعطائهم الهدية التي ٱشتريتها .%s  لديك",
		random = "%s رفيق روحك ليس موجودا في الغرفة، يمكنك إهداء هديتك ل.",
		noMate = "!لسوء الحظ، لم نستطع إيجاد أحد لتأخذه في موعد. ربما في المرة المقبلة يجب أن تجد واحدا لنفسك!",
		gifted = "!%s قام بإهدائك %s",
		sent = "!تم إرسال هديتك",
	},
}

function main()
	local com = tfm.get.room.community:lower()
	s.l = t[com] and com or "en"
	s.round = tfm.get.room.round

	tfm.exec.newGame(event["shop"].xml, true, 42)

end

function eventNewGame()
	local rn = tfm.get.room.name

	for n in pairs(tfm.get.room.playerList) do
		eventNewPlayer(n)
	end

	ui.setMapName(event["shop"].name)


	local c = tfm.get.room.xmlMapInfo.mapCode
	for k, v in pairs(tfm.get.room.playerList) do
		p[k].got = nil
		hearts(k)
		if p[k].market then
			showMarket(k)
		end
		shopButton(k, true)
	end
end


function eventNewPlayer(n)
	p[fix(n, true)] = {
		count = 0,
		ui = {},
		bought = {},
		lastGift = 0,
	}
	for k, v in pairs({0, 1, 2, 3, 67}) do
		tfm.exec.bindKeyboard(n, v, true, true)
		tfm.exec.bindKeyboard(n, v, false, true)
	end
	system.loadPlayerData(fix(n, true))
	tfm.exec.bindKeyboard(n, 32, true, true)
	tfm.exec.bindKeyboard(n, 74, true, true)
	tfm.exec.bindKeyboard(n, 75, true, true)
	tfm.exec.bindKeyboard(n, 76, true, true)
	tfm.exec.bindKeyboard(n, 67, true, true)
	hearts(n)

	ui.setMapName(event["shop"].name)

end

function eventEmotePlayed(n, e, f, target)
	local pn = tfm.get.room.playerList[n]
	if e == 22 and fix(target, true) == fix(p[n].soulmate, true) then
		if pn.x > 1880 then
			system.giveTitle(n, 3208)
			system.giveTitle(target, 3208)
		end
	end
end

function saveData(n)
	n = fix(n, true)
	if p[n] then
		local bought = {}
		for k, v in pairs(p[n].bought) do
			table.insert(bought, k)
		end
		local data = p[n].count..";"..(#bought > 0 and table.concat(bought, ",") or "-")..";"..(p[n].holding or "-")..";"..p[n].lastGift..";"..(p[n].broke and "true" or "-")
		system.savePlayerData(n, data)
	end
end

function eventPlayerDataLoaded(n, d)
	local n = fix(n, true)
	if d:find(";") then
		local data = split(d, ";")
		if p[n] then
			p[n].count = data[1] and tonumber(data[1]) or 0
			local bought = data[2] and split(data[2], ",") or {}
			p[n].holding = data[3] and #data[3] > 1 and data[3] or nil
			p[n].lastGift = data[4] and tonumber(data[4]) or 0
			p[n].broke = data[5] and #data[5] > 1 and true or nil
			for k, v in pairs(bought) do
				if v ~= "-" then
					p[n].bought[v] = true
				end
			end
			hearts(n)
		end
	end
end

function eventPlayerWon(n)
	if p[n].got and not tfm.get.room.playerList[n].isShaman then
		p[n].count = p[n].count + 1
		hearts(n)
		saveData(n)
	end
end

function reset(n)
	local c = tfm.get.room.xmlMapInfo.mapCode
	if not p[n] then
		p[n] = {count = 0}
	end
	if m[c] then
		p[n].heart = tfm.exec.addImage("heart_new.png", "?999", m[c][1] - 20, m[c][2] - 20, n)
		tfm.exec.bindKeyboard(n, 32, true, true)
	end
	hearts(n)
end


function findSoulmate(n)
	local alone = {}
	for k, v in pairs(tfm.get.room.playerList) do
		local sm = system.bilgi(k).soulmate
		if p[k].holding and not tfm.get.room.playerList[sm] and p[k].soulmate == nil and k ~= n then
			table.insert(alone, k)
		end
	end
	if #alone > 0 then
		local randomSoulmate = alone[math.random(#alone)]
		p[randomSoulmate].soulmate = n
		p[n].soulmate = randomSoulmate
	end
end

function hearts(n, c, l)

		c = c or p[n].count
		local text, x, w = "size='14'><p align='right'>"..c, 725, 65
		ui.addTextArea(0, "<font color='#000001' "..text, n, x - 1, 369, w, nil, 0, 1, 0)
		ui.addTextArea(1, "<font color='#000001' "..text, n, x - 1, 371, w, nil, 0, 1, 0)
		ui.addTextArea(2, "<font color='#000001' "..text, n, x + 1, 369, w, nil, 0, 1, 0)
		ui.addTextArea(3, "<font color='#000001' "..text, n, x + 1, 371, w, nil, 0, 1, 0)
		ui.addTextArea(4, "<font color='#000001' "..text, n, x - 1, 370, w, nil, 0, 1, 0)
		ui.addTextArea(5, "<font color='#000001' "..text, n, x, 371, w, nil, 0, 1, 0)
		ui.addTextArea(6, "<font color='#000001' "..text, n, x, 369, w, nil, 0, 1, 0)
		ui.addTextArea(7, "<font color='#000001' "..text, n, x + 1, 370, w, nil, 0, 1, 0)
		ui.addTextArea(8, "<font "..text, n, x, 370, w, nil, 0, 1, 0)
		local minus = 0
		if p[n].countIMG then tfm.exec.removeImage(p[n].countIMG) end
		for i = 0, tostring(c):len() do minus = minus + 10 end
		if p[n].itemIMG then
			tfm.exec.removeImage(p[n].itemIMG)
			p[n].itemIMG = nil
		end
		if p[n].glowIMG then
			tfm.exec.removeImage(p[n].glowIMG)
			p[n].glowIMG = nil
		end
		p[n].countIMG = tfm.exec.addImage("heart_count.4292", "&0", 772 - minus + (l and 10 or 0), 370, n)

end


function particles(n, x, y)
	local list = {30, 31}
	for i = 1, 50 do
		tfm.exec.displayParticle(list[math.random(#list)], math.random(x - 5, x + 5), math.random(y - 5, y + 5), math.cos(i) * 6 * math.random(30), math.sin(i) * 6 * math.random(30), 0, 0, n)
	end
end

function split(s, t)
	local tb = {}
	for k in s:gmatch("[^"..t.."]+") do
		tb[1 + #tb] = k
	end
	return tb
end

function fix(n, noPlus)
	if n then
		if noPlus then
			return n:sub(1, 1):upper()..n:sub(2):lower()
		else
			local plus = n:sub(1, 1) == "+" and true
			if plus then
				n = n:sub(2)
			end
			return (plus and "+" or "")..n:sub(1, 1):upper()..n:sub(2):lower()
		end
	end
end

-- User Interface settings

function shopButton(n, show)
	if show then
		p[n].shopButton = tfm.exec.addImage("shopbutton.4302", "&-1", 5, 27, n)
		ui.addTextArea(999, "<a href='event:shop'><p align='center'><CH>"..t[s.l].shop.."\n", n, 10, 35, 120, 17, 0, 1, 0)
	else
		if p[n].shopButton then
			tfm.exec.removeImage(p[n].shopButton)
		end
		ui.removeTextArea(999, n)
	end
end

function eventTextAreaCallback(i, n, c)
	if c == "shop" then
		showMarket(n)
	elseif c == "close" then
		if p[n].badgeWindow then
			for k, v in pairs(p[n].badgeWindow) do
				tfm.exec.removeImage(v)
			end
			p[n].badgeWindow = nil
		end
	elseif c:sub(1, 3) == "buy" and tfm.get.room.unique > 3   then
		n = fix(n, true)
		if p[n].holding then
			tfm.exec.chatMessage("<ROSE>"..t[s.l].another, n)
		else
			local item = c:sub(4)
			p[n].count = p[n].count - items[item].cost
			p[n].bought[item] = true
			p[n].holding = item
			showMarket(n, true)
			hearts(n)
			saveData(n)
			system.giveTitle(n, titles[p[n].holding].buy)
		end
	elseif c == "gift" and tfm.get.room.unique > 3 then
		n = fix(n, true)
		if p[n].holding then
			if p[n].glowIMG then
				tfm.exec.removeImage(p[n].glowIMG)
			end
			if p[n].itemIMG then
				tfm.exec.removeImage(p[n].itemIMG)
			end
			ui.removeTextArea(0, n)
			tfm.exec.chatMessage("<V>[•] <BL>"..t[s.l].gifted:format("<CH>"..n.."<BL>", "<CH>"..t[s.l][p[n].holding]:lower().."<BL>"), p[n].soulmate)
			tfm.exec.chatMessage("<V>[•] <BL>"..t[s.l].sent, n)
			system.giveTitle(n, titles[p[n].holding].give)
			p[n].holding = nil
			if p[n].lastGift < 2 then p[n].lastGift = 0 end

			local bCount = 0
			for k, v in pairs(p[n].bought) do
				bCount = bCount + 1
			end
			if bCount == 6 then
				system.giveBadge(n, 244)
				p[n].badgeWindow = {}
				ui.addTextArea(11, "<a href='event:close'>\n\n", n, 650, 70, 40, 40)
				table.insert(p[n].badgeWindow, tfm.exec.addImage("ui_shop_bg.4300", "&-1", 100, 60, n))
				table.insert(p[n].badgeWindow, tfm.exec.addImage("x_button.4303", "&-1", 660, 80, n))
				table.insert(p[n].badgeWindow, tfm.exec.addImage("bear_badge.4799", "&0", 380, 190, n))
			end
			saveData(n)
		end
	end
end

function showMarket(n, l)
	if p[n].market then
		for i = 0, 14 do
			ui.removeTextArea(i + 10, n)
			if i < 6 then
				removeButton(i, n)
			end
		end
		for k, v in pairs(p[n].market) do
			tfm.exec.removeImage(v)
		end
		p[n].market = nil
		if not l then
			shopButton(n, true)
			return
		end
	end
	p[n].market = {}
	shopButton(n)

	-- Item names & background
	ui.addTextArea(11, "<a href='event:shop'>\n\n", n, 650, 70, 40, 40)
	table.insert(p[n].market, tfm.exec.addImage("ui_shop_bg.4300", "&-1", 100, 60, n))
	table.insert(p[n].market, tfm.exec.addImage("x_button.4303", "&-1", 660, 80, n))
	ui.addTextArea(12, "<ROSE><font size='12'>"..t[s.l].choc, n, 200, 100, 190, 40, 0, 1, 0)
	ui.addTextArea(13, "<ROSE><font size='12'>"..t[s.l].ring, n, 200, 185, 190, 40, 0, 1, 0)
	ui.addTextArea(14, "<ROSE><font size='12'>"..t[s.l].neck, n, 200, 270, 190, 40, 0, 1, 0)
	ui.addTextArea(15, "<ROSE><font size='12'>"..t[s.l].rose, n, 460, 100, 190, 40, 0, 1, 0)
	ui.addTextArea(16, "<ROSE><font size='12'>"..t[s.l].wine, n, 460, 185, 190, 40, 0, 1, 0)
	ui.addTextArea(17, "<ROSE><font size='12'>"..t[s.l].bear, n, 460, 270, 190, 40, 0, 1, 0)

	-- Price numbers
	ui.addTextArea(18, "<BL><font size='12'>"..items.choc.cost.."</font>", n, 365, 128, 190, 40, 0, 1, 0)
	ui.addTextArea(19, "<BL><font size='12'>"..items.ring.cost.."</font>", n, 365, 213, 190, 40, 0, 1, 0)
	ui.addTextArea(20, "<BL><font size='12'>"..items.neck.cost.."</font>", n, 365, 298, 190, 40, 0, 1, 0)
	ui.addTextArea(21, "<BL><font size='12'>"..items.rose.cost.."</font>", n, 625, 128, 190, 40, 0, 1, 0)
	ui.addTextArea(22, "<BL><font size='12'>"..items.wine.cost.."</font>", n, 625, 213, 190, 40, 0, 1, 0)
	ui.addTextArea(23, "<BL><font size='12'>"..items.bear.cost.."</font>", n, 625, 298, 190, 40, 0, 1, 0)

	-- Buttons
	addButton(0, (p[n].bought.choc and (p[n].holding == "choc" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.choc.cost and "<CH><a href='event:buychoc'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 205, 130, 125)
	addButton(1, (p[n].bought.ring and (p[n].holding == "ring" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.ring.cost and "<CH><a href='event:buyring'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 205, 215, 125)
	addButton(2, (p[n].bought.neck and (p[n].holding == "neck" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.neck.cost and "<CH><a href='event:buyneck'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 205, 300, 125)
	addButton(3, (p[n].bought.rose and (p[n].holding == "rose" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.rose.cost and "<CH><a href='event:buyrose'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 465, 130, 125)
	addButton(4, (p[n].bought.wine and (p[n].holding == "wine" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.wine.cost and "<CH><a href='event:buywine'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 465, 215, 125)
	addButton(5, (p[n].bought.bear and (p[n].holding == "bear" and "<J>" or "<BL>")..t[s.l].already) or (p[n].count >= items.bear.cost and "<CH><a href='event:buybear'>"..t[s.l].buy) or "<BL>"..t[s.l].notEnough, n, 465, 300, 125)

	-- Item images
	table.insert(p[n].market, tfm.exec.addImage(items.choc.image, "&0", 140, 100, n))
	table.insert(p[n].market, tfm.exec.addImage(items.ring.image, "&0", 140, 185, n))
	table.insert(p[n].market, tfm.exec.addImage(items.neck.image, "&0", 140, 270, n))
	table.insert(p[n].market, tfm.exec.addImage(items.rose.image, "&0", 400, 100, n))
	table.insert(p[n].market, tfm.exec.addImage(items.wine.image, "&0", 400, 185, n))
	table.insert(p[n].market, tfm.exec.addImage(items.bear.image, "&0", 400, 270, n))

	-- Price hearts
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 345, 128, n))
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 345, 213, n))
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 345, 298, n))
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 605, 128, n))
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 605, 213, n))
	table.insert(p[n].market, tfm.exec.addImage("heart_count.4292", "&0", 605, 298, n))
end

function addButton(i, t, n, x, y, w)
	ui.addTextArea(14000 + i, "", n, x + 1, y + 3, w, 13, 0x11171C, 0x11171C, 1)
	ui.addTextArea(15000 + i, "", n, x - 1, y + 1, w, 13, 0x5D7D90, 0x5D7D90, 1)
	ui.addTextArea(16000 + i, "", n, x, y + 2, w, 13, 0x324650, 0x324650, 1)
	ui.addTextArea(17000 + i, "<p align='center'>"..t.."\n", n, x, y, w, 17, 0, 0, 0)
end

function removeButton(i, n)
	for k = 4, 7 do
		ui.removeTextArea(10000 + i + (k * 1000), n)
	end
end

main()
